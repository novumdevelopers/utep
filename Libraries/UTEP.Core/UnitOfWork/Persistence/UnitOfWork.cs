﻿using UTEP.Core.Repository.Implementation.Persistance.CompanyBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.CooperationBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.CountryBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.DeliveryBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.DriverBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.NotificationBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.OfferBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.ProductBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.RoleBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.SiteBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.SiteLanguageBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.SubscribeBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.TrailerBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.TruckBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.UserBasedGroup;
using UTEP.Core.Repository.Implementation.Persistance.VehicleBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.CompanyBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.DeliveryBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.DriverBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ICompanyBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ICooperationBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ICountryBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.IDriverBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.INotificationBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.IProductBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ISiteBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ISubscribeBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.IVehicleBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.OfferBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ProductBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.RoleBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.SiteLanguageBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.TrailerBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.TruckBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.UserBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.VehicleBasedGroup;
using UTEP.Core.UnitOfWork.Core;
using UTEP.Data;

namespace UTEP.Core.UnitOfWork.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly UTEPDbContext _context;
        public UnitOfWork(UTEPDbContext context)
        {
            _context = context;
            CompanyOrders = new CompanyOrderRepository(_context);
            Companies = new CompanyRepository(_context);
            CompanyShippings = new CompanyShippingRepository(_context);
            CompanyTypes = new CompanyTypeRepository(_context);
            Cities = new CityRepository(_context);
            Countries = new CountryRepository(_context);
            Cooperations = new CooperationRepository(_context);
            DeliveryDetails = new DeliveryDetailRepository(_context);
            Deliveries = new DeliveryRepository(_context);
            DriverDestinations = new DriverDestinationRepository(_context);
            DriverOrderBiddings = new DriverOrderBiddingRepository(_context);
            Drivers = new DriverRepository(_context);
            DriverResponses = new DriverResponseRepository(_context);
            DriverTrucks = new DriverTruckRepository(_context);
            DriverTrailers = new DriverTrailerRepository(_context);
            OfferToDrivers = new OfferToDriverRepository(_context);
            Products = new ProductRepository(_context);
            ProductTypes = new ProductTypeRepository(_context);
            ProductTypeTranslations = new ProductTypeTranslationRepository(_context);
            ProductImages = new ProductImageRepository(_context);
            Roles = new RoleRepository(_context);
            Trailers = new TrailerRepository(_context);
            TrailerTypes = new TrailerTypeRepository(_context);
            Trucks = new TruckRepository(_context);
            UserGroupClaims = new UserGroupClaimRepository(_context);
            UserGroups = new UserGroupRepository(_context);
            Users = new UserRepository(_context);
            UserTypes = new UserTypeRepository(_context);
            VehicleMarks = new VehicleMarkRepository(_context);
            VehicleModels = new VehicleModelRepository(_context);
            VehicleTypes = new VehicleTypeRepository(_context);
            VehicleTypeTranslations = new VehicleTypeTranslationRepository(_context);
            SiteLanguages = new SiteLanguageRepository(_context);
            SiteTexts = new SiteTextRepository(_context);
            SiteTextTranslations = new SiteTextTranslationRepository(_context);
            NotificationTokens = new NotificationTokenRepository(_context);
            Subscribes = new SubscribeRepository(_context);
        }

        public ICompanyOrderRepository CompanyOrders { get; }

        public ICompanyRepository Companies { get; }

        public ICompanyShippingRepository CompanyShippings { get; }

        public ICompanyTypeRepository CompanyTypes { get; }

        public ICountryRepository Countries { get; set; }

        public ICityRepository Cities { get; set; }

        public ICooperationRepository Cooperations { get; }

        public IDeliveryDetailRepository DeliveryDetails { get; }

        public IDeliveryRepository Deliveries { get; }

        public IDriverDestinationRepository DriverDestinations { get; }

        public IDriverOrderBiddingRepository DriverOrderBiddings { get; }

        public IDriverRepository Drivers { get; }

        public IDriverResponseRepository DriverResponses { get; }

        public IDriverTruckRepository DriverTrucks { get; }

        public IDriverTrailerRepository DriverTrailers { get; }

        public IOfferToDriverRepository OfferToDrivers { get; }

        public IProductRepository Products { get; }

        public IProductTypeRepository ProductTypes { get; }

        public IProductTypeTranslationRepository ProductTypeTranslations { get; }

        public IProductImageRepository ProductImages { get; }

        public IRoleRepository Roles { get; }

        public ITrailerRepository Trailers { get; }

        public ITrailerTypeRepository TrailerTypes { get; }

        public ITruckRepository Trucks { get; }

        public IUserGroupClaimRepository UserGroupClaims { get; }

        public IUserGroupRepository UserGroups { get; }

        public IUserRepository Users { get; }

        public IUserTypeRepository UserTypes { get; }

        public IVehicleMarkRepository VehicleMarks { get; }

        public IVehicleModelRepository VehicleModels { get; }

        public IVehicleTypeRepository VehicleTypes { get; }

        public IVehicleTypeTranslationRepository VehicleTypeTranslations { get; }

        public ISiteLanguageRepository SiteLanguages { get; }

        public INotificationTokenRepository NotificationTokens { get; }

        public ISiteTextRepository SiteTexts { get; }

        public ISiteTextTranslationRepository SiteTextTranslations { get; }

        public ISubscribeRepository Subscribes { get; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
