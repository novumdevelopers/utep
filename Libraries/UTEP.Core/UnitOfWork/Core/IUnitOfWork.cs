﻿using System;
using UTEP.Core.Repository.Implementation.Repositories.CompanyBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.DeliveryBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.DriverBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ICompanyBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ICooperationBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ICountryBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.IDriverBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.INotificationBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.IProductBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ISiteBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ISubscribeBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.IVehicleBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.OfferBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.ProductBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.RoleBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.SiteLanguageBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.TrailerBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.TruckBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.UserBasedGroup;
using UTEP.Core.Repository.Implementation.Repositories.VehicleBasedGroup;

namespace UTEP.Core.UnitOfWork.Core
{
    public interface IUnitOfWork : IDisposable
    {

        // ICompanyBaseGroup
        ICompanyOrderRepository CompanyOrders { get; }
        ICompanyRepository Companies { get; }
        ICompanyShippingRepository CompanyShippings { get; }
        ICompanyTypeRepository CompanyTypes { get; }

        // ICountryBasedGoup
        ICountryRepository Countries { get; }
        ICityRepository Cities { get; }

        // ICooperationBasedGoup
        ICooperationRepository Cooperations { get; }

        // IDeliveryBasedGroup
        IDeliveryDetailRepository DeliveryDetails { get; }
        IDeliveryRepository Deliveries { get; }

        //IDriverBasedGroup
        IDriverDestinationRepository DriverDestinations { get; }
        IDriverOrderBiddingRepository DriverOrderBiddings { get; }
        IDriverRepository Drivers { get; }
        IDriverResponseRepository DriverResponses { get; }
        IDriverTruckRepository DriverTrucks { get; }
        IDriverTrailerRepository DriverTrailers { get; }

        //IOfferBasedGoup
        IOfferToDriverRepository OfferToDrivers { get; }

        //IProductBasedGroup
        IProductRepository Products { get; }
        IProductTypeRepository ProductTypes { get; }
        IProductTypeTranslationRepository ProductTypeTranslations { get; }
        IProductImageRepository ProductImages { get; }


        //IRoleBasedGroup
        IRoleRepository Roles { get; }

        //ITrailerRepository
        ITrailerRepository Trailers { get; }
        ITrailerTypeRepository TrailerTypes { get; }

        //ITruckBAsedGroup
        ITruckRepository Trucks { get; }

        //IUserBasedGroup
        IUserGroupClaimRepository UserGroupClaims { get; }
        IUserGroupRepository UserGroups { get; }
        IUserRepository Users { get; }
        IUserTypeRepository UserTypes { get; }

        //IVehicleBasedGroup
        IVehicleMarkRepository VehicleMarks { get; }
        IVehicleModelRepository VehicleModels { get; }
        IVehicleTypeRepository VehicleTypes { get; }
        IVehicleTypeTranslationRepository VehicleTypeTranslations { get; }

        //ISiteLanguage
        ISiteLanguageRepository SiteLanguages { get; }

        //NotificationToken
        INotificationTokenRepository NotificationTokens { get; }

        //siteText
        ISiteTextRepository SiteTexts { get; }
        ISiteTextTranslationRepository SiteTextTranslations { get; }

        //Subscribe
        ISubscribeRepository Subscribes { get; }



        int Complete();


    }
}
