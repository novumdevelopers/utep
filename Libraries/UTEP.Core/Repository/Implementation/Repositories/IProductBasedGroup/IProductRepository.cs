﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.ProductViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.ProductBasedGroup
{
    public interface IProductRepository : IRepository<Product>
    {
        IDTPagedList<ProductIndexListViewModel> FindDataTable(Expression<Func<Product, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
