﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.IProductBasedGroup
{
    public interface IProductImageRepository : IRepository<ProductImage>
    {
    }
}
