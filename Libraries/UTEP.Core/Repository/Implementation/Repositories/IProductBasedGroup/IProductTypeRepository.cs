﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.ProductTypeViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.ProductBasedGroup
{
    public interface IProductTypeRepository : IRepository<ProductType>
    {
        IDTPagedList<ProductTypeIndexListViewModel> FindDataTable(Expression<Func<ProductType, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
        IQueryable<ProductType> LastTypes(Expression<Func<ProductType, bool>> predicate);
    }
}
