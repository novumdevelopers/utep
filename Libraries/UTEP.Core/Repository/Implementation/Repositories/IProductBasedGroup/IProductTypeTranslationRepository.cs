﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.IProductBasedGroup
{
    public interface IProductTypeTranslationRepository : IRepository<ProductTypeTranslation>
    {
        IQueryable<ProductTypeTranslation> LastTypes(Expression<Func<ProductTypeTranslation, bool>> predicate);
    }
}
