﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.CooperationBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CooperationViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.ICooperationBasedGroup
{
    public interface ICooperationRepository : IRepository<Cooperation>
    {
        IDTPagedList<CooperationIndexListViewModel> FindDataTable(Expression<Func<Cooperation, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
