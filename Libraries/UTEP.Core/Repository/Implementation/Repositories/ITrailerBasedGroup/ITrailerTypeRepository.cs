﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.TrailerTypeViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.TrailerBasedGroup
{
    public interface ITrailerTypeRepository : IRepository<TrailerType>
    {
        IDTPagedList<TrailerTypeIndexListViewModel> FindDataTable(Expression<Func<TrailerType, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
