﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.RoleViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.RoleBasedGroup
{
    public interface IRoleRepository : IRepository<Role>
    {
        IDTPagedList<RoleIndexListViewModel> FindDataTable(Expression<Func<Role, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
