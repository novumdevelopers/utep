﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.DriverViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.IDriverBasedGroup
{
    public interface IDriverResponseRepository : IRepository<DriverResponse>
    {
        IDTPagedList<DriverResponseIndexListViewModel> FindDataTable(Expression<Func<DriverResponse, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
