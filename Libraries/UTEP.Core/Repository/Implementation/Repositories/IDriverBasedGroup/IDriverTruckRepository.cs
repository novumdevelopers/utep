﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.IDriverBasedGroup
{
    public interface IDriverTruckRepository : IRepository<DriverTruck>
    {
    }
}
