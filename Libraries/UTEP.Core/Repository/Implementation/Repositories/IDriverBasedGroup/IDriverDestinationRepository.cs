﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.DriverBasedGroup
{
    public interface IDriverDestinationRepository : IRepository<DriverDestination>
    {
    }
}
