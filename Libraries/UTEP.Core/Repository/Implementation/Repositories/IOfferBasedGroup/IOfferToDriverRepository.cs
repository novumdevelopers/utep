﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.OfferToDriverViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.OfferBasedGroup
{
    public interface IOfferToDriverRepository : IRepository<OfferToDriver>
    {
        IDTPagedList<OfferToDriverIndexListViewModel> FindDataTable(Expression<Func<OfferToDriver, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
