﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.TruckViewModel;

namespace UTEP.Core.Repository.Implementation.Repositories.TruckBasedGroup
{
    public interface ITruckRepository : IRepository<Truck>
    {
        IDTPagedList<TruckIndexListViewModel> FindDataTable(Expression<Func<Truck, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
