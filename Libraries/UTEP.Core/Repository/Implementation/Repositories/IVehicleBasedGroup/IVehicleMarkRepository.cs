﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.VehicleViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.VehicleBasedGroup
{
    public interface IVehicleMarkRepository : IRepository<VehicleMark>
    {
        IDTPagedList<VehicleMarkIndexListViewModel> FindDataTable(Expression<Func<VehicleMark, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
        VehicleMark GetWithVehicleModels(Expression<Func<VehicleMark, bool>> predicate);
    }
}
