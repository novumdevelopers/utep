﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.IVehicleBasedGroup
{
    public interface IVehicleTypeTranslationRepository : IRepository<VehicleTypeTranslation>
    {
    }
}
