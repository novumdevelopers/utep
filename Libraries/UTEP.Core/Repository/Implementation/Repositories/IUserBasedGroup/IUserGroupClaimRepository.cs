﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.UserBasedGroup
{
    public interface IUserGroupClaimRepository : IRepository<UserGroupClaim>
    {
    }
}
