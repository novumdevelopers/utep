﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.UserBasedGroup
{
    public interface IUserGroupRepository : IRepository<UserGroup>
    {
        IDTPagedList<UserGroupIndexListViewModel> FindDataTable(Expression<Func<UserGroup, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
