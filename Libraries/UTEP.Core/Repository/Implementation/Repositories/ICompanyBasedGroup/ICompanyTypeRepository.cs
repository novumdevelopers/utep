﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.CompanyBasedGroup
{
    public interface ICompanyTypeRepository : IRepository<CompanyType>
    {
        IDTPagedList<CompanyTypeIndexListViewModel> FindDataTable(Expression<Func<CompanyType, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
