﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;
using static UTEP.Core.Repository.Implementation.Persistance.CompanyBasedGroup.CompanyOrderRepository;

namespace UTEP.Core.Repository.Implementation.Repositories.ICompanyBasedGroup
{
    public interface ICompanyOrderRepository : IRepository<CompanyOrder>
    {
        IDTPagedList<CompanyOrderIndexListViewModel> FindDataTable(Expression<Func<CompanyOrder, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
        CompanyOrder GetWithProduct(Expression<Func<CompanyOrder, bool>> predicate);
        IQueryable<CompanyOrder> GetWithProducts(Expression<Func<CompanyOrder, bool>> predicate);
        IQueryable<CompanyOrder> LastOrders(Expression<Func<CompanyOrder, bool>> predicate);
        string GetWeight(int num);
    };
}
