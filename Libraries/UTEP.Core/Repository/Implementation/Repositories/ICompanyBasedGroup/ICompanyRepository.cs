﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.CompanyBasedGroup
{
    public interface ICompanyRepository : IRepository<Company>
    {
        IDTPagedList<CompanyIndexListViewModel> FindDataTable(Expression<Func<Company, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
        Company GetWithUser(Expression<Func<Company, bool>> predicate);
        IQueryable<Company> GetWithUsers(Expression<Func<Company, bool>> predicate);
        Company GetOrderAndUser(Expression<Func<Company, bool>> predicate);
    }
}
