﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.NotificationBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.INotificationBasedGroup
{
    public interface INotificationTokenRepository : IRepository<NotificationToken>
    {
    }
}
