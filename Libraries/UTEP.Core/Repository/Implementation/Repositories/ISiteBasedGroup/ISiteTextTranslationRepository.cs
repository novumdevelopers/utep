﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.SiteBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.ISiteBasedGroup
{
    public interface ISiteTextTranslationRepository : IRepository<SiteTextTranslation>
    {
    }
}
