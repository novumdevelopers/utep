﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.SiteBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.ISiteBasedGroup
{
    public interface ISiteTextRepository : IRepository<SiteText>
    {
        SiteText GetForTranslation(Expression<Func<SiteText, bool>> predicate);
    }
}
