﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.SiteLanguageViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.SiteLanguageBasedGroup
{
    public interface ISiteLanguageRepository : IRepository<SiteLanguage>
    {
        IDTPagedList<SiteLanguageIndexListViewModel> FindDataTable(Expression<Func<SiteLanguage, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
