﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.SubscribeBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.ISubscribeBasedGroup
{
    public interface ISubscribeRepository : IRepository<Subscribe>
    {
    }
}
