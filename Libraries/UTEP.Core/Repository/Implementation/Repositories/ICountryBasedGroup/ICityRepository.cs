﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.CountryBasedGroup;

namespace UTEP.Core.Repository.Implementation.Repositories.ICountryBasedGroup
{
    public interface ICityRepository : IRepository<City>
    {
    }
}
