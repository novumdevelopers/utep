﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Repositories;
using UTEP.Data.Domain.Models.Tables.DeliveryBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.DeliveryViewModels;

namespace UTEP.Core.Repository.Implementation.Repositories.DeliveryBasedGroup
{
    public interface IDeliveryDetailRepository : IRepository<DeliveryDetail>
    {
        IDTPagedList<DeliveryDetailIndexListViewModel>FindDataTable(Expression<Func<DeliveryDetail, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue);
    }
}
