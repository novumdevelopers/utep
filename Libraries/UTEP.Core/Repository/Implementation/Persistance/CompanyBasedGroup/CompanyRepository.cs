﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.CompanyBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.CompanyBasedGroup
{
    public class CompanyRepository : Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<CompanyIndexListViewModel> FindDataTable(Expression<Func<Company, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Company>().Where(predicate).Count();

            var list = Context.Set<Company>()
                .Include(x => x.User)
                .Where(predicate)
                .Select(x => new CompanyIndexListViewModel
                {
                    Id = x.Id,
                    CompanyName = x.Name,
                    CompanyEmail = x.Email,
                    CompanyPhone = x.Phone,
                    CompanyAddress = x.Address,

                    CompanyType = new CompanyTypeForView
                    {
                        Name = x.CompanyType.Name,
                        Status = x.CompanyType.Status
                    },

                    FirstName = x.User != null ? x.User.FirstName : "----",
                    LastName = x.User != null ? x.User.LastName : "----",
                    Email = x.User != null ? x.User.Email : "----",
                    PhoneNumber = x.User != null ? x.User.PhoneNumber : "----",

                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CompanyIndexListViewModel.CompanyName):
                            list = list.OrderBy(p => p.CompanyName);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyEmail):
                            list = list.OrderBy(p => p.CompanyEmail);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyPhone):
                            list = list.OrderBy(p => p.CompanyPhone);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyAddress):
                            list = list.OrderBy(p => p.CompanyAddress);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyType):
                            list = list.OrderBy(p => p.CompanyType.Name);
                            break;
                        case nameof(CompanyIndexListViewModel.FirstName):
                            list = list.OrderBy(p => p.FirstName);
                            break;
                        case nameof(CompanyIndexListViewModel.LastName):
                            list = list.OrderBy(p => p.LastName);
                            break;
                        case nameof(CompanyIndexListViewModel.Email):
                            list = list.OrderBy(p => p.Email);
                            break;
                        case nameof(CompanyIndexListViewModel.PhoneNumber):
                            list = list.OrderBy(p => p.PhoneNumber);
                            break;
                        case nameof(CompanyIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CompanyIndexListViewModel.CompanyName):
                            list = list.OrderByDescending(p => p.CompanyName);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyEmail):
                            list = list.OrderByDescending(p => p.CompanyEmail);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyPhone):
                            list = list.OrderByDescending(p => p.CompanyPhone);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyAddress):
                            list = list.OrderByDescending(p => p.CompanyAddress);
                            break;
                        case nameof(CompanyIndexListViewModel.CompanyType):
                            list = list.OrderByDescending(p => p.CompanyType.Name);
                            break;
                        case nameof(CompanyIndexListViewModel.FirstName):
                            list = list.OrderByDescending(p => p.FirstName);
                            break;
                        case nameof(CompanyIndexListViewModel.LastName):
                            list = list.OrderByDescending(p => p.LastName);
                            break;
                        case nameof(CompanyIndexListViewModel.Email):
                            list = list.OrderByDescending(p => p.Email);
                            break;
                        case nameof(CompanyIndexListViewModel.PhoneNumber):
                            list = list.OrderByDescending(p => p.PhoneNumber);
                            break;
                        case nameof(CompanyIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.CompanyName.Contains(searchValue) || m.CompanyEmail.Contains(searchValue) || m.CompanyPhone.Contains(searchValue) || m.CompanyType.Name.Contains(searchValue) || m.FirstName.Contains(searchValue) || m.LastName.Contains(searchValue) || m.Email.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<CompanyIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

        public Company GetWithUser(Expression<Func<Company, bool>> predicate)
        {
            return Context.Set<Company>()
                .Include(x => x.User)
                .FirstOrDefault(predicate);
        }

        public IQueryable<Company> GetWithUsers(Expression<Func<Company, bool>> predicate)
        {
            return Context.Set<Company>()
                .Include(x => x.User)
                .Where(predicate);
        }

        public Company GetOrderAndUser(Expression<Func<Company, bool>> predicate)
        {
            return Context.Set<Company>()
                .Include(x => x.User)
                .Include(x => x.CompanyProducts).ThenInclude(x => x.CompanyOrder)
                .FirstOrDefault(predicate);
        }
    }
}
