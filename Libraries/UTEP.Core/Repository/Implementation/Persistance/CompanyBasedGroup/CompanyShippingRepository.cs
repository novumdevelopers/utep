﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.CompanyBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.CompanyBasedGroup
{
    public class CompanyShippingRepository : Repository<CompanyShipping>, ICompanyShippingRepository
    {
        public CompanyShippingRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<CompanyShippingIndexListViewModel> FindDataTable(Expression<Func<CompanyShipping, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<CompanyShipping>().Where(predicate).Count();

            var list = Context.Set<CompanyShipping>()
                .Where(predicate)
                .Select(x => new CompanyShippingIndexListViewModel
                {
                    Id = x.Id,
                    CompanyOrder= new CompanyOrderForView
                    {
                        Name=x.CompanyOrder.Products.First(z => z.Status == Status.Active).Name,
                        Status=x.CompanyOrder.Status
                    },
                    OriginTownLongitude = x.OriginTownLongitude,
                    OriginTownLatitude = x.OriginTownLatitude,
                    DestinationTownLongitude = x.DestinationTownLongitude,
                    DestinationTownLatitude = x.DestinationTownLatitude,
                    Deadline = x.Deadline,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CompanyShippingIndexListViewModel.CompanyOrder):
                            list = list.OrderBy(p => p.CompanyOrder.Name);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.OriginTownLongitude):
                            list = list.OrderBy(p => p.OriginTownLongitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.OriginTownLatitude):
                            list = list.OrderBy(p => p.OriginTownLatitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.DestinationTownLongitude):
                            list = list.OrderBy(p => p.DestinationTownLongitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.DestinationTownLatitude):
                            list = list.OrderBy(p => p.DestinationTownLatitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.Deadline):
                            list = list.OrderBy(p => p.Deadline);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CompanyShippingIndexListViewModel.CompanyOrder):
                            list = list.OrderByDescending(p => p.CompanyOrder.Name);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.OriginTownLongitude):
                            list = list.OrderByDescending(p => p.OriginTownLongitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.OriginTownLatitude):
                            list = list.OrderByDescending(p => p.OriginTownLatitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.DestinationTownLongitude):
                            list = list.OrderByDescending(p => p.DestinationTownLongitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.DestinationTownLatitude):
                            list = list.OrderByDescending(p => p.DestinationTownLatitude);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.Deadline):
                            list = list.OrderByDescending(p => p.Deadline);
                            break;
                        case nameof(CompanyShippingIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.CompanyOrder.Name.Contains(searchValue) || m.DestinationTownLatitude.ToString().Contains(searchValue) || m.DestinationTownLongitude.ToString().Contains(searchValue) || m.OriginTownLatitude.ToString().Contains(searchValue) || m.OriginTownLongitude.ToString().Contains(searchValue));
            }
            var recordsFiltered = list.Count();

            //total number of rows count 

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<CompanyShippingIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }


    }
}
