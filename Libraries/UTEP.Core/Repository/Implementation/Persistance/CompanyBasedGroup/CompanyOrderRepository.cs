﻿using Microsoft.EntityFrameworkCore;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.ICompanyBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.CompanyBasedGroup
{
    public class CompanyOrderRepository : Repository<CompanyOrder>, ICompanyOrderRepository
    {
        public CompanyOrderRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<CompanyOrderIndexListViewModel> FindDataTable(Expression<Func<CompanyOrder, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<CompanyOrder>().Where(predicate).Count();

            var list = Context.Set<CompanyOrder>()
                .Include(x => x.Products).ThenInclude(x => x.Company)
                .Include(x => x.CompanyShippings)
                .Where(predicate)
                .Select(x => new CompanyOrderIndexListViewModel
                {
                    Id = x.Id,
                    ProporsalPrice = x.ProporsalPrice,
                    Currency = x.Currency,
                    Name = x.Products.FirstOrDefault(y => y.Status == Status.Active).Name,
                    //Size = x.Products.FirstOrDefault(y => y.Status == Status.Active).Size,
                    Tonnage = x.Products.FirstOrDefault(y => y.Status == Status.Active).Tonnage,
                    Weight = GetWeight(x.Products.FirstOrDefault(y => y.Status == Status.Active).Weight),
                    PaletteCount = x.Products.FirstOrDefault(y => y.Status == Status.Active).PaletteCount,
                    From = $"{x.CountryFrom} ({x.From})",
                    To = $"{x.CountryTo} ({x.To})",
                    Company = new CompanyForView
                    {
                        Name = x.Products.FirstOrDefault().Company.Name != null ?
                            x.Products.FirstOrDefault(y => y.Status == Status.Active).Company.Name : "NULL",
                        Status = x.Products.FirstOrDefault().Company.Status != 0 ?
                            x.Products.FirstOrDefault(y => y.Status == Status.Active).Company.Status : 0
                    },
                    DeadLine = x.CompanyShippings.First().Deadline,
                    DealDate = x.DealDate,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CompanyOrderIndexListViewModel.ProporsalPrice):
                            list = list.OrderBy(p => p.ProporsalPrice);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.Size):
                            list = list.OrderBy(p => p.Size);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.Tonnage):
                            list = list.OrderBy(p => p.Tonnage);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.PaletteCount):
                            list = list.OrderBy(p => p.PaletteCount);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.DeadLine):
                            list = list.OrderBy(p => p.DeadLine);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CompanyOrderIndexListViewModel.ProporsalPrice):
                            list = list.OrderByDescending(p => p.ProporsalPrice);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.DeadLine):
                            list = list.OrderByDescending(p => p.DeadLine);
                            break;
                        case nameof(CompanyOrderIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue) /*|| m.Company.Name.Contains(searchValue)*/ || m.ProporsalPrice.ToString().Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging
            var data = list.Skip(pageNumber).Take(pageSize).ToList();


            return new DTPagedList<CompanyOrderIndexListViewModel>(data, recordsTotal, recordsFiltered);


        }

        public CompanyOrder GetWithProduct(Expression<Func<CompanyOrder, bool>> predicate)
        {
            return Context.Set<CompanyOrder>()
                .Include(x => x.Products).ThenInclude(y => y.Company).ThenInclude(x => x.User)
                .Include(x => x.Products).ThenInclude(y => y.ProductType).ThenInclude(y => y.ProductTypeTranslations)
                .Include(x => x.Products).ThenInclude(x => x.ProductImages)
                .Include(x => x.CompanyShippings)
                .FirstOrDefault(predicate);
        }

        public new IPagedList ToPagedList(Expression<Func<CompanyOrder, bool>> predicate, string lang, int pageNumber, int pageSize)
        {
            var allPostsCount = Context.Set<CompanyOrder>().Where(predicate).Count();

            var list = Context.Set<CompanyOrder>()
                        .Include(x => x.Products).ThenInclude(y => y.Company).ThenInclude(x => x.User)
                        .Include(x => x.Products).ThenInclude(y => y.ProductType).ThenInclude(y => y.ProductTypeTranslations).ThenInclude(y => y.SiteLanguage)
                        .Include(x => x.Products).ThenInclude(y => y.ProductImages)
                        .Include(x => x.Products).ThenInclude(x => x.CompanyOrder)
                        .Include(x => x.CompanyShippings)
                        .Where(predicate)
                        .OrderByDescending(x => x.AddedDate)
                        .Skip((pageNumber - 1) * pageSize)
                        .Take(pageSize);

            var posts = list.Select(x => new CompanyOrderIndexListViewModel
            {
                Id = x.Id,
                Company = new CompanyForView
                {
                    Name = x.Products.FirstOrDefault().Company.Name != null ?
                            x.Products.FirstOrDefault(y => y.Status == Status.Active).Company.Name : "NULL",
                    Status = x.Products.FirstOrDefault().Company.Status != 0 ?
                            x.Products.FirstOrDefault(y => y.Status == Status.Active).Company.Status : 0
                },
                AddedDate = x.AddedDate,
                DeadLine = x.CompanyShippings.First().Deadline,
                ProporsalPrice = x.ProporsalPrice,
                Currency = x.Currency,
                Name = x.Products.FirstOrDefault(y => y.Status == Status.Active).Name,
                Size = x.Products.FirstOrDefault(y => y.Status == Status.Active).Size,
                Tonnage = x.Products.FirstOrDefault(y => y.Status == Status.Active).Tonnage,
                PaletteCount = x.Products.FirstOrDefault(y => y.Status == Status.Active).PaletteCount,
                From = $"{x.CountryFrom} ({x.From})",
                To = $"{x.CountryTo} ({x.To})",
                Products = x.Products.Where(y => y.Status == Status.Active && y.CompanyOrder.Status == Status.Active).ToList(),
                ProductImages = x.Products.FirstOrDefault().ProductImages.Where(y => y.Status == Status.Active).ToList()
            });

            return new StaticPagedList<CompanyOrderIndexListViewModel>(posts, pageNumber, pageSize, allPostsCount);
        }

        public IQueryable<CompanyOrder> GetWithProducts(Expression<Func<CompanyOrder, bool>> predicate)
        {
            return Context.Set<CompanyOrder>()
                .Include(x => x.Products).ThenInclude(y => y.Company)
                .Include(x => x.Products).ThenInclude(y => y.ProductType).ThenInclude(y => y.ProductTypeTranslations)
                .Where(predicate);
        }

        public IQueryable<CompanyOrder> LastOrders(Expression<Func<CompanyOrder, bool>> predicate)
        {
            return Context.Set<CompanyOrder>()
                            .Include(x => x.Products).ThenInclude(x => x.Company).ThenInclude(x => x.User)
                            .Include(x => x.Products).ThenInclude(x => x.ProductImages)
                            .Where(predicate);
            //list.Select(x => new CompanyOrderListViewModel
            //{
            //    Currency = GetCurrency(x.Currency),
            //    Name = x.Products.First().Name,
            //    ProductImages = x.Products.First().ProductImages,
            //    From = x.From,
            //    To = x.To
            //});

            //return list;
        }

        public string GetWeight(int num)
        {
            if (num == 1)
            {
                return "Kq";
            }
            else if (num == 2)
            {
                return "Ton";
            }
            else
            {
                return "---";
            }
        }

    }
}
