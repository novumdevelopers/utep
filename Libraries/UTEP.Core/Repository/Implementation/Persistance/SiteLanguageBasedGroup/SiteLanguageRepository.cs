﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories;
using UTEP.Core.Repository.Implementation.Repositories.SiteLanguageBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.SiteLanguageViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.SiteLanguageBasedGroup
{
    public class SiteLanguageRepository : Repository<SiteLanguage>, ISiteLanguageRepository
    {
        public SiteLanguageRepository(DbContext context) : base(context)
        {

        }
        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<SiteLanguageIndexListViewModel> FindDataTable(Expression<Func<SiteLanguage, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<SiteLanguage>().Where(predicate).Count();

            var list = Context.Set<SiteLanguage>()
                .Include(x=>x.LanguageCode)
                .Where(predicate)
                .Select(x=> new SiteLanguageIndexListViewModel {
                    Id = x.Id,
                    Name = x.Name,
                    Code = x.LanguageCode,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(SiteLanguageIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(SiteLanguageIndexListViewModel.Code):
                            list = list.OrderBy(p => p.Code);
                            break;
                        case nameof(SiteLanguageIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(SiteLanguageIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(SiteLanguageIndexListViewModel.Code):
                            list = list.OrderByDescending(p => p.Code);
                            break;
                        case nameof(SiteLanguageIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue) || m.Code.Contains(searchValue));
            }
            var recordsFiltered = list.Count();

            //total number of rows count 

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<SiteLanguageIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }
    }
}
