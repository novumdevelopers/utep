﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.RoleBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using System.Linq.Expressions;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Data.Domain.Models.ViewModels.RoleViewModels;
using System.Linq;

namespace UTEP.Core.Repository.Implementation.Persistance.RoleBasedGroup
{
    public class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<RoleIndexListViewModel> FindDataTable(Expression<Func<Role, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Role>().Where(predicate).Count();

            var list = Context.Set<Role>()
                .Where(predicate)
                .Select(x => new RoleIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(RoleIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        default:
                            list = list.OrderBy(p => p.Name);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(RoleIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.Name);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<RoleIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }
    }
}
