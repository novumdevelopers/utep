﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.ICooperationBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CooperationBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CooperationViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.CooperationBasedGroup
{
    public class CooperationRepository : Repository<Cooperation>, ICooperationRepository
    {
        public CooperationRepository(DbContext context) : base(context)
        {
        }

        UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<CooperationIndexListViewModel> FindDataTable(Expression<Func<Cooperation, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Cooperation>().Where(predicate).Count();

            var list = Context.Set<Cooperation>()
                .Where(predicate)
                .Select(x => new CooperationIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CooperationIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(CooperationIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(CooperationIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(CooperationIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging
            var data = list.Skip(pageNumber).Take(pageSize).ToList();


            return new DTPagedList<CooperationIndexListViewModel>(data, recordsTotal, recordsFiltered);


        }
    }
}
