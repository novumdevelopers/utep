﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.ISubscribeBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.SubscribeBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.SubscribeBasedGroup
{
    public class SubscribeRepository : Repository<Subscribe>, ISubscribeRepository
    {
        public SubscribeRepository(DbContext context) : base(context)
        {
        }

        UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
    }
}
