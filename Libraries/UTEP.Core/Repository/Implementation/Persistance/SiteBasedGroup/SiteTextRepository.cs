﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.ISiteBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.SiteBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.SiteBasedGroup
{
    public class SiteTextRepository : Repository<SiteText>, ISiteTextRepository
    {
        public SiteTextRepository(DbContext context) : base(context)
        {
        }

        UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public SiteText GetForTranslation(Expression<Func<SiteText, bool>> predicate)
        {
            return Context.Set<SiteText>()
                .Include(x => x.SiteTextTranslations).ThenInclude(x => x.SiteLanguage)
                .FirstOrDefault(predicate);
        }

        public new IQueryable<SiteText> Find(Expression<Func<SiteText, bool>> predicate)
        {
            return Context.Set<SiteText>()
                .Include(x => x.SiteTextTranslations).ThenInclude(x => x.SiteLanguage)
                .Where(predicate);
        }


        public new SiteText Get(Expression<Func<SiteText, bool>> predicate)
        {
            return Context.Set<SiteText>()
                .Include(x => x.SiteTextTranslations).ThenInclude(x => x.SiteLanguage)
                .FirstOrDefault(predicate);
        }

    }
}
