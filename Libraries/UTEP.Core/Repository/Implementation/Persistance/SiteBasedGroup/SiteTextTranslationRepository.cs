﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.ISiteBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.SiteBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.SiteBasedGroup
{
    public class SiteTextTranslationRepository : Repository<SiteTextTranslation>, ISiteTextTranslationRepository
    {
        public SiteTextTranslationRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

    }
}
