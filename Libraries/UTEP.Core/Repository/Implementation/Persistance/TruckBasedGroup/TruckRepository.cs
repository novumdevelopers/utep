﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.TruckBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;
using System.Linq.Expressions;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Data.Domain.Models.ViewModels.TruckViewModel;
using System.Linq;

namespace UTEP.Core.Repository.Implementation.Persistance.TruckBasedGroup
{
    public class TruckRepository : Repository<Truck>, ITruckRepository
    {
        public TruckRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<TruckIndexListViewModel> FindDataTable(Expression<Func<Truck, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Truck>().Where(predicate).Count();

            var list = Context.Set<Truck>()
                .Where(predicate)
                .Select(x => new TruckIndexListViewModel
                {
                    Id = x.Id,
                    RegistrationNumber = x.RegistrationNumber,
                    RegistrationCertificate = x.RegistrationCertificate,
                    YearOf = x.YearOf,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(TruckIndexListViewModel.RegistrationNumber):
                            list = list.OrderBy(p => p.RegistrationNumber);
                            break;
                        case nameof(TruckIndexListViewModel.RegistrationCertificate):
                            list = list.OrderBy(p => p.RegistrationCertificate);
                            break;
                        case nameof(TruckIndexListViewModel.YearOf):
                            list = list.OrderBy(p => p.YearOf);
                            break;
                        case nameof(TruckIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(TruckIndexListViewModel.RegistrationNumber):
                            list = list.OrderByDescending(p => p.RegistrationNumber);
                            break;
                        case nameof(TruckIndexListViewModel.RegistrationCertificate):
                            list = list.OrderByDescending(p => p.RegistrationCertificate);
                            break;
                        case nameof(TruckIndexListViewModel.YearOf):
                            list = list.OrderByDescending(p => p.YearOf);
                            break;
                        case nameof(TruckIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.RegistrationCertificate.Contains(searchValue) || m.RegistrationNumber.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<TruckIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }
    }
}
