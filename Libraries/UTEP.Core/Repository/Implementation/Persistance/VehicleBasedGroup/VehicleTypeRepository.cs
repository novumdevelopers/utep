﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.VehicleBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.VehicleTypeViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.VehicleBasedGroup
{
    public class VehicleTypeRepository : Repository<VehicleType>, IVehicleTypeRepository
    {
        public VehicleTypeRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<VehicleTypeIndexListViewModel> FindDataTable(Expression<Func<VehicleType, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<VehicleType>().Where(predicate).Count();

            var list = Context.Set<VehicleType>()
                .Include(x => x.VehicleTypeTranslations).ThenInclude(x => x.SiteLanguage)
                .Where(predicate)
                .Select(x => new VehicleTypeIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.VehicleTypeTranslations.FirstOrDefault(z => z.SiteLanguage.LanguageCode == lang).Name,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(VehicleTypeIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(VehicleTypeIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(VehicleTypeIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(VehicleTypeIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<VehicleTypeIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

        public new VehicleType Get(Expression<Func<VehicleType, bool>> predicate)
        {
            return Context.Set<VehicleType>()
                .Include(x => x.VehicleTypeTranslations).ThenInclude(y => y.SiteLanguage)
                .FirstOrDefault(predicate);
        }

    }
}
