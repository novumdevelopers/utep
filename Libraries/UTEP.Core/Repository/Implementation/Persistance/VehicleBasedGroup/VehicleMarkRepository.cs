﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.VehicleBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.VehicleViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.VehicleBasedGroup
{
    public class VehicleMarkRepository : Repository<VehicleMark>, IVehicleMarkRepository
    {
        public VehicleMarkRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<VehicleMarkIndexListViewModel> FindDataTable(Expression<Func<VehicleMark, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<VehicleMark>().Where(predicate).Count();

            var list = Context.Set<VehicleMark>()
                .Where(predicate)
                .Select(x => new VehicleMarkIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    BrandLogo=x.BrandLogo,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(VehicleMarkIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(VehicleMarkIndexListViewModel.BrandLogo):
                            list = list.OrderBy(p => p.BrandLogo);
                            break;
                        case nameof(VehicleMarkIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(VehicleMarkIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(VehicleMarkIndexListViewModel.BrandLogo):
                            list = list.OrderByDescending(p => p.BrandLogo);
                            break;
                        case nameof(VehicleMarkIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue) || m.BrandLogo.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<VehicleMarkIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

        public VehicleMark GetWithVehicleModels(Expression<Func<VehicleMark, bool>> predicate)
        {
            return Context.Set<VehicleMark>()
                .Include(x=>x.VehicleModels)
                .FirstOrDefault(predicate);
        }
    }
}
