﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.VehicleBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.VehicleViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.VehicleBasedGroup
{
    public class VehicleModelRepository : Repository<VehicleModel>, IVehicleModelRepository
    {
        public VehicleModelRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<VehicleModelIndexListViewModel> FindDataTable(Expression<Func<VehicleModel, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<VehicleModel>().Where(predicate).Count();

            var list = Context.Set<VehicleModel>()
                .Include(x => x.VehicleMark)
                .Where(predicate)
                .Select(x => new VehicleModelIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    VehicleType = new VehicleTypeForView
                    {
                        Name = x.VehicleType.VehicleTypeTranslations.FirstOrDefault(y => y.SiteLanguage.LanguageCode == lang).Name,
                        Status = x.VehicleType.VehicleTypeTranslations.FirstOrDefault(y => y.SiteLanguage.LanguageCode == lang).Status,
                    },
                    VehicleMark = new VehicleMarkForView
                    {
                        Name = x.VehicleMark.Name,
                        Status = x.VehicleMark.Status
                    },
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(VehicleModelIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(VehicleModelIndexListViewModel.VehicleType):
                            list = list.OrderBy(p => p.VehicleType.Name);
                            break;
                        case nameof(VehicleModelIndexListViewModel.VehicleMark):
                            list = list.OrderBy(p => p.VehicleMark.Name);
                            break;
                        case nameof(VehicleModelIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(VehicleModelIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(VehicleModelIndexListViewModel.VehicleType):
                            list = list.OrderByDescending(p => p.VehicleType.Name);
                            break;
                        case nameof(VehicleModelIndexListViewModel.VehicleMark):
                            list = list.OrderByDescending(p => p.VehicleMark.Name);
                            break;
                        case nameof(VehicleModelIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue) || m.VehicleType.Name.Contains(searchValue) || m.VehicleMark.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<VehicleModelIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
