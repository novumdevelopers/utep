﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.IVehicleBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.VehicleBasedGroup
{
    public class VehicleTypeTranslationRepository : Repository<VehicleTypeTranslation>, IVehicleTypeTranslationRepository
    {
        public VehicleTypeTranslationRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
    }
}
