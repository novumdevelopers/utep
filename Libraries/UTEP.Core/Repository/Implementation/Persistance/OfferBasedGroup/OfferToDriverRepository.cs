﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.OfferBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.OfferToDriverViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.OfferBasedGroup
{
    public class OfferToDriverRepository : Repository<OfferToDriver>, IOfferToDriverRepository
    {
        public OfferToDriverRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<OfferToDriverIndexListViewModel> FindDataTable(Expression<Func<OfferToDriver, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<OfferToDriver>().Where(predicate).Count();

            var list = Context.Set<OfferToDriver>()
                .Where(predicate)
                .Select(x => new OfferToDriverIndexListViewModel
                {
                    Id = x.Id,
                    Driver = new DriverForView
                    {
                        Id = x.DriverId,
                        Name = x.Driver.User.FirstName + " " + x.Driver.User.LastName,
                        Status = x.Driver.Status
                    },
                    CompanyOrder = new CompanyOrderForView
                    {
                        Name = x.CompanyOrder.Products.First(z => z.Status == Status.Active).Name,
                        Status = x.CompanyOrder.Status
                    },
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(OfferToDriverIndexListViewModel.CompanyOrder):
                            list = list.OrderBy(p => p.CompanyOrder.Name);
                            break;
                        case nameof(OfferToDriverIndexListViewModel.Driver):
                            list = list.OrderBy(p => p.Driver.Name);
                            break;
                        case nameof(OfferToDriverIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(OfferToDriverIndexListViewModel.CompanyOrder):
                            list = list.OrderByDescending(p => p.CompanyOrder.Name);
                            break;
                        case nameof(OfferToDriverIndexListViewModel.Driver):
                            list = list.OrderByDescending(p => p.Driver.Name);
                            break;
                        case nameof(OfferToDriverIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Driver.Name.Contains(searchValue) || m.CompanyOrder.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<OfferToDriverIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
