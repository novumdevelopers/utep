﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.ProductBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.ProductViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.ProductBasedGroup
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
        public IDTPagedList<ProductIndexListViewModel> FindDataTable(Expression<Func<Product, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Product>().Where(predicate).Count();

            var list = Context.Set<Product>()
                .Where(predicate)
                .Select(x => new ProductIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    PaletteCount = x.PaletteCount,
                    Size = x.Size,
                    Tonnage = x.Tonnage,
                    Company = new CompanyForView
                    {
                        Name = x.Company.Name,
                        Status = x.Company.Status
                    },
                    ProductType = new ProductTypeForView
                    {
                        Name = x.ProductType.ProductTypeTranslations.FirstOrDefault(y => y.SiteLanguage.LanguageCode == lang).Name,
                        Status = x.ProductType.ProductTypeTranslations.FirstOrDefault(y => y.SiteLanguage.LanguageCode == lang).Status
                    },
                    AddedDate = x.AddedDate,
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(ProductIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(ProductIndexListViewModel.PaletteCount):
                            list = list.OrderBy(p => p.PaletteCount);
                            break;
                        case nameof(ProductIndexListViewModel.Tonnage):
                            list = list.OrderBy(p => p.Tonnage);
                            break;
                        case nameof(ProductIndexListViewModel.Size):
                            list = list.OrderBy(p => p.Size);
                            break;
                        case nameof(ProductIndexListViewModel.Company):
                            list = list.OrderBy(p => p.Company.Name);
                            break;
                        case nameof(ProductIndexListViewModel.ProductType):
                            list = list.OrderBy(p => p.ProductType.Name);
                            break;
                        case nameof(ProductIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(ProductIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(ProductIndexListViewModel.PaletteCount):
                            list = list.OrderByDescending(p => p.PaletteCount);
                            break;
                        case nameof(ProductIndexListViewModel.Tonnage):
                            list = list.OrderByDescending(p => p.Tonnage);
                            break;
                        case nameof(ProductIndexListViewModel.Size):
                            list = list.OrderByDescending(p => p.Size);
                            break;
                        case nameof(ProductIndexListViewModel.Company):
                            list = list.OrderByDescending(p => p.Company.Name);
                            break;
                        case nameof(ProductIndexListViewModel.ProductType):
                            list = list.OrderByDescending(p => p.ProductType.Name);
                            break;
                        case nameof(ProductIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue) || m.Company.Name.Contains(searchValue) || m.ProductType.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<ProductIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
