﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.IProductBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.ProductBasedGroup
{
    public class ProductTypeTranslationRepository : Repository<ProductTypeTranslation>, IProductTypeTranslationRepository
    {
        public ProductTypeTranslationRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IQueryable<ProductTypeTranslation> LastTypes(Expression<Func<ProductTypeTranslation, bool>> predicate)
        {
            return Context.Set<ProductTypeTranslation>()
                    .Include(x => x.ProductType).ThenInclude(x => x.CompanyProducts).ThenInclude(x => x.CompanyOrder)
                    .Include(x => x.SiteLanguage)
                    .Where(predicate);
        }
    }
}
