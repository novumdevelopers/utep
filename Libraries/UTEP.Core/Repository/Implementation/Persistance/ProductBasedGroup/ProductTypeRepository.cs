﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.ProductBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.ProductTypeViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.ProductBasedGroup
{
    public class ProductTypeRepository : Repository<ProductType>, IProductTypeRepository
    {
        public ProductTypeRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<ProductTypeIndexListViewModel> FindDataTable(Expression<Func<ProductType, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<ProductType>().Where(predicate).Count();

            var list = Context.Set<ProductType>()
                .Include(x => x.ProductTypeTranslations).ThenInclude(x => x.SiteLanguage)
                .Where(predicate)
                .Select(x => new ProductTypeIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.ProductTypeTranslations.FirstOrDefault(z => z.SiteLanguage.LanguageCode == lang).Name,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(ProductTypeIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(ProductTypeIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(ProductTypeIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(ProductTypeIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<ProductTypeIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

        //Include Elemek yaddan cixmasin
        public new ProductType Get(Expression<Func<ProductType, bool>> predicate)
        {

            return Context.Set<ProductType>()
                .Include(x => x.ProductTypeTranslations).ThenInclude(y => y.SiteLanguage)
                .FirstOrDefault(predicate);
        }

        public IQueryable<ProductType> LastTypes(Expression<Func<ProductType, bool>> predicate)
        {
            return Context.Set<ProductType>()
                        .Include(x => x.ProductTypeTranslations).ThenInclude(x => x.SiteLanguage)
                        .Include(x => x.CompanyProducts).ThenInclude(x => x.CompanyOrder)
                        .Where(predicate);
        }

        public new IQueryable<ProductType> Find(Expression<Func<ProductType, bool>> predicate)
        {
            return Context.Set<ProductType>()
                        .Include(x => x.ProductTypeTranslations).ThenInclude(x => x.SiteLanguage)
                        .Include(x => x.CompanyProducts).ThenInclude(x => x.CompanyOrder)
                        .Where(predicate);
        }


    }
}
