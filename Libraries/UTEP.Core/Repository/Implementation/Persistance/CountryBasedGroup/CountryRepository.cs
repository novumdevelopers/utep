﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.ICountryBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CountryBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.CountryBasedGroup
{
    public class CountryRepository : Repository<Country> , ICountryRepository
    {
        public CountryRepository(DbContext context): base(context)
        {
        }

        UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
    }
}
