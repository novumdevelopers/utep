﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.DeliveryBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DeliveryBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.DeliveryViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.DeliveryBasedGroup
{
    public class DeliveryRepository : Repository<Delivery>, IDeliveryRepository
    {
        public DeliveryRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<DeliveryIndexListViewModel> FindDataTable(Expression<Func<Delivery, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Delivery>().Where(predicate).Count();

            var list = Context.Set<Delivery>()
                .Where(predicate)
                .Select(x => new DeliveryIndexListViewModel
                {
                    Id = x.Id,
                    CompanyOrder = new CompanyOrderForView {
                        Name=x.CompanyOrder.Products.First(z => z.Status == Status.Active).Name,
                        Status=x.CompanyOrder.Status
                    },
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DeliveryIndexListViewModel.CompanyOrder):
                            list = list.OrderBy(p => p.CompanyOrder.Name);
                            break;
                        case nameof(DeliveryIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DeliveryIndexListViewModel.CompanyOrder):
                            list = list.OrderByDescending(p => p.CompanyOrder.Name);
                            break;
                        case nameof(DeliveryIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.CompanyOrder.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<DeliveryIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
