﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.DeliveryBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DeliveryBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.DeliveryViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.DeliveryBasedGroup
{
    public class DeliveryDetailRepository : Repository<DeliveryDetail>, IDeliveryDetailRepository
    {
        public DeliveryDetailRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<DeliveryDetailIndexListViewModel> FindDataTable(Expression<Func<DeliveryDetail, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<DeliveryDetail>().Where(predicate).Count();

            var list = Context.Set<DeliveryDetail>()
                .Where(predicate)
                .Select(x => new DeliveryDetailIndexListViewModel
                {
                    Id = x.Id,
                    Delivery = new DeliveryForView
                    {
                        Name = x.Shipping.CompanyOrder.Products.First(z => z.Status == Status.Active).Name,
                        Status = x.Shipping.CompanyOrder.Status
                    },
                    DestinationTownLatitude = x.DestinationTownLatitude,
                    DestinationTownLongitude = x.DestinationTownLongitude,
                    FinishDate = x.FinishDateTime,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DeliveryDetailIndexListViewModel.Delivery):
                            list = list.OrderBy(p => p.Delivery.Name);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.DestinationTownLatitude):
                            list = list.OrderBy(p => p.DestinationTownLatitude);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.DestinationTownLongitude):
                            list = list.OrderBy(p => p.DestinationTownLongitude);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.FinishDate):
                            list = list.OrderBy(p => p.FinishDate);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DeliveryDetailIndexListViewModel.Delivery):
                            list = list.OrderByDescending(p => p.Delivery.Name);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.DestinationTownLatitude):
                            list = list.OrderByDescending(p => p.DestinationTownLatitude);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.DestinationTownLongitude):
                            list = list.OrderByDescending(p => p.DestinationTownLongitude);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.FinishDate):
                            list = list.OrderByDescending(p => p.FinishDate);
                            break;
                        case nameof(DeliveryDetailIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Delivery.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<DeliveryDetailIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
