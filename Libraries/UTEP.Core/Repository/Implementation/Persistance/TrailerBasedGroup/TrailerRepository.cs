﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.TrailerBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.TrailerViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.TrailerBasedGroup
{
    public class TrailerRepository : Repository<Trailer>, ITrailerRepository
    {
        public TrailerRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
        public IDTPagedList<TrailerIndexListViewModel> FindDataTable(Expression<Func<Trailer, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Trailer>().Where(predicate).Count();

            var list = Context.Set<Trailer>()
                .Where(predicate)
                .Select(x => new TrailerIndexListViewModel
                {
                    Id = x.Id,
                    Tonnage = x.Tonnage,
                    PaletteCount = x.PaletteCount,
                    RegistrationNumber = x.RegistrationNumber,
                    TrailerType = new TrailerTypeForView
                    {
                        Name = x.TrailerType.Name,
                        Status=x.TrailerType.Status
                    },
                    VehicleModel = new VehicleModelForView
                    {
                        Name=x.VehicleModel.Name,
                        Status=x.VehicleModel.Status
                    },
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(TrailerIndexListViewModel.Tonnage):
                            list = list.OrderBy(p => p.Tonnage);
                            break;
                        case nameof(TrailerIndexListViewModel.PaletteCount):
                            list = list.OrderBy(p => p.PaletteCount);
                            break;
                        case nameof(TrailerIndexListViewModel.RegistrationCertificate):
                            list = list.OrderBy(p => p.RegistrationCertificate);
                            break;
                        case nameof(TrailerIndexListViewModel.RegistrationNumber):
                            list = list.OrderBy(p => p.RegistrationNumber);
                            break;
                        case nameof(TrailerIndexListViewModel.TrailerType):
                            list = list.OrderBy(p => p.TrailerType.Name);
                            break;
                        case nameof(TrailerIndexListViewModel.VehicleModel):
                            list = list.OrderBy(p => p.VehicleModel.Name);
                            break;
                        case nameof(TrailerIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(TrailerIndexListViewModel.Tonnage):
                            list = list.OrderByDescending(p => p.Tonnage);
                            break;
                        case nameof(TrailerIndexListViewModel.PaletteCount):
                            list = list.OrderByDescending(p => p.PaletteCount);
                            break;
                        case nameof(TrailerIndexListViewModel.RegistrationCertificate):
                            list = list.OrderByDescending(p => p.RegistrationCertificate);
                            break;
                        case nameof(TrailerIndexListViewModel.RegistrationNumber):
                            list = list.OrderByDescending(p => p.RegistrationNumber);
                            break;
                        case nameof(TrailerIndexListViewModel.TrailerType):
                            list = list.OrderByDescending(p => p.TrailerType.Name);
                            break;
                        case nameof(TrailerIndexListViewModel.VehicleModel):
                            list = list.OrderByDescending(p => p.VehicleModel.Name);
                            break;
                        case nameof(TrailerIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.RegistrationNumber.Contains(searchValue) || m.RegistrationCertificate.Contains(searchValue) || m.TrailerType.Name.Contains(searchValue) || m.VehicleModel.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<TrailerIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
