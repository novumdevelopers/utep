﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.TrailerBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using System.Linq.Expressions;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Data.Domain.Models.ViewModels.TrailerTypeViewModels;
using System.Linq;

namespace UTEP.Core.Repository.Implementation.Persistance.TrailerBasedGroup
{
    public class TrailerTypeRepository : Repository<TrailerType>, ITrailerTypeRepository
    {
        public TrailerTypeRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<TrailerTypeIndexListViewModel> FindDataTable(Expression<Func<TrailerType, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<TrailerType>().Where(predicate).Count();

            var list = Context.Set<TrailerType>()
                .Where(predicate)
                .Select(x => new TrailerTypeIndexListViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(TrailerTypeIndexListViewModel.Name):
                            list = list.OrderBy(p => p.Name);
                            break;
                        case nameof(TrailerTypeIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(TrailerTypeIndexListViewModel.Name):
                            list = list.OrderByDescending(p => p.Name);
                            break;
                        case nameof(TrailerTypeIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.Name.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<TrailerTypeIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }
    }
}
