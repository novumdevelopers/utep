﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.INotificationBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.NotificationBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.NotificationBasedGroup
{
    public class NotificationTokenRepository : Repository<NotificationToken>, INotificationTokenRepository
    {
        public NotificationTokenRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
    }
}
