﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.DriverBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.DriverViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.DriverBasedGroup
{
    public class DriverOrderBiddingRepository : Repository<DriverOrderBidding>, IDriverOrderBiddingRepository
    {
        public DriverOrderBiddingRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<DriverOrderBiddingIndexListViewModel> FindDataTable(Expression<Func<DriverOrderBidding, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<DriverOrderBidding>().Where(predicate).Count();

            var list = Context.Set<DriverOrderBidding>()
                .Where(predicate)
                .Select(x => new DriverOrderBiddingIndexListViewModel
                {
                    Id = x.Id,
                    DriverName = x.Driver.User.FirstName + " " + x.Driver.User.LastName,
                    OfferToDriverName = x.OfferToDriver.CompanyOrder.Products.First(z => z.Status == Status.Active).Name,
                    SuggestedPrice = x.SuggestedPrice,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DriverOrderBiddingIndexListViewModel.OfferToDriverName):
                            list = list.OrderBy(p => p.OfferToDriverName);
                            break;
                        case nameof(DriverOrderBiddingIndexListViewModel.DriverName):
                            list = list.OrderBy(p => p.DriverName);
                            break;
                        case nameof(DriverOrderBiddingIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DriverOrderBiddingIndexListViewModel.OfferToDriverName):
                            list = list.OrderByDescending(p => p.OfferToDriverName);
                            break;
                        case nameof(DriverOrderBiddingIndexListViewModel.DriverName):
                            list = list.OrderByDescending(p => p.DriverName);
                            break;
                        case nameof(DriverOrderBiddingIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.OfferToDriverName.Contains(searchValue) || m.DriverName.Contains(searchValue) || m.DriverName.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<DriverOrderBiddingIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
