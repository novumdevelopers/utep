﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.IDriverBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.DriverBasedGroup
{
    public class DriverTrailerRepository : Repository<DriverTrailer>,IDriverTrailerRepository
    {
        public DriverTrailerRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
    }
}
