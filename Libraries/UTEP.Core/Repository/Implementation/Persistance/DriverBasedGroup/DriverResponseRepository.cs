﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.IDriverBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.DriverViewModels;
using UTEP.Settings;

namespace UTEP.Core.Repository.Implementation.Persistance.DriverBasedGroup
{
    public class DriverResponseRepository : Repository<DriverResponse>, IDriverResponseRepository
    {
        public DriverResponseRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<DriverResponseIndexListViewModel> FindDataTable(Expression<Func<DriverResponse, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<DriverResponse>().Where(predicate).Count();

            var list = Context.Set<DriverResponse>()
                .Where(predicate)
                .Select(x => new DriverResponseIndexListViewModel
                {
                    Id = x.Id,
                    Driver=new DriverForView {
                        Name = x.Driver.User.FirstName + " " + x.Driver.User.LastName,
                        Status=x.Driver.Status
                    } ,
                    Truck=new TruckForView { 
                        Name=x.Truck.RegistrationNumber,
                        Status=x.Truck.Status
                    },
                    Trailer=new TrailerForView {
                        Name=x.Trailer.RegistrationNumber,
                        Status=x.Trailer.Status
                    },
                    CompanyOrder= new CompanyOrderForView {
                        Name=x.CompanyOrder.Products.First(z => z.Status == Status.Active).Name,
                        Status=x.CompanyOrder.Status
                    },
                    EstimatePrice=x.EstimatePrice.ToString(),
                    StartDate=x.StartDate,
                    EndDate=x.EndDate,
                    DayCount=x.DayCount,
                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DriverResponseIndexListViewModel.CompanyOrder):
                            list = list.OrderBy(p => p.CompanyOrder.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.Driver):
                            list = list.OrderBy(p => p.Driver.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.Truck):
                            list = list.OrderBy(p => p.Truck.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.Trailer):
                            list = list.OrderBy(p => p.Trailer.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.StartDate):
                            list = list.OrderBy(p => p.StartDate);
                            break;
                        case nameof(DriverResponseIndexListViewModel.EndDate):
                            list = list.OrderBy(p => p.EndDate);
                            break;
                        case nameof(DriverResponseIndexListViewModel.EstimatePrice):
                            list = list.OrderBy(p => p.EstimatePrice);
                            break;
                        case nameof(DriverResponseIndexListViewModel.DayCount):
                            list = list.OrderBy(p => p.DayCount);
                            break;
                        case nameof(DriverResponseIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DriverResponseIndexListViewModel.CompanyOrder):
                            list = list.OrderByDescending(p => p.CompanyOrder.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.Driver):
                            list = list.OrderByDescending(p => p.Driver.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.Truck):
                            list = list.OrderByDescending(p => p.Truck.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.Trailer):
                            list = list.OrderByDescending(p => p.Trailer.Name);
                            break;
                        case nameof(DriverResponseIndexListViewModel.StartDate):
                            list = list.OrderByDescending(p => p.StartDate);
                            break;
                        case nameof(DriverResponseIndexListViewModel.EndDate):
                            list = list.OrderByDescending(p => p.EndDate);
                            break;
                        case nameof(DriverResponseIndexListViewModel.EstimatePrice):
                            list = list.OrderByDescending(p => p.EstimatePrice);
                            break;
                        case nameof(DriverResponseIndexListViewModel.DayCount):
                            list = list.OrderByDescending(p => p.DayCount);
                            break;
                        case nameof(DriverResponseIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.CompanyOrder.Name.Contains(searchValue) || m.Driver.Name.Contains(searchValue) || m.Trailer.Name.Contains(searchValue) || m.Trailer.Name.Contains(searchValue) || m.EstimatePrice.ToString().Contains(searchValue) || m.DayCount.ToString().Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<DriverResponseIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

    }
}
