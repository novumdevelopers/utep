﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.DriverBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.DriverBasedGroup
{
    public class DriverDestinationRepository : Repository<DriverDestination>, IDriverDestinationRepository
    {
        public DriverDestinationRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
    }
}
