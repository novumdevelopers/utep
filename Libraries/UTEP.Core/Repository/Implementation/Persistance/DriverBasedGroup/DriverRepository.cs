﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.DriverBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.DriverViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.DriverBasedGroup
{
    public class DriverRepository : Repository<Driver>, IDriverRepository
    {
        public DriverRepository(DbContext context) : base(context)
        {
        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<DriverIndexListViewModel> FindDataTable(Expression<Func<Driver, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<Driver>().Where(predicate).Count();

            var list = Context.Set<Driver>()
                .Include(x => x.User)
                .Where(predicate)
                .Select(x => new DriverIndexListViewModel
                {
                    Id = x.Id,
                    FirstName = x.User != null ? x.User.FirstName : "---",
                    LastName = x.User != null ? x.User.LastName : "---",
                    Email = x.User != null ? x.User.Email : "---",
                    PhoneNumber = x.User != null ? x.User.PhoneNumber : "---",
                    Company = x.Company != null ? x.Company.Name : "---",

                    AddedDate = x.AddedDate
                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DriverIndexListViewModel.FirstName):
                            list = list.OrderBy(p => p.FirstName);
                            break;
                        case nameof(DriverIndexListViewModel.LastName):
                            list = list.OrderBy(p => p.LastName);
                            break;
                        case nameof(DriverIndexListViewModel.Email):
                            list = list.OrderBy(p => p.Email);
                            break;
                        case nameof(DriverIndexListViewModel.PhoneNumber):
                            list = list.OrderBy(p => p.PhoneNumber);
                            break;
                        case nameof(DriverIndexListViewModel.AddedDate):
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderBy(p => p.AddedDate);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(DriverIndexListViewModel.FirstName):
                            list = list.OrderByDescending(p => p.FirstName);
                            break;
                        case nameof(DriverIndexListViewModel.LastName):
                            list = list.OrderByDescending(p => p.LastName);
                            break;
                        case nameof(DriverIndexListViewModel.Email):
                            list = list.OrderByDescending(p => p.Email);
                            break;
                        case nameof(DriverIndexListViewModel.PhoneNumber):
                            list = list.OrderByDescending(p => p.PhoneNumber);
                            break;
                        case nameof(DriverIndexListViewModel.AddedDate):
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.AddedDate);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.FirstName.Contains(searchValue) || m.LastName.Contains(searchValue) || m.Email.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<DriverIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }

        public Driver GetWithUser(Expression<Func<Driver, bool>> predicate)
        {
            return Context.Set<Driver>()
                .Include(x => x.User)
                .Include(y => y.DriverTrailers)
                .Include(z => z.DriverTrucks)
                .FirstOrDefault(predicate);
        }
    }
}
