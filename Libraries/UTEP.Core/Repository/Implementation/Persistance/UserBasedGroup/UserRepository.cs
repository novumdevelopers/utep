﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UTEP.Core.Additional.AjaxDataTable.DTPagedList;
using UTEP.Core.Repository.Implementation.Repositories.UserBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.UserViewModels;

namespace UTEP.Core.Repository.Implementation.Persistance.UserBasedGroup
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;

        public IDTPagedList<UserIndexListViewModel> FindDataTable(Expression<Func<User, bool>> predicate, string lang, int pageNumber, int pageSize, string sortColumn, string sortColumnDirection, string searchValue)
        {
            var recordsTotal = Context.Set<User>().Where(predicate).Count();

            var list = Context.Set<User>()
                .Where(predicate)
                .Select(x => new UserIndexListViewModel
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    PhoneNumber = x.PhoneNumber != null ? x.PhoneNumber : "----"

                });

            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                if (sortColumnDirection == "asc")
                {
                    switch (sortColumn)
                    {
                        case nameof(UserIndexListViewModel.FirstName):
                            list = list.OrderBy(p => p.FirstName);
                            break;
                        case nameof(UserIndexListViewModel.LastName):
                            list = list.OrderBy(p => p.LastName);
                            break;
                        default:
                            list = list.OrderBy(p => p.FirstName);
                            break;
                    }
                }
                else if (sortColumnDirection == "desc")
                {
                    switch (sortColumn)
                    {
                        case nameof(UserIndexListViewModel.FirstName):
                            list = list.OrderByDescending(p => p.FirstName);
                            break;
                        case nameof(UserIndexListViewModel.LastName):
                            list = list.OrderByDescending(p => p.LastName);
                            break;
                        default:
                            list = list.OrderByDescending(p => p.FirstName);
                            break;
                    }
                }
            }

            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                list = list.Where(m => m.FirstName.Contains(searchValue) || m.LastName.Contains(searchValue));
            }

            //total number of rows count 
            var recordsFiltered = list.Count();

            //Paging 
            var data = list.Skip(pageNumber).Take(pageSize).ToList();

            return new DTPagedList<UserIndexListViewModel>(data, recordsTotal, recordsFiltered);
        }
    }

}
