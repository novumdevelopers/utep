﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Core.Repository.Implementation.Repositories.UserBasedGroup;
using UTEP.Core.Repository.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;

namespace UTEP.Core.Repository.Implementation.Persistance.UserBasedGroup
{
    public class UserGroupClaimRepository : Repository<UserGroupClaim>, IUserGroupClaimRepository
    {
        public UserGroupClaimRepository(DbContext context) : base(context)
        {

        }

        public UTEPDbContext UTEPDbContext => Context as UTEPDbContext;
    }
}
