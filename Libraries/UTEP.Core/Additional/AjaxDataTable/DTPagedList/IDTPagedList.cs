﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Core.Additional.AjaxDataTable.DTPagedList
{
    public interface IDTPagedList<T>
    {
        int PageCount { get; }
        int RecordsTotal { get; }
        int RecordsFiltered { get; }
        IEnumerable<T> Data { get; }
    }
}
