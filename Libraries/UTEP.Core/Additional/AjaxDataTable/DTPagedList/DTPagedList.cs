﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Core.Additional.AjaxDataTable.DTPagedList
{
    public class DTPagedList<T> : IDTPagedList<T>
    {
        public DTPagedList(IEnumerable<T> data, int recordsTotal, int recordsFiltered)
        {
            Data = data;
            RecordsTotal = recordsTotal;
            RecordsFiltered = recordsFiltered;
        }
        public int PageCount { get; protected set; }

        public int RecordsTotal { get; protected set; }

        public int RecordsFiltered { get; protected set; }

        public IEnumerable<T> Data { get; protected set; }
    }
}
