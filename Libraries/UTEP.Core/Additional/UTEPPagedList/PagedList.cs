﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Settings;

namespace UTEP.Core.Additional.UTEPPagedList
{
    public class PagedList<TEntity> : IPagedList<TEntity>
    {
        public PagedList(int pageNumber, int pageSize, int totalItemCount, IEnumerable<TEntity> elements)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalItemCount = totalItemCount;
            Elements = elements;
            PageCount = NMath.Adjust((double)TotalItemCount / pageSize);
            HasPreviousPage = PageNumber > 1 ? true : false;
            HasNextPage = PageNumber < PageCount ? true : false;
            IsFirstPage = PageNumber == 1 ? true : false;
            IsLastPage = PageNumber == PageCount ? true : false;
        }
        public IEnumerable<TEntity> Elements { get; set; }

        public int PageCount { get; protected set; }

        public int TotalItemCount { get; protected set; }

        public int PageNumber { get; protected set; }

        public int PageSize { get; protected set; }

        public bool HasPreviousPage { get; protected set; }

        public bool HasNextPage { get; protected set; }

        public bool IsFirstPage { get; protected set; }

        public bool IsLastPage { get; protected set; }
    }
}
