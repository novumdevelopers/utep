﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Core.Additional.UTEPPagedList
{
    public interface IPagedList<TEntity>
    {
        IEnumerable<TEntity> Elements { get; }
        int PageCount { get; }
        int TotalItemCount { get; }
        int PageNumber { get; }
        int PageSize { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
        bool IsFirstPage { get; }
        bool IsLastPage { get; }
    }
}
