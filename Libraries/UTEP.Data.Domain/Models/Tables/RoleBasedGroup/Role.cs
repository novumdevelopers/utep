﻿namespace UTEP.Data.Domain.Models.Tables.RoleBasedGroup
{
    public class Role : Microsoft.AspNetCore.Identity.IdentityRole<long>
    {
        public string Description { get; set; } 
    }
}
