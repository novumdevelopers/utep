﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.OfferBasedGroup
{
    public class OfferToDriver : BaseEntity
    {
        public OfferToDriver()
        {
            DriverOrderBiddings = new HashSet<DriverOrderBidding>();
        }

        public long DriverId { get; set; }
        public virtual Driver Driver { get; set; }
        public long CompanyOrderId { get; set; }
        public virtual CompanyOrder CompanyOrder { get; set; }
        public virtual ICollection<DriverOrderBidding> DriverOrderBiddings { get; set; }
    }
}