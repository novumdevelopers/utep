﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.SubscribeBasedGroup
{
    public class Subscribe : BaseEntity
    {
        public string Email { get; set; }
    }
}
