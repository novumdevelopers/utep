﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.CompanyBasedGroup
{
    public class CompanyOrder : BaseEntity
    {
        public CompanyOrder()
        {
            CompanyShippings = new HashSet<CompanyShipping>();
            OfferToDrivers = new HashSet<OfferToDriver>();
            DriverResponses = new HashSet<DriverResponse>();
            Products = new HashSet<Product>();
        }

        
        public string RegistrationToken { get; set; } = Guid.NewGuid().ToString();
        public virtual DateTime DealDate { get; set; }
        public decimal ProporsalPrice { get; set; }

        //from CompanyShipping
        public string From { get; set; }
        public string CountryFrom { get; set; }
        public string FromDescription { get; set; }
        public string To { get; set; }
        public string CountryTo { get; set; }
        public string ToDescription { get; set; }
        public decimal OriginTownLongitude { get; set; }
        public decimal OriginTownLatitude { get; set; }
        public decimal DestinationTownLongitude { get; set; }
        public decimal DestinationTownLatitude { get; set; }
        public int Currency { get; set; } = Settings.Currency.AZN;


        public virtual ICollection<CompanyShipping> CompanyShippings { get; set; }
        public virtual ICollection<OfferToDriver> OfferToDrivers { get; set; }
        public virtual ICollection<DriverResponse> DriverResponses { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
