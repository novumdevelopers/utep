﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.CompanyBasedGroup
{
    public class Company : BaseEntity
    {
        public Company()
        {
            CompanyProducts = new HashSet<Product>();
            Drivers = new HashSet<Driver>();
        }
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public long CompanyTypeId { get; set; } 
        public virtual CompanyType CompanyType { get; set; }
        public string TaxIdentificationNumber { get; set; }
        public int TypeOfCompany { get; set; } = UTEP.Settings.TypeOfCompany.Client;

        public string Name { get; set; }
        public string Address { get; set; }
        public decimal LocationLongitude { get; set; }
        public decimal LocationLatitude { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Logo { get; set; } = "logodefault.png";
        public string VOEN { get; set; }

        public virtual ICollection<Product> CompanyProducts { get; set; }
        public virtual ICollection<Driver> Drivers { get; set; }
    }
}
