﻿using System.Collections.Generic;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.CompanyBasedGroup
{
    public class CompanyType : BaseEntity
    {
        public CompanyType()
        {
            Companies = new HashSet<Company>();
        }

        public string Name { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
    }
}
