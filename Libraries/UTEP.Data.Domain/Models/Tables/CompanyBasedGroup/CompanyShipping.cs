﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.CompanyBasedGroup
{
    public class CompanyShipping : BaseEntity
    {
        public long CompanyOrderId { get; set; }
        public virtual CompanyOrder CompanyOrder { get; set; }
        public decimal OriginTownLongitude { get; set; }
        public decimal OriginTownLatitude { get; set; }
        public decimal DestinationTownLongitude { get; set; }
        public decimal DestinationTownLatitude { get; set; }
        public virtual DateTime Deadline { get; set; }
    }
}
