﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.TrailerBasedGroup
{
    public class Trailer : BaseEntity
    {
        public Trailer()
        {
            DriverResponses = new HashSet<DriverResponse>();
            DriverTrailers = new HashSet<DriverTrailer>();
            //Drivers = new HashSet<Driver>();
        }
        public long VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
        public long TrailerTypeId { get; set; }
        public virtual TrailerType TrailerType { get; set; }
        public virtual DateTime YearOf { get; set; }
        public decimal Size { get; set; }
        public decimal Tonnage { get; set; }
        public int PaletteCount { get; set; }
        public string RegistrationNumber { get; set; }
        public string RegistrationCertificate { get; set; }
        //public virtual ICollection<Driver> Drivers { get; set; }
        public virtual ICollection<DriverResponse> DriverResponses { get; set; }
        public virtual ICollection<DriverTrailer> DriverTrailers { get; set; }

    }
}