﻿using System.Collections.Generic;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.TrailerBasedGroup
{
    public class TrailerType:BaseEntity
    {
        public TrailerType()
        {
            Trailers = new HashSet<Trailer>();
        }

        public string Name { get; set; }
        public virtual ICollection<Trailer> Trailers { get; set; }
    }
}
