﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.ProductBasedGroup
{
    public class ProductTypeTranslation : BaseEntity
    {
        public long ProductTypeId { get; set; }
        public ProductType ProductType { get; set; }

        public long SiteLanguageId { get; set; }
        public SiteLanguage SiteLanguage { get; set; }

        public string Name { get; set; }
    }
}
