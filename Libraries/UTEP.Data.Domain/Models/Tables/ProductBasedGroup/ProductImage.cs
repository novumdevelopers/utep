﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.ProductBasedGroup
{
    public class ProductImage : BaseEntity
    {
        public string MainPath { get; set; }
        public string MediumPath { get; set; }
        public long ProductId { get; set; }
        public Product Product { get; set; }
    }
}
