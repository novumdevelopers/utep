﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.ProductBasedGroup
{
  public  class ProductType:BaseEntity
    {
        public ProductType()
        {
            CompanyProducts = new HashSet<Product>();
            ProductTypeTranslations = new HashSet<ProductTypeTranslation>();
        }
        public string ClassName { get; set; }

        public virtual ICollection<Product> CompanyProducts { get; set; }
        public virtual ICollection<ProductTypeTranslation> ProductTypeTranslations { get; set; }
    }
}
