﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.ProductBasedGroup
{
    public class Product : BaseEntity
    {
        public Product()
        {
            ProductImages = new HashSet<ProductImage>();
        }
        public long ProductTypeId { get; set; }
        public virtual ProductType ProductType { get; set; }
        public long CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public long CompanyOrderId { get; set; }
        public virtual CompanyOrder CompanyOrder { get; set; }
        public string Name { get; set; }
        public decimal Size { get; set; }
        public decimal Tonnage { get; set; }
        public int Weight { get; set; } = Settings.Weight.Kg;
        public int WeightType { get; set; }
        public int PaletteCount { get; set; }
        public int ViewCount { get; set; } = 0;
        public string Description { get; set; }

        public virtual ICollection<ProductImage> ProductImages { get; set; }


    }
}
