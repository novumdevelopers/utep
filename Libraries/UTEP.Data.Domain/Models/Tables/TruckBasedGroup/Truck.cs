﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.TruckBasedGroup
{
    public class Truck : BaseEntity
    {
        public Truck()
        {
            DriverResponses = new HashSet<DriverResponse>();
            DriverTrucks = new HashSet<DriverTruck>();
            //Drivers = new HashSet<Driver>();
        }

        public long VehicleModelId { get; set; }
        public virtual VehicleModel VehicleModel { get; set; }
        public virtual DateTime YearOf { get; set; }
        public string RegistrationNumber { get; set; }
        public string RegistrationCertificate { get; set; }
        //public virtual ICollection<Driver> Drivers { get; set; }
        public virtual ICollection<DriverTruck> DriverTrucks { get; set; }
        public virtual ICollection<DriverResponse> DriverResponses { get; set; }
    }
}
