﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.CountryBasedGroup
{
    public class City : BaseEntity
    {
        public string Name { get; set; }
        public long CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}
