﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.CountryBasedGroup
{
    public class Country : BaseEntity
    {
        public Country()
        {
            Cities = new HashSet<City>();
        }
        public string Name { get; set; }
        public virtual ICollection<City> Cities { get; set; }
    }
}
