﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.DeliveryBasedGroup
{
    public class Delivery : BaseEntity
    {
        public Delivery()
        {
            DeliveryDetails = new HashSet<DeliveryDetail>();
        }

        public long CompanyOrderId { get; set; }
        public virtual CompanyOrder CompanyOrder { get; set; }
        public long RegistrationToken { get; set; }
        public virtual ICollection<DeliveryDetail> DeliveryDetails { get; set; }
    }
}
