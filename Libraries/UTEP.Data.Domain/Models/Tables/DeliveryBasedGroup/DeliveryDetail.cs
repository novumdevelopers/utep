﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.DeliveryBasedGroup
{
    public class DeliveryDetail : BaseEntity
    {
        public long ShippingId { get; set; }
        public virtual Delivery Shipping { get; set; }
        public decimal DestinationTownLongitude { get; set; }
        public decimal DestinationTownLatitude { get; set; }
        public virtual DateTime? FinishDateTime { get; set; }
    }
}
