﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.CooperationBasedGroup
{
    public class Cooperation : BaseEntity
    {
        public string Name { get; set; }
        public string Logo { get; set; }
    }
}
