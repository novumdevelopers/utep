﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.SiteBasedGroup
{
    public class SiteTextTranslation : BaseEntity
    {
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Body1 { get; set; }
        public string Body2 { get; set; }
        public string Body3 { get; set; }
        public string ImagePath1 { get; set; }
        public string ImagePath2 { get; set; }
        public string ImagePath3 { get; set; }

        public long SiteLanguageId { get; set; }
        public SiteLanguage SiteLanguage { get; set; }

        public long SiteTextId { get; set; }
        public SiteText SiteText { get; set; }
    }
}
