﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.SiteBasedGroup
{
    public class SiteText : BaseEntity
    {
        public SiteText()
        {
            SiteTextTranslations = new HashSet<SiteTextTranslation>();
        }
        public ICollection<SiteTextTranslation> SiteTextTranslations { get; set; }

        public int Type { get; set; }
    }
}
