﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.Tables.SiteBasedGroup;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup
{
    public class SiteLanguage : BaseEntity
    {
        public SiteLanguage()
        {
            ProductTypeTranslations = new HashSet<ProductTypeTranslation>();
            VehicleTypeTranslations = new HashSet<VehicleTypeTranslation>();
            SiteTextTranslations = new HashSet<SiteTextTranslation>();
        }

        public string LanguageCode { get; set; }
        public string Name { get; set; }
        public int OrderIndex { get; set; }

        public virtual ICollection<ProductTypeTranslation> ProductTypeTranslations { get; set; }
        public virtual ICollection<VehicleTypeTranslation> VehicleTypeTranslations { get; set; }
        public virtual ICollection<SiteTextTranslation> SiteTextTranslations { get; set; }
    }
}
