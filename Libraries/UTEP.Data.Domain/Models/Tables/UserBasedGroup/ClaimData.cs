﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.UserBasedGroup
{
    public class ClaimData : BaseEntity
    {
        public static List<string> UserClaims { get; set; } = new List<string> {
            "User",
            "Business",
            "Report",
            "MonthlyPaymentReport",
            "Edit",
            "TextEdit",
            "TransactionEdit",
            "UserTypeEdit"
        };
    }
}
