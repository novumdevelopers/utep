﻿using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.UserBasedGroup
{
    public class UserGroupClaim : BaseEntity
    {
        public long UserGroupId { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public string ClaimData { get; set; }
    }
}
