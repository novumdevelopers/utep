﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.Tables.NotificationBasedGroup;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;
using UTEP.Settings;

namespace UTEP.Data.Domain.Models.Tables.UserBasedGroup
{
    public class User : IdentityUser<long>
    {
        public User()
        {
            DriverDestinations = new HashSet<DriverDestination>();
            NotificationTokens = new HashSet<NotificationToken>();
        }
        public bool IsEnabled { get; set; }
        public int TypeOfUser { get; set; } = TypeOfUsers.Others;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string About { get; set; }
        public string Address { get; set; }
        public byte Gender { get; set; }
        public string FacebookLink { get; set; }
        public string LinkedinLink { get; set; }
        public string TwitterLink { get; set; }

        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<IdentityUserRole<long>> Roles { get; set; } = new List<IdentityUserRole<long>>();

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<IdentityUserClaim<long>> Claims { get; } = new List<IdentityUserClaim<long>>();

        /// <summary>
        /// Navigation property for this users login accounts.
        /// </summary>
        public virtual ICollection<IdentityUserLogin<long>> Logins { get; } = new List<IdentityUserLogin<long>>();

        public long? UserGroupId { get; set; }
        public virtual UserGroup UserGroup { get; set; }
        public long? UserTypeId { get; set; }
        public virtual UserType UserType { get; set; }

        public Company Company { get; set; }
        public Driver Driver { get; set; }

        public virtual ICollection<DriverDestination> DriverDestinations { get; set; }
        public virtual ICollection<NotificationToken> NotificationTokens { get; set; }

    }
}
