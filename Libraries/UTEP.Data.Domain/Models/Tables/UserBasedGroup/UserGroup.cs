﻿using System;
using System.Collections.Generic;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.UserBasedGroup
{
    public class UserGroup
    {
        public UserGroup()
        {
            Users = new HashSet<User>();
            UserGroupClaims = new HashSet<UserGroupClaim>();
        }
       
        [System.ComponentModel.DataAnnotations.Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime AddedDate { get; set; } = DateTime.UtcNow;
        public DateTime? UpdatedDate { get; set; }
        public int Status { get; set; } = Settings.Status.Active;
        public string Description { get; set; }
        public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserGroupClaim> UserGroupClaims { get; set; }
    }
}
