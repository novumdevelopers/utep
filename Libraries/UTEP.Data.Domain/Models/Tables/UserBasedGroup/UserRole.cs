﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.Tables.UserBasedGroup
{
    public class UserRole<TKey> : IdentityUserRole<long> where TKey : IEquatable<TKey>
    {
        public TKey IdOfUser { get; set; }
        public virtual User User { get; set; }
    }
}
