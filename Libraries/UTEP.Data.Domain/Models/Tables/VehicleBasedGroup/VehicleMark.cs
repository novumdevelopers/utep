﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.VehicleBasedGroup
{
    public class VehicleMark : BaseEntity
    {
        public VehicleMark()
        {
            VehicleModels = new HashSet<VehicleModel>();
        }

        public string Name { get; set; }
        public string BrandLogo { get; set; }
        public virtual ICollection<VehicleModel> VehicleModels { get; set; }
    }
}
