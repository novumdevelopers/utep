﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.VehicleBasedGroup
{
    public class VehicleModel : BaseEntity
    {
        public VehicleModel()
        {
            Trailers = new HashSet<Trailer>();
            Trucks = new HashSet<Truck>();
        }

        public string Name { get; set; }
        public long VehicleMarkId { get; set; }
        public virtual VehicleMark VehicleMark { get; set; }
        public long VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }
        public virtual ICollection<Trailer> Trailers { get; set; }
        public virtual ICollection<Truck> Trucks { get; set; }

    }
}
