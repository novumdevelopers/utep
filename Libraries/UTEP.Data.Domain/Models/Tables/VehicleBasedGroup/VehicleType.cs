﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.VehicleBasedGroup
{
    public class VehicleType : BaseEntity
    {
        public VehicleType()
        {
            VehicleModels = new HashSet<VehicleModel>();
            VehicleTypeTranslations = new HashSet<VehicleTypeTranslation>();
        }
        public virtual ICollection<VehicleModel> VehicleModels { get; set; }
        public virtual ICollection<VehicleTypeTranslation> VehicleTypeTranslations { get; set; }
    }
}
