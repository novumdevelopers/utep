﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.VehicleBasedGroup
{
    public class VehicleTypeTranslation : BaseEntity
    {

        public long VehicleTypeId { get; set; }
        public virtual VehicleType VehicleType { get; set; }

        public long SiteLanguageId { get; set; }
        public SiteLanguage SiteLanguage { get; set; }

        public string Name { get; set; }
    }
}
