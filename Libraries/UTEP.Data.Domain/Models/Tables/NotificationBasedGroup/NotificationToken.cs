﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.NotificationBasedGroup
{
    public class NotificationToken : BaseEntity
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public string FCMToken { get; set; }
    }
}
