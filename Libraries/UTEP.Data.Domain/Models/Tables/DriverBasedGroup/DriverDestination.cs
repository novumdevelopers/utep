﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.DriverBasedGroup
{
    public class DriverDestination : BaseEntity
    {
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public virtual DateTime ExistingMomentDateTime { get; set; }
    }
}
