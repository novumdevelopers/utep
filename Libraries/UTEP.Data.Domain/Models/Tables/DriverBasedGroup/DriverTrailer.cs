﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.DriverBasedGroup
{
    public class DriverTrailer : BaseEntitySimple
    {
        public long DriverId { get; set; }
        public Driver Driver { get; set; }
        public long TrailerId { get; set; }
        public Trailer Trailer { get; set; }
    }
}
