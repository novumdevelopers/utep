﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.DriverBasedGroup
{
    public class DriverTruck : BaseEntitySimple
    {
        public long DriverId { get; set; }
        public Driver Driver { get; set; }
        public long TruckId { get; set; }
        public Truck Truck { get; set; }
    }
}
