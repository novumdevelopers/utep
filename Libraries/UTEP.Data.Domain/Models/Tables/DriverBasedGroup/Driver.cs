﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.DriverBasedGroup
{
    public class Driver : BaseEntity
    {
        public Driver()
        {
            DriverTrucks = new HashSet<DriverTruck>();
            DriverTrailers = new HashSet<DriverTrailer>();
            DriverOrderBiddings = new HashSet<DriverOrderBidding>();
            OfferToDrivers = new HashSet<OfferToDriver>();
            DriverResponses = new HashSet<DriverResponse>();
        }
        public long UserId { get; set; }
        public virtual User User { get; set; }
        //public long TruckId { get; set; }
        //public virtual Truck Truck { get; set; }
        //public long TrailerId { get; set; }
        //public virtual Trailer Trailer { get; set; }
        public virtual Guid RegistrationToken { get; set; }
        public long CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public virtual ICollection<DriverOrderBidding> DriverOrderBiddings { get; set; }
        public virtual ICollection<OfferToDriver> OfferToDrivers { get; set; }
        public virtual ICollection<DriverTrailer> DriverTrailers { get; set; }
        public virtual ICollection<DriverTruck> DriverTrucks { get; set; }
        public virtual ICollection<DriverResponse> DriverResponses { get; set; }
    }
}
