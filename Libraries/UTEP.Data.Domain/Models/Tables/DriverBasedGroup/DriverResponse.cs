﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.DriverBasedGroup
{
    public class DriverResponse : BaseEntity
    {
        public long DriverId { get; set; }
        public Driver Driver { get; set; }
        public long TruckId { get; set; }
        public Truck Truck { get; set; }
        public long TrailerId { get; set; }
        public Trailer Trailer { get; set; }
        public long CompanyOrderId { get; set; }
        public CompanyOrder CompanyOrder { get; set; }
        public decimal EstimatePrice { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public int DayCount { get; set; }
    }
}
