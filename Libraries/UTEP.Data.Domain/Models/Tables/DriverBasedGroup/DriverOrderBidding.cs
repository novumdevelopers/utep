﻿using System;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.DriverBasedGroup
{
    public class DriverOrderBidding : BaseEntity
    {
        public long DriverId { get; set; }
        public virtual Driver Driver { get; set; }
        public long OfferToDriverId { get; set; }
        public virtual OfferToDriver OfferToDriver { get; set; }
        public decimal SuggestedPrice { get; set; }
    }
}
