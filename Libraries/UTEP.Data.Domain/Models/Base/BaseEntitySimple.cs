﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.Base
{
    public class BaseEntitySimple
    {
        public DateTime AddedDate { get; set; } = DateTime.UtcNow;
        public DateTime? UpdatedDate { get; set; }
        public int Status { get; set; } = Settings.Status.Active;
        public string AdditionalDescription { get; set; }
    }
}
