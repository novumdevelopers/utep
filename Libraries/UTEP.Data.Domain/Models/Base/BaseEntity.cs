﻿using System;
using UTEP.Settings;

namespace UTEP.Data.Domain.Models.Base
{
    public class BaseEntity
    {
        [System.ComponentModel.DataAnnotations.Key]
        public long Id { get; set; }
        public DateTime AddedDate { get; set; } = AzDateTime.Convert(DateTime.UtcNow);
        public DateTime? UpdatedDate { get; set; }
        public int Status { get; set; } = Settings.Status.Active;
        public string AdditionalDescription { get; set; }
    }
}
