﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.CompanyViewModels
{
    public class CompanyIndexListViewModel
    {
        public long Id { get; set; }
        public CompanyTypeForView CompanyType { get; set; }

        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyAddress { get; set; }

        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public DateTime AddedDate { get; set; }
    }

    public class CompanyTypeForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
