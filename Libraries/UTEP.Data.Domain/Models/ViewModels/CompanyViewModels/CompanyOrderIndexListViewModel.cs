﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;

namespace UTEP.Data.Domain.Models.ViewModels.CompanyViewModels
{
    public class CompanyOrderIndexListViewModel
    {
        public CompanyOrderIndexListViewModel()
        {
            Products = new HashSet<Product>();
            ProductImages = new HashSet<ProductImage>();
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Size { get; set; }
        public decimal Tonnage { get; set; }
        public int PaletteCount { get; set; }
        public decimal ProporsalPrice { get; set; }
        public int Currency { get; set; }
        public string Weight { get; set; }
        public CompanyForView Company { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime DealDate { get; set; }
        public DateTime AddedDate { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public IEnumerable<Product> Products { get; set; }
        public IEnumerable<ProductImage> ProductImages { get; set; }
    }

    public class CompanyForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
