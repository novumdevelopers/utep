﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.CompanyViewModels
{
    public class CompanyShippingIndexListViewModel
    {
        public long Id { get; set; }
        public CompanyOrderForView CompanyOrder{ get; set; }
        public decimal OriginTownLongitude { get; set; }
        public decimal OriginTownLatitude { get; set; }
        public decimal DestinationTownLongitude { get; set; }
        public decimal DestinationTownLatitude { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime AddedDate { get; set; }
    }

    public class CompanyOrderForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
