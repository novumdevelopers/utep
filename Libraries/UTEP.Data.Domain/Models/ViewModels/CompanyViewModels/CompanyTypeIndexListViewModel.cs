﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.CompanyViewModels
{
    public class CompanyTypeIndexListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
