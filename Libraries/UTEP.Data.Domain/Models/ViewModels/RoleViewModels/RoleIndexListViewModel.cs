﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.RoleViewModels
{
    public class RoleIndexListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
