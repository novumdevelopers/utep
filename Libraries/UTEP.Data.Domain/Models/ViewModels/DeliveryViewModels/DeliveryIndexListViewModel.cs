﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.DeliveryViewModels
{
    public class DeliveryIndexListViewModel
    {
        public long Id { get; set; }
        public CompanyOrderForView CompanyOrder { get; set; }
        public DateTime AddedDate { get; set; }
    }

    public class CompanyOrderForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
