﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.DeliveryViewModels
{
    public class DeliveryDetailIndexListViewModel
    {
        public long Id { get; set; }
        public DeliveryForView Delivery { get; set; }
        public decimal DestinationTownLongitude { get; set; }
        public decimal DestinationTownLatitude { get; set; }
        public DateTime? FinishDate { get; set; }
        public DateTime AddedDate { get; set; }
    }

    public class DeliveryForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
