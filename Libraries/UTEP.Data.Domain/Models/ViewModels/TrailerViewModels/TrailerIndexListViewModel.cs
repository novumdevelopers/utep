﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.TrailerViewModels
{
    public class TrailerIndexListViewModel
    {
        public long Id { get; set; }
        public decimal Tonnage { get; set; }
        public decimal PaletteCount { get; set; }
        public string RegistrationNumber { get; set; }
        public string RegistrationCertificate { get; set; }
        public DateTime AddedDate { get; set; }
        public TrailerTypeForView TrailerType { get; set; }
        public VehicleModelForView VehicleModel { get; set; }
        public string Image { get; set; }
    }

    public class TrailerTypeForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }

    public class VehicleModelForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
