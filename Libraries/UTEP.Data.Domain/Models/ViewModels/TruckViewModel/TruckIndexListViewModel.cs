﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.TruckViewModel
{
    public class TruckIndexListViewModel
    {
        public long  Id { get; set; }
        public string RegistrationNumber { get; set; }
        public string RegistrationCertificate { get; set; }
        public DateTime YearOf { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
