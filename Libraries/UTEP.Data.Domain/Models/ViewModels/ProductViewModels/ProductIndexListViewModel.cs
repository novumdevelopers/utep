﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.ProductViewModels
{
    public class ProductIndexListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Size { get; set; }
        public decimal Tonnage { get; set; }
        public decimal PaletteCount { get; set; }
        public ProductTypeForView ProductType { get; set; }
        public CompanyForView Company { get; set; }
        public DateTime AddedDate { get; set; }
        
    }

    public class CompanyForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }

    public class ProductTypeForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
