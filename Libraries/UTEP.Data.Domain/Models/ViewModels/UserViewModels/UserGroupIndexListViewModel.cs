﻿using System;

namespace UTEP.Core.Repository.Implementation.Repositories.UserBasedGroup
{
    public class UserGroupIndexListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime AddedDate { get; set; }
    }
}