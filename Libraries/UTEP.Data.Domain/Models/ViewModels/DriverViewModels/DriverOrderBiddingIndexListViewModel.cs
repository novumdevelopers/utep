﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.DriverViewModels
{
    public class DriverOrderBiddingIndexListViewModel
    {
        public long Id { get; set; }
        public string DriverName { get; set; }
        public string OfferToDriverName { get; set; }
        public decimal SuggestedPrice { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
