﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.DriverViewModels
{
    public class DriverResponseIndexListViewModel
    {
        public long Id { get; set; }
        public DriverForView Driver { get; set; }
        public TruckForView Truck { get; set; }
        public TrailerForView Trailer { get; set; }
        public CompanyOrderForView CompanyOrder { get; set; }
        public string EstimatePrice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DayCount { get; set; }
        public DateTime AddedDate { get; set; }
    }

    public class DriverForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }

    public class TruckForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }

    public class TrailerForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }

    public class CompanyOrderForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
