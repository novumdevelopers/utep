﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.CooperationViewModels
{
    public class CooperationIndexListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
