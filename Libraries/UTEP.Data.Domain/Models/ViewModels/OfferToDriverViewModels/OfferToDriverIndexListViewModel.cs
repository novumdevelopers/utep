﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.OfferToDriverViewModels
{
    public class OfferToDriverIndexListViewModel
    {
        public long Id { get; set; }
        public DriverForView Driver { get; set; }
        public CompanyOrderForView CompanyOrder { get; set; }
        public DateTime AddedDate { get; set; }
    }

    public class CompanyOrderForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }

    public class DriverForView
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
