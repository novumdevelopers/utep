﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.VehicleViewModels
{
    public class VehicleModelIndexListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime AddedDate { get; set; }
        public VehicleMarkForView VehicleMark { get; set; }
        public VehicleTypeForView VehicleType { get; set; }
    }

    public class VehicleMarkForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }

    public class VehicleTypeForView
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
