﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Data.Domain.Models.ViewModels.VehicleViewModels
{
    public class VehicleMarkIndexListViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string BrandLogo { get; set; }
        public DateTime AddedDate { get; set; }
    }
}
