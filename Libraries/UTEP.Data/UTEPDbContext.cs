﻿using Microsoft.EntityFrameworkCore;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using System.Linq;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.Tables;
using UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup;
using UTEP.Data.Domain.Models.Tables.DeliveryBasedGroup;
using UTEP.Data.Domain.Models.Tables.NotificationBasedGroup;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using UTEP.Data.Domain.Models.Tables.SiteBasedGroup;
using UTEP.Data.Domain.Models.Tables.SubscribeBasedGroup;
using UTEP.Data.Domain.Models.Tables.CooperationBasedGroup;
using UTEP.Data.Domain.Models.Tables.CountryBasedGroup;

namespace UTEP.Data
{
    public class UTEPDbContext : IdentityDbContext<User, Role, long>
    {
        private readonly DbContextOptions<UTEPDbContext> _options;
        public UTEPDbContext(DbContextOptions<UTEPDbContext> options) : base(options)
        {
            _options = options;
        }


        /*
         User
        */
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<UserGroupClaim> UserGroupClaims { get; set; }


        /*
         Location
        */
        //public DbSet<Location> Locations { get; set; }
        //public DbSet<LocationType> LocationTypes { get; set; }


        /*
         Company
        */
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyType> CompanyTypes { get; set; }
        public DbSet<CompanyOrder> CompanyOrders { get; set; }


        /*
         Countries
        */
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }

        /*
         Cooperation
        */
        public DbSet<Cooperation> Cooperations { get; set; }

        /*
         Vehicle
        */
        public DbSet<VehicleType> VehicleTypes { get; set; }
        public DbSet<VehicleModel> VehicleModels { get; set; }
        public DbSet<VehicleMark> VehicleMarks { get; set; }
        public DbSet<VehicleTypeTranslation> VehicleTypeTranslations { get; set; }


        /*
         Trailer 
        */
        public DbSet<TrailerType> TrailerTypes { get; set; }
        public DbSet<Trailer> Trailers { get; set; }


        /*
         Truck 
        */
        public DbSet<Truck> Trucks { get; set; }


        //NotificitionToken
        public DbSet<NotificationToken> NotificationTokens { get; set; }


        /*
         Delivery 
        */
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<DeliveryDetail> DeliveryDetails { get; set; }

        /*
         Product
        */
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<ProductTypeTranslation> ProductTypeTranslations { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }


        /*
         Driver 
        */
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<DriverDestination> DriverDestinations { get; set; }
        public DbSet<DriverOrderBidding> DriverOrderBiddings { get; set; }
        public DbSet<DriverResponse> DriverResponses { get; set; }
        public DbSet<DriverTruck> DriverTrucks { get; set; }
        public DbSet<DriverTrailer> DriverTrailers { get; set; }

        /*
         Offer 
        */
        public DbSet<OfferToDriver> OfferToUsers { get; set; }

        /*
        SiteLanguage
        */
        public DbSet<SiteLanguage> SiteLanguages { get; set; }

        /*
        SiteText
        */
        public DbSet<SiteText> SiteTexts { get; set; }
        public DbSet<SiteTextTranslation> SiteTextTranslations { get; set; }

        /*
        Subscribe
        */
        public DbSet<Subscribe> Subscribes { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);


            /*
             Location
            */
            //builder.Entity<LocationType>(options =>
            //{
            //    options.Property(obj => obj.Name)
            //        .IsRequired();
            //    options.Property(obj => obj.Status)
            //        .IsRequired();
            //});

            //builder.Entity<Location>(options =>
            //{
            //    options.Property(obj => obj.Name)
            //        .IsRequired();
            //    options.Property(obj => obj.LocationTypeId)
            //        .IsRequired();
            //    options.Property(obj => obj.Status)
            //        .IsRequired();

            //    options.HasOne(obj => obj.Parent)
            //        .WithMany(obj => obj.Children)
            //        .HasForeignKey(obj => obj.ParentId);

            //    options.HasOne(obj => obj.LocationType)
            //        .WithMany(obj => obj.Locations)
            //        .HasForeignKey(obj => obj.LocationTypeId);
            //});


            /*
             Country
             */

            builder.Entity<Country>(options =>
            {
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();

                options.HasMany(obj => obj.Cities)
                    .WithOne(obj => obj.Country)
                    .HasForeignKey(obj => obj.CountryId);
            });

            builder.Entity<City>(options =>
            {
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
            });


            /*
             Company
            */
            builder.Entity<CompanyType>(options =>
            {
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();
            });

            builder.Entity<Company>(options =>
            {
                options.Property(obj => obj.CompanyTypeId)
                    .IsRequired();
                options.Property(obj => obj.TaxIdentificationNumber)
                    .IsRequired();
                options.Property(obj => obj.TypeOfCompany)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();
                // Company Info
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.Address)
                    .IsRequired();
                options.Property(obj => obj.LocationLongitude)
                    .IsRequired();
                options.Property(obj => obj.LocationLatitude)
                    .IsRequired();
                options.Property(obj => obj.Email)
                    .IsRequired();
                options.Property(obj => obj.Phone)
                    .IsRequired();
                options.Property(obj => obj.Logo)
                    .IsRequired();

                options.HasOne(obj => obj.CompanyType)
                    .WithMany(obj => obj.Companies)
                    .HasForeignKey(obj => obj.CompanyTypeId);

            });

            builder.Entity<Product>(options =>
            {
                options.Property(obj => obj.ProductTypeId)
                    .IsRequired();
                options.Property(obj => obj.CompanyId)
                    .IsRequired();
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.Size)
                    .IsRequired();
                options.Property(obj => obj.Tonnage)
                    .IsRequired();
                options.Property(obj => obj.PaletteCount)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                options.HasOne(obj => obj.Company)
                   .WithMany(obj => obj.CompanyProducts)
                   .HasForeignKey(obj => obj.CompanyId);

                options.HasOne(obj => obj.ProductType)
                   .WithMany(obj => obj.CompanyProducts)
                   .HasForeignKey(obj => obj.ProductTypeId);

                options.HasOne(obj => obj.CompanyOrder)
                   .WithMany(obj => obj.Products)
                   .HasForeignKey(obj => obj.CompanyOrderId);
            });

            builder.Entity<ProductTypeTranslation>(options =>
            {
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.HasOne(obj => obj.ProductType)
                    .WithMany(obj => obj.ProductTypeTranslations)
                    .HasForeignKey(obj => obj.ProductTypeId);
            });

            builder.Entity<ProductImage>(options =>
            {
                options.Property(obj => obj.MainPath)
                    .IsRequired();
                options.Property(obj => obj.MediumPath)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                    .IsRequired();
            });

            builder.Entity<CompanyOrder>(options =>
            {
                options.Property(obj => obj.RegistrationToken)
                    .IsRequired();
                options.Property(obj => obj.DealDate)
                    .IsRequired();
                //options.Property(obj => obj.ProporsalPrice)
                //    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.From)
                     .IsRequired();
                options.Property(obj => obj.To)
                     .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();
            });

            builder.Entity<CompanyShipping>(options =>
            {
                options.Property(obj => obj.CompanyOrderId)
                    .IsRequired();
                options.Property(obj => obj.OriginTownLongitude)
                    .IsRequired();
                options.Property(obj => obj.OriginTownLatitude)
                    .IsRequired();
                options.Property(obj => obj.DestinationTownLongitude)
                    .IsRequired();
                options.Property(obj => obj.DestinationTownLatitude)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                options.HasOne(obj => obj.CompanyOrder)
                    .WithMany(obj => obj.CompanyShippings)
                    .HasForeignKey(obj => obj.CompanyOrderId);
            });


            /*
             Cooperation
            */
            builder.Entity<Cooperation>(options =>
            {
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.Logo)
                    .IsRequired();
            });


            /*
             Vehicle
            */
            builder.Entity<VehicleType>(options =>
            {
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();
                options.HasMany(obj => obj.VehicleTypeTranslations)
                    .WithOne(obj => obj.VehicleType)
                    .HasForeignKey(obj => obj.VehicleTypeId);
            });

            builder.Entity<VehicleTypeTranslation>(options =>
            {
                options.Property(obj => obj.Name)
                    .IsRequired();
            });

            builder.Entity<VehicleModel>(options =>
            {
                options.Property(obj => obj.Name)
                     .IsRequired();
                options.Property(obj => obj.VehicleMarkId)
                     .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                options.HasOne(obj => obj.VehicleMark)
                    .WithMany(obj => obj.VehicleModels)
                    .HasForeignKey(obj => obj.VehicleMarkId);

                options.HasOne(obj => obj.VehicleType)
                    .WithMany(obj => obj.VehicleModels)
                    .HasForeignKey(obj => obj.VehicleTypeId);
            });

            builder.Entity<VehicleMark>(options =>
            {
                options.Property(obj => obj.Name)
                   .IsRequired();
                options.Property(obj => obj.BrandLogo)
                     .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();
                options.Property(obj => obj.Status)
                     .IsRequired();
            });


            //NotificationToken
            builder.Entity<NotificationToken>(options =>
            {
                options.Property(x => x.UserId)
                    .IsRequired();
                options.Property(x => x.FCMToken)
                    .IsRequired();
                options.Property(x => x.Status)
                    .IsRequired();
                options.HasOne(x => x.User)
                    .WithMany(x => x.NotificationTokens)
                    .HasForeignKey(x => x.UserId);
            });


            /*
             Trailer 
            */
            builder.Entity<TrailerType>(options =>
            {
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();
            });

            builder.Entity<Trailer>(options =>
            {
                options.Property(obj => obj.VehicleModelId)
                   .IsRequired();
                options.Property(obj => obj.TrailerTypeId)
                   .IsRequired();
                options.Property(obj => obj.YearOf)
                   .IsRequired();
                options.Property(obj => obj.Size)
                   .IsRequired();
                options.Property(obj => obj.Tonnage)
                   .IsRequired();
                options.Property(obj => obj.PaletteCount)
                   .IsRequired();
                options.Property(obj => obj.RegistrationNumber)
                   .IsRequired();
                options.Property(obj => obj.RegistrationCertificate)
                   .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                options.HasOne(obj => obj.TrailerType)
                   .WithMany(obj => obj.Trailers)
                   .HasForeignKey(obj => obj.TrailerTypeId);

                options.HasOne(obj => obj.VehicleModel)
                   .WithMany(obj => obj.Trailers)
                   .HasForeignKey(obj => obj.VehicleModelId);
            });

            builder.Entity<Truck>(options =>
            {
                options.Property(obj => obj.VehicleModelId)
                  .IsRequired();
                options.Property(obj => obj.YearOf)
                   .IsRequired();
                options.Property(obj => obj.RegistrationNumber)
                   .IsRequired();
                options.Property(obj => obj.RegistrationCertificate)
                   .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                options.HasOne(obj => obj.VehicleModel)
                   .WithMany(obj => obj.Trucks)
                   .HasForeignKey(obj => obj.VehicleModelId);
            });


            /*
             Delivery 
            */
            builder.Entity<Delivery>(options =>
            {
                options.Property(obj => obj.CompanyOrderId)
                    .IsRequired();
                options.Property(obj => obj.Status)
                     .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();
                options.Property(obj => obj.RegistrationToken)
                    .IsRequired();
            });

            builder.Entity<DeliveryDetail>(options =>
            {
                options.Property(obj => obj.ShippingId)
                    .IsRequired();
                options.Property(obj => obj.DestinationTownLatitude)
                    .IsRequired();
                options.Property(obj => obj.DestinationTownLongitude)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                options.HasOne(obj => obj.Shipping)
                    .WithMany(obj => obj.DeliveryDetails)
                    .HasForeignKey(obj => obj.ShippingId);
            });


            /*
             Driver
             */
            builder.Entity<Driver>(options =>
            {
                options.Property(obj => obj.UserId)
                     .IsRequired();
                //options.Property(obj => obj.TruckId)
                //     .IsRequired();
                //options.Property(obj => obj.TrailerId)
                //     .IsRequired();
                options.Property(obj => obj.RegistrationToken)
                     .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                //options.HasOne(obj => obj.Truck)
                //   .WithMany(obj => obj.Drivers)
                //   .HasForeignKey(obj => obj.TruckId);

                //options.HasOne(obj => obj.Trailer)
                //   .WithMany(obj => obj.Drivers)
                //   .HasForeignKey(obj => obj.TrailerId);
            });

            builder.Entity<DriverTruck>(options =>
            {
                options.HasKey(x => new
                {
                    x.DriverId,
                    x.TruckId
                });

                options.HasOne(x => x.Truck)
                    .WithMany(x => x.DriverTrucks)
                    .HasForeignKey(x => x.TruckId);

                options.HasOne(x => x.Driver)
                    .WithMany(x => x.DriverTrucks)
                    .HasForeignKey(x => x.DriverId);
            });

            builder.Entity<DriverTrailer>(options =>
            {
                options.HasKey(x => new
                {
                    x.DriverId,
                    x.TrailerId
                });

                options.HasOne(x => x.Trailer)
                    .WithMany(x => x.DriverTrailers)
                    .HasForeignKey(x => x.TrailerId);

                options.HasOne(x => x.Driver)
                    .WithMany(x => x.DriverTrailers)
                    .HasForeignKey(x => x.DriverId);
            });

            builder.Entity<DriverDestination>(options =>
            {
                options.Property(obj => obj.UserId)
                    .IsRequired();
                options.Property(obj => obj.Longitude)
                    .IsRequired();
                options.Property(obj => obj.Latitude)
                    .IsRequired();
                options.Property(obj => obj.ExistingMomentDateTime)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();

                options.HasOne(obj => obj.User)
                   .WithMany(obj => obj.DriverDestinations)
                   .HasForeignKey(obj => obj.UserId);
            });

            builder.Entity<DriverResponse>(options =>
            {
                options.Property(x => x.DriverId)
                    .IsRequired();
                options.Property(x => x.TrailerId)
                    .IsRequired();
                options.Property(x => x.TruckId)
                    .IsRequired();
                options.Property(x => x.CompanyOrderId)
                    .IsRequired();
                options.Property(x => x.EstimatePrice)
                    .IsRequired();
                options.Property(x => x.StartDate)
                    .IsRequired();
                options.Property(x => x.EndDate)
                    .IsRequired();
                options.Property(x => x.DayCount)
                    .IsRequired();

                options.HasOne(x => x.Driver)
                    .WithMany(x => x.DriverResponses)
                    .HasForeignKey(x => x.DriverId);

                options.HasOne(x => x.Truck)
                    .WithMany(x => x.DriverResponses)
                    .HasForeignKey(x => x.TruckId);


                options.HasOne(x => x.Trailer)
                    .WithMany(x => x.DriverResponses)
                    .HasForeignKey(x => x.TrailerId);
            });

            builder.Entity<DriverOrderBidding>(options =>
            {
                options.Property(obj => obj.DriverId)
                    .IsRequired();
                options.Property(obj => obj.OfferToDriverId)
                    .IsRequired();
                options.Property(obj => obj.SuggestedPrice)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();

                options.HasOne(obj => obj.Driver)
                   .WithMany(obj => obj.DriverOrderBiddings)
                   .HasForeignKey(obj => obj.DriverId);
            });


            builder.Entity<OfferToDriver>(options =>
            {
                options.Property(obj => obj.DriverId)
                    .IsRequired();
                options.Property(obj => obj.CompanyOrderId)
                    .IsRequired();
                options.Property(obj => obj.Status)
                    .IsRequired();
                options.Property(obj => obj.AddedDate)
                     .IsRequired();

                options.HasOne(obj => obj.Driver)
                    .WithMany(obj => obj.OfferToDrivers)
                    .HasForeignKey(obj => obj.DriverId);

                options.HasOne(obj => obj.CompanyOrder)
                    .WithMany(obj => obj.OfferToDrivers)
                    .HasForeignKey(obj => obj.CompanyOrderId);
            });

            builder.Entity<SiteLanguage>(options =>
            {
                options.Property(obj => obj.Name)
                    .IsRequired();
                options.Property(obj => obj.LanguageCode)
                    .IsRequired();
                options.Property(obj => obj.OrderIndex)
                    .IsRequired();
                options.HasMany(obj => obj.ProductTypeTranslations)
                    .WithOne(obj => obj.SiteLanguage)
                    .HasForeignKey(obj => obj.SiteLanguageId);
                options.HasMany(obj => obj.VehicleTypeTranslations)
                    .WithOne(obj => obj.SiteLanguage)
                    .HasForeignKey(obj => obj.SiteLanguageId);
                options.HasMany(obj => obj.SiteTextTranslations)
                    .WithOne(obj => obj.SiteLanguage)
                    .HasForeignKey(obj => obj.SiteLanguageId);
            });

            builder.Entity<SiteText>(options =>
            {
                options.HasMany(obj => obj.SiteTextTranslations)
                    .WithOne(obj => obj.SiteText)
                    .HasForeignKey(obj => obj.SiteTextId);
            });

            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }


            /*
            User
           */

            builder.Entity<User>(options =>
            {
                options.HasOne(obj => obj.Company)
                    .WithOne(obj => obj.User)
                    .HasForeignKey<Company>(obj => obj.UserId);
                options.HasOne(obj => obj.Driver)
                    .WithOne(obj => obj.User)
                    .HasForeignKey<Driver>(obj => obj.UserId);
                options.HasOne(obj => obj.UserType)
                   .WithMany(obj => obj.Users)
                   .HasForeignKey(obj => obj.UserTypeId);
                options.HasOne(obj => obj.UserGroup)
                   .WithMany(obj => obj.Users)
                   .HasForeignKey(obj => obj.UserGroupId);
            });

            //Identity
            builder.Entity<User>(options =>
            {
                options.HasMany(obj => obj.Claims)
                    .WithOne()
                    .HasForeignKey(obj => obj.UserId)
                    .IsRequired()
                    .OnDelete(DeleteBehavior.Cascade);

                options.HasMany(obj => obj.Logins)
                    .WithOne()
                    .HasForeignKey(obj => obj.UserId)
                    .IsRequired()
                    .OnDelete(DeleteBehavior.Cascade);

                options.HasMany(obj => obj.Roles)
                    .WithOne()
                    .HasForeignKey(obj => obj.UserId)
                    .IsRequired()
                    .OnDelete(DeleteBehavior.Cascade);
            });



        }
    }
}
