﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
namespace UTEP.Data
{
    public class UTEPDbContextFactory : IDesignTimeDbContextFactory<UTEPDbContext>
    {
        public UTEPDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<UTEPDbContext>();
            builder.UseSqlServer(
                "Data Source = SQL6003.site4now.net;Initial Catalog=DB_A493B0_utep; User Id=DB_A493B0_utep_admin; Password = utepasAS12_; MultipleActiveResultSets=true;"
                               //"Server=NICAT\\SQLEXPRESS;Database=UTEP;Trusted_Connection=True;"
                               );
            return new UTEPDbContext(builder.Options);
        }
    }
}
