﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UTEP.Data.Migrations
{
    public partial class AddcountryColToOrderTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CountryFrom",
                table: "CompanyOrders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CountryTo",
                table: "CompanyOrders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CountryFrom",
                table: "CompanyOrders");

            migrationBuilder.DropColumn(
                name: "CountryTo",
                table: "CompanyOrders");
        }
    }
}
