﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UTEP.Data.Migrations
{
    public partial class AddWeightType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WeightType",
                table: "Products",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WeightType",
                table: "Products");
        }
    }
}
