﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace UTEP.Data
{
   public class Startup
    {
        public static class ServiceCollectionExtensions
        {
            public static void AddEntityFramework(IServiceCollection services, string connectionString)
            {
                services.AddDbContext<UTEPDbContext>(options =>
                        options.UseSqlServer(connectionString));
            }
        }
    }
}
