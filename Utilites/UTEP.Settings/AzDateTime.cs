﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Settings
{
    public static class AzDateTime
    {
        public static DateTime Convert(DateTime dateTimeUTC)
        {
            return TimeZoneInfo.ConvertTime(dateTimeUTC, TimeZoneInfo.FindSystemTimeZoneById("Azerbaijan Standard Time"));
        }
    }
}
