﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Settings
{
    public static class Weight
    {
        public const int Kg = 1;
        public const int Ton = 2;

        public static string GetWeight(int num)
        {
            string weight = "";
            switch (num)
            {
                case Weight.Kg:
                    weight = "Kg";
                    break;
                case Weight.Ton:
                    weight = "Ton";
                    break;
                default:
                    break;
            }
            return weight;
        }
    }

    public static class WeightType
    {
        public const int Palet = 1;
        public const int Yeşik = 2;
        public const int Kisə = 3;

        public static string GetWeightType(int num, string lang)
        {
            string wt = "";
            if (lang == "az")
            {
                switch (num)
                {
                    case WeightType.Palet:
                        wt = "Palet";
                        break;
                    case WeightType.Yeşik:
                        wt = "Yeşik";
                        break;
                    case WeightType.Kisə:
                        wt = "Kisə";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (num)
                {
                    case WeightType.Palet:
                        wt = "Палет";
                        break;
                    case WeightType.Yeşik:
                        wt = "Ящики";
                        break;
                    case WeightType.Kisə:
                        wt = "Сумка";
                        break;
                    default:
                        break;
                }
            }
            return wt;
        }
    }

}
