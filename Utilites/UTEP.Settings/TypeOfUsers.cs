﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Settings
{
    public static class TypeOfUsers
    {
        public const int Others = 0;
        public const int CompanyConfirmed = 1;
        public const int CompanyNotConfirmed = 2;
        public const int DriverConfirmed = 3;
        public const int DriverNotConfirmed = 4;
    }
}
