﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Settings
{
    public static class Currency
    {
        public const int AZN = 1;
        public const int USD = 2;
        public const int EURO = 3;

        public static string GetCurrency(int num)
        {
            string curr;
            switch (num)
            {
                case Currency.AZN:
                    curr = "AZN";
                    break;
                case Currency.USD:
                    curr = "USD";
                    break;
                case Currency.EURO:
                default:
                    curr = "EURO";
                    break;
            }
            return curr;
        }


    }
}
