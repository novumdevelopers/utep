﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Settings
{
    public static class SiteTextType
    {
        public const int About = 0;
        public const int Main = 1;
        public const int Contact = 2;
    }
}
