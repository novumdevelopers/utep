﻿using System;

namespace UTEP.Settings
{
    public static class Status
    {
        public const int All = 0;
        public const int Active = 1;
        public const int Deleted = 2;
        public const int Blocked = 3;
        public const int Published = 4;
        public const int Waited = 5;
        public const int End = 6;
    }
}
