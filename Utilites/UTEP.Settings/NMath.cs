﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTEP.Settings
{
    public static class NMath
    {
        public static int Adjust(double input)
        {
            double whole = Math.Truncate(input);
            double remainder = input - whole;
            if (remainder == 0)
            {
                remainder = 0;
            }
            else
            {
                remainder = 1;
            }
            return (int)(whole + remainder);
        }

    }
}
