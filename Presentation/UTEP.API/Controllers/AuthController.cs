﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace UTEP.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [HttpPost("token")]
        public IActionResult Token()
        {
            var header = Request.Headers["Authorization"];
            if (header.ToString().StartsWith("Basic"))
            {
                var credValue = header.ToString().Substring("basic".Length).Trim();
                var userCredential = Encoding.UTF8.GetString(Convert.FromBase64String(credValue));
                var userName = userCredential.Split(":");
                if (userName[0] == "admin")
                {
                    var token = new JwtSecurityToken(
                        issuer: "localhost:44358",
                        audience: "localhost:44358",
                        expires: DateTime.Now.AddMinutes(10),
                        claims: new[] { new Claim(ClaimTypes.Name, "admin") },
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes("mySecurityKeyFromApp.Config")),
                            SecurityAlgorithms.HmacSha256Signature)
                        );
                    string tokenString = new JwtSecurityTokenHandler().WriteToken(token);
                    return Ok( tokenString );
                }

            }
            return BadRequest("username duzgun deyil");




            //var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes("super secret key");
            //var tokenDescriptor = new SecurityTokenDescriptor

            //{
            //    Subject = new ClaimsIdentity(new Claim[]{
            //        new Claim(ClaimTypes.Name, "username")
            //    }),
            //    Expires = DateTime.Now.AddDays(1),
            //    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
            //        SecurityAlgorithms.HmacSha512Signature)
            //};
            //var token = tokenHandler.CreateToken(tokenDescriptor);
            //var tokenString = tokenHandler.WriteToken(token);

            //return Ok(new { tokenString });



        }
    }
}