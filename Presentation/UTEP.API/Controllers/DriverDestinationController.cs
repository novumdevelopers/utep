﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Settings;

namespace UTEP.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverResponseController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        public DriverResponseController(
            DbContextOptions<UTEPDbContext> contextOptions
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        [HttpGet]
        public IActionResult Get()
        {
            var list = _unitOfWork.DriverResponses.Find(x => x.Status == Status.Active).ToList();
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult GetdriverDestination([FromRoute] long id)
        {
            if (ModelState.IsValid)
            {
                var driverResponse = _unitOfWork.DriverResponses.Get(x => x.Id == id && x.Status == Status.Active);

                if (driverResponse != null)
                {
                    return Ok(driverResponse);
                }
            }
            return BadRequest(ModelState);

        }

        [HttpPost("{id}")]
        public IActionResult Edit([FromRoute] long id, [FromBody] DriverResponse model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            var driverResponse = _unitOfWork.DriverResponses.Get(x => x.Id == id && x.Status == Status.Active);
            driverResponse.DayCount = model.DayCount;
            driverResponse.EndDate = model.EndDate;
            driverResponse.EstimatePrice = model.EstimatePrice;
            driverResponse.StartDate= model.StartDate;
            driverResponse.TrailerId = model.TrailerId;
            driverResponse.TruckId = model.TruckId;

            try
            {
                _unitOfWork.DriverResponses.Update(driverResponse);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DriverDestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public IActionResult Post([FromBody] DriverResponse driverResponse)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _unitOfWork.DriverResponses.Add(driverResponse);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return CreatedAtAction("GetdriverDestination", new { id = driverResponse.Id }, driverResponse);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var driverResponse = _unitOfWork.DriverResponses.Get(x => x.Id == id && x.Status == Status.Active);
            if (driverResponse != null)
            {
                driverResponse.Status = Status.Deleted;
                try
                {
                    _unitOfWork.DriverResponses.Update(driverResponse);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return NotFound();
        }


        private bool DriverDestionExists(long id)
        {
            return _unitOfWork.DriverResponses.Any(e => e.Id == id);
        }

    }
}