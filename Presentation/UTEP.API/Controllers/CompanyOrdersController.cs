﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Settings;

namespace UTEP.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyOrdersController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        public CompanyOrdersController(
            DbContextOptions<UTEPDbContext> contextOptions,
            UserManager<User> userManager
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        // GET: api/CompanyOrders
        [HttpGet]
        public IActionResult GetCompanyOrders()
        {
            var list = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).ToList();
            return Ok(list);
        }

        // GET: api/CompanyOrders/5
        [HttpGet("{id}")]
        public IActionResult GetCompanyOrder([FromRoute] long id)
        {
            if (ModelState.IsValid)
            {
                var companyOrder = _unitOfWork.CompanyOrders.Get(x => x.Id == id && x.Status == Status.Active);

                if (companyOrder != null)
                {
                    return Ok(companyOrder);
                }
            }

            return BadRequest(ModelState);
        }

        // PUT: api/CompanyOrders/5
        [HttpPost("{id}")]
        public IActionResult Edit([FromRoute] long id, [FromBody] CompanyOrder model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            var companyOrder = _unitOfWork.CompanyOrders.Get(x => x.Id == id && x.Status == Status.Active);
            companyOrder.ProporsalPrice = model.ProporsalPrice;
            companyOrder.OriginTownLatitude = model.OriginTownLatitude;
            companyOrder.OriginTownLongitude = model.OriginTownLongitude;
            companyOrder.DestinationTownLatitude = model.DestinationTownLatitude;
            companyOrder.DestinationTownLongitude = model.DestinationTownLongitude;
            companyOrder.Products.FirstOrDefault(x => x.Status == Status.Active).Name = model.Products.FirstOrDefault(x => x.Status == Status.Active).Name;
            companyOrder.Products.FirstOrDefault(x => x.Status == Status.Active).PaletteCount = model.Products.FirstOrDefault(x => x.Status == Status.Active).PaletteCount;
            companyOrder.Products.FirstOrDefault(x => x.Status == Status.Active).Size = model.Products.FirstOrDefault(x => x.Status == Status.Active).Size;
            companyOrder.Products.FirstOrDefault(x => x.Status == Status.Active).Tonnage = model.Products.FirstOrDefault(x => x.Status == Status.Active).Tonnage;

            try
            {
                _unitOfWork.CompanyOrders.Update(companyOrder);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CompanyOrders
        [HttpPost]
        public IActionResult PostCompanyOrder([FromBody] CompanyOrder companyOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _unitOfWork.CompanyOrders.Add(companyOrder);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return CreatedAtAction("GetCompanyOrder", new { id = companyOrder.Id }, companyOrder);
        }

        // DELETE: api/CompanyOrders/5
        [HttpDelete("{id}")]
        public IActionResult DeleteCompanyOrder([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var companyOrder = _unitOfWork.CompanyOrders.Get(x => x.Id == id && x.Status == Status.Active);
            if (companyOrder != null)
            {
                companyOrder.Status = Status.Deleted;
                try
                {
                    _unitOfWork.CompanyOrders.Update(companyOrder);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return NotFound();
        }

        private bool CompanyOrderExists(long id)
        {
            return _unitOfWork.CompanyOrders.Any(e => e.Id == id);
        }
    }
}