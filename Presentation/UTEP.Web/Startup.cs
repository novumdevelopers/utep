﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UTEP.Web.Services;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using Microsoft.AspNetCore.Routing;
using UTEP.Web.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using UTEP.Core.UnitOfWork.Core;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.AspNetCore.Http;
using UTEP.Web.Models;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;
using System.Net;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Facebook;
using Microsoft.AspNetCore.DataProtection;
using System.IO;
using WebMarkupMin.AspNetCore2;

namespace UTEP.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env /*IConfiguration configuration*/)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UTEPDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddIdentity<User, Role>(config =>
            {
                config.SignIn.RequireConfirmedEmail = true;

                config.Password.RequiredUniqueChars = 0;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;
                config.Password.RequireDigit = true;
                config.Password.RequireLowercase = false;
            })
                .AddEntityFrameworkStores<UTEPDbContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<CustomIdentityErrorDescriber>();

            services.ConfigureApplicationCookie(options =>
            {
                //options.LoginPath = new PathString("/az/account/openLogin");
                //options.AccessDeniedPath = new PathString("/az/account/openLogin");

                options.LoginPath = new PathString("/az");
                options.AccessDeniedPath = new PathString("/az");
            });

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.Configure<RouteOptions>(options =>
            {
                options.ConstraintMap.Add("lang", typeof(LanguageRouteConstraint));
                options.ConstraintMap.Add("account", typeof(AccountRouteConstraint));
            });

            services.AddMvc()
                 // Add support for finding localized views, based on file name suffix, e.g. Index.fr.cshtml
                 .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                 // Add support for localizing strings in data annotations (e.g. validation messages) via the
                 // IStringLocalizer abstractions.
                 .AddDataAnnotationsLocalization()
                 .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());


            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache

            services.AddSession();

            services.AddDataProtection()
                    //.PersistKeysToFileSystem(new DirectoryInfo(@"D:\writable\temp\directory\"))
                    .SetDefaultKeyLifetime(TimeSpan.FromDays(14));

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("az-Latn-AZ"),
                    new CultureInfo("ru-RU"),
                };

                options.DefaultRequestCulture = new RequestCulture(culture: "az-Latn-AZ", uiCulture: "az-Latn-AZ");

                options.SupportedCultures = supportedCultures;

                options.SupportedUICultures = supportedCultures;

                options.RequestCultureProviders.Insert(0, new RouteCultureProvider(options.DefaultRequestCulture));

            });

            services.AddWebMarkupMin(
               options =>
               {
                   options.AllowMinificationInDevelopmentEnvironment = true;
                   options.AllowCompressionInDevelopmentEnvironment = true;
               })
           .AddHtmlMinification(
               options =>
               {
                   options.MinificationSettings.RemoveRedundantAttributes = true;
                   options.MinificationSettings.RemoveHttpProtocolFromAttributes = true;
                   options.MinificationSettings.RemoveHttpsProtocolFromAttributes = true;
                   options.MinificationSettings.MinifyInlineJsCode = true;
                   options.MinificationSettings.MinifyInlineCssCode = true;
                   options.MinificationSettings.RemoveHtmlComments = true;
                   options.MinificationSettings.RemoveHtmlCommentsFromScriptsAndStyles = true;
               })
           .AddHttpCompression();

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            services.Configure<SmtpConfig>(optionsSetup =>
            {
                optionsSetup.SmtpPassword = Configuration.GetSection("AGS").GetSection("Password").Value;
                optionsSetup.SmtpUserEmail = Configuration.GetSection("AGS").GetSection("Email").Value;
                optionsSetup.SmtpHost = Configuration.GetSection("AGS").GetSection("Host").Value;
                optionsSetup.SmtpPort = Convert.ToInt32(Configuration.GetSection("AGS").GetSection("Port").Value);
                //optionsSetup.SmtpPassword = "nicatnicat12345";
                //optionsSetup.SmtpUserEmail = "nicathidayetzade@gmail.com";
                //optionsSetup.SmtpHost = "smtp.gmail.com";
                //optionsSetup.SmtpPort = 587;
            });
            services.AddCors();

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IFileProcessor, FileProcessor>();

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/az/home/error");
                app.UseStatusCodePagesWithReExecute("/az/home/error/{0}");
            }

            app.UseStaticFiles(
            //    new StaticFileOptions
            //{
            //    OnPrepareResponse = ctx =>
            //    {
            //        const int durationInSeconds = 604800;
            //        ctx.Context.Response.Headers[HeaderNames.CacheControl] =
            //            "public,max-age=" + durationInSeconds;
            //    }
            //}
            );

            //app.UseWebMarkupMin();

            app.UseAuthentication();

            app.UseSession();

            var localizationOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();

            app.UseRequestLocalization(localizationOptions.Value);

            app.UseStatusCodePagesWithReExecute("/az/account/login/{0}");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areas",
                    template: "{area:exists}/{lang:lang}/{controller}/{action}/{id:long?}",
                    defaults: new { lang = "az", controller = "Home", action = "Index" }
                    );
                routes.MapRoute(
                   name: "default",
                   template: "{lang:lang}/{controller}/{action}/{id:long?}",
                   defaults: new { lang = "az", controller = "Language", action = "Index" }
               );
            });
        }
    }
}
