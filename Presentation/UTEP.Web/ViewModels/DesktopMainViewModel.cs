﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.ViewModels
{
    public class DesktopMainViewModel
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string ImagePath { get; set; }
    }
}
