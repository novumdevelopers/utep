﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.ViewModels.Language
{
    public class LanguageViewModel
    {
        public string ExistLanguage { get; set; }
        public List<LanguageClass> Languages { get; set; } = new List<LanguageClass>()
        {
            new LanguageClass {Name="Azərbaycan dili", LanguageCode = "az", LangFlag = "az.png"},
            new LanguageClass {Name="Русский язык", LanguageCode = "ru", LangFlag = "ru.png"}
        };
    }

    public class LanguageClass
    {
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        public string LangFlag { get; set; }
    }
}
