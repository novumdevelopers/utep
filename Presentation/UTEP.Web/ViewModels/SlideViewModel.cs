﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.ViewModels
{
    public class SlideViewModel
    {
        public List<SlideDetail> Slides { get; set; }
    }

    public class SlideDetail
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }

}
