﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.ViewModels
{
    public class SearchViewModel
    {
        public string FromId { get; set; }
        public List<SelectListItem> Froms { get; set; }
    }
}
