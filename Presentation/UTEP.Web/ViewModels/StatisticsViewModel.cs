﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.ViewModels
{
    public class StatisticsViewModel
    {
        public StatisticsDetail To { get; set; }
        public StatisticsDetail Companies { get; set; }
        public StatisticsDetail Orders { get; set; }
    }

    public class StatisticsDetail
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
