﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.ManageViewModels
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Köhnə Şifrə bölməsi doldurulmalıdır!")]
        [DataType(DataType.Password)]
        [Display(Name = "Köhnə Şifrə")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Yeni Şifrə bölməsi doldurulmalıdır!")]
        [StringLength(100, ErrorMessage = "Şifrə ən az {2} və ən çox {1} simvol uzunluğunda olmalıdır.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Yeni Şifrə Təkrar bölməsi doldurulmalıdır!")]
        [DataType(DataType.Password)]
        [Display(Name = "Yeni Şifrə Təkrar")]
        [Compare("NewPassword", ErrorMessage = "Şifrələr eyni olmalıdır.")]
        public string ConfirmPassword { get; set; }

        public string StatusMessage { get; set; }
    }
}
