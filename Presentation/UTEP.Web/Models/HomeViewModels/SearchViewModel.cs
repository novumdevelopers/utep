﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.HomeViewModels
{
    public class SearchViewModel
    {
        public DateTime dateFrom { get; set; }
        public DateTime dateTo { get; set; }
        public string From { get; set; }
    }
}
