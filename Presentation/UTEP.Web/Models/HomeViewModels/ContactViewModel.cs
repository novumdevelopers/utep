﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.HomeViewModels
{
    public class ContactViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Adın nədir?")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "E-mail ünvanını qeyd edin.")]
        [EmailAddress(ErrorMessage = "E-mail düzgün formatda deyil")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Mətni qeyd et.")]
        public string Message { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }


    }
}
