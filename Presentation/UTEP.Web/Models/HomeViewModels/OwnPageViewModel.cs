﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.HomeViewModels
{
    public class OwnPageViewModel
    {
        public string CompanyName { get; set; } //NotChange

        [Required(AllowEmptyStrings = false, ErrorMessage = "Adres bölməsi boş ola bilməz!")]
        public string CompanyAddress { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Telefon bölməsi boş ola bilməz!")]
        public string CompanyPhone { get; set; }

        public string CompanyLogo { get; set; }

        public string LogoForView { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Menecer Adı bölməsi boş ola bilməz!")]
        public string ManagerFirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Menecer Soyadı bölməsi boş ola bilməz!")]
        public string ManagerLastName { get; set; }
    }
}
