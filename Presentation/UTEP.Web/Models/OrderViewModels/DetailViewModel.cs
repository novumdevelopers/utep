﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.OrderViewModels
{
    public class DetailViewModel
    {
        public DetailViewModel()
        {
            ProductImages = new HashSet<ProductImageViewModel>();
        }
        public long Id { get; set; }
        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public string Manager { get; set; }
        public string CompanyAddress { get; set; }
        public string From { get; set; }
        public string FromDescription { get; set; }
        public string To { get; set; }
        public string ToDescription { get; set; }
        public string ProductName { get; set; }
        public decimal Tonnage { get; set; }
        public int PaletteCount { get; set; }
        public int ViewCount { get; set; }
        public DateTime DealDate { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime AddedDate { get; set; }
        public string Price { get; set; }
        public string Weight { get; set; }
        public string Currency { get; set; }
        public string Description { get; set; }
        public ICollection<ProductImageViewModel> ProductImages { get; set; }
        public ICollection<SimiliarProductsViewModel> SimiliarProducts { get; set; }
    }

    public class ProductImageViewModel
    {
        //public long Id { get; set; }
        //public string Name { get; set; }
        //public string Price { get; set; }
        //public string Currency { get; set; }
        public string MainPath { get; set; }
        public string MediumPath { get; set; }
    }

    public class SimiliarProductsViewModel
    {
        public long OrderId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Price { get; set; }
        public int Currency { get; set; }
        public string To { get; set; }
        public string From { get; set; }
    }
}
