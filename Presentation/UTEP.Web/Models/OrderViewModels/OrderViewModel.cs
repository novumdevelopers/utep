﻿using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;

namespace UTEP.Web.Models.OrderViewModels
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            ProductTypeTranslations = new List<ProductTypeTranslation>();
        }
        public IPagedList<CompanyOrderIndexListViewModel> CompanyOrders { get; set; }
        public List<ProductTypeTranslation> ProductTypeTranslations { get; set; }
        public long CurrentProductTypeId { get; set; }
    }
}
