﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Web.Areas.Admin.Models.SiteTextViewModels;
using UTEP.Web.Models.HomeViewModels;
using static UTEP.Web.Controllers.HomeController;

namespace UTEP.Web.Models.IndexViewModels
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            CompanyOrders = new HashSet<CompanyOrder>();
            ProductTypeTranslations = new HashSet<ProductTypeTranslation>();
        }
        public Claim Email { get; set; }
        public int AllTrucks { get; set; }
        public int AllCompanies { get; set; }
        public int AllOrders { get; set; }
        public int AllDirections { get; set; }
        public string External { get; set; }
        public ICollection<CompanyOrder> CompanyOrders { get; set; }
        public ICollection<ProductTypeTranslation> ProductTypeTranslations { get; set; }
        public SearchViewModel SearchVM { get; set; }
        public IEnumerable<EditSiteTextViewModel> SiteTextVM { get; set; }
        public long FromId { get; set; }
        public IEnumerable<SelectListItem> From { get; set; }

        //For Reset Password
        public string Token { get; set; }
        public long UserId { get; set; }
    }
}
