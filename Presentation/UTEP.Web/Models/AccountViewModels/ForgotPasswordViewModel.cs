﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email bölməsi boş ola bilməz!")]
        [EmailAddress(ErrorMessage = "Email düzgün formatda deyil.")]
        public string Email { get; set; }
    }
}
