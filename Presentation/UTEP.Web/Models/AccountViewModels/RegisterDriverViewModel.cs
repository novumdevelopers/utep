﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.AccountViewModels
{
    public class RegisterDriverViewModel
    {
        [Required(ErrorMessage = "Telefon bölməsi boş ola bilməz!")]
        [Phone]
        [Display(Name = "Mobil Nömrə")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Şifrə bölməsi boş ola bilməz!")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Şifrə")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Şifrə Təkrar bölməsi boş ola bilməz!")]
        [DataType(DataType.Password)]
        [Display(Name = "Şifrə Təkrar")]
        [Compare("Password", ErrorMessage = "Şifrələr eyni olmalıdır.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Ad bölməsi boş ola bilməz!")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Soyad bölməsi boş ola bilməz!")]
        public string LastName { get; set; }
    }
}
