﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.AccountViewModels
{
    public class LoginRegisterViewModel
    {
        public LoginViewModel Login { get; set; }
        public RegisterDriverViewModel RegisterDriver { get; set; }
        public RegisterCompanyViewModel RegisterCompany { get; set; }
    }
}
