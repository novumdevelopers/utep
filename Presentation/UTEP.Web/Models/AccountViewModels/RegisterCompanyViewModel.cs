﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.AccountViewModels
{
    public class RegisterCompanyViewModel
    {
        [Required(ErrorMessage = "Email bölməsi boş ola bilməz!")]
        [EmailAddress(ErrorMessage = "Email düzgün formatda deyil.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Şifrə bölməsi boş ola bilməz!")]
        [StringLength(100, ErrorMessage = "Şifrə ən az {2} və ən çox {1} simvoldan ibarət olmalıdır.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Şifrə")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Şifrə Təkrar bölməsi boş ola bilməz!")]
        [DataType(DataType.Password)]
        [Display(Name = "Şifrə Təkrar")]
        [Compare("Password", ErrorMessage = "Şifrələr eyni olmalıdır.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Şirkət Adı bölməsi boş ola bilməz!")]
        public string CompanyName { get; set; }
        
        public string VOEN { get; set; }

        [Required(ErrorMessage = "Telefon bölməsi boş ola bilməz!")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Ad bölməsi boş ola bilməz!")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Soyad bölməsi boş ola bilməz!")]
        public string LastName { get; set; }



    }
}
