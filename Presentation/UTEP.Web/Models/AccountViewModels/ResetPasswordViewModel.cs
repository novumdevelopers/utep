﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.AccountViewModels
{
    public class ResetPasswordViewModel
    {
        //[Required(ErrorMessage = "Email bölməsi boş ola bilməz!")]
        [EmailAddress(ErrorMessage = "Email düzgün formatda deyil.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Şifrə bölməsi boş ola bilməz!")]
        [StringLength(100, ErrorMessage = "Ən az {2} və ən çox {1} simvoldan ibarət olmalıdır.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Şifrə Təkrar bölməsi boş ola bilməz!")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Şifrələr eyni olmalıdır.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
        public long UserId { get; set; }
        public bool RememberMe { get; set; }
    }
}
