﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [EmailAddress(ErrorMessage ="Email düzgün formatda deyil.")]
        [Required(AllowEmptyStrings =false, ErrorMessage = "Email bölməsi boş ola bilməz!")]
        public string EmailOrPhone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Şifrə bölməsi boş ola bilməz!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string returnUrl { get; set; }
    }
}
