﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models
{
    public class SmtpConfig
    {
        public string SmtpPassword { get; set; }
        public string SmtpUserEmail { get; set; }
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }   
    }
}
