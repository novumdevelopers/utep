﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Models.ViewComponents
{
    public class AboutViewModel
    {
        public string Text { get; set; }
    }
}
