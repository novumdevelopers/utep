﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Models.AccountViewModels;

namespace UTEP.Web.ViewComponents
{
    public class LoginViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string returnUrl)
        {
            var model = new LoginViewModel
            {
                returnUrl = returnUrl
            };
            return View("Login", model);
        }
    }
}
