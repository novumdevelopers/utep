﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.ViewComponents
{
    public class ForgotPasswordViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("ForgotPassword");
        }
    }
}
