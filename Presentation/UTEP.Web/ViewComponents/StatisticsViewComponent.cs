﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.ViewModels;

namespace UTEP.Web.ViewComponents
{
    public class StatisticsViewComponent : ViewComponent
    {
        public UnitOfWork _unitOfWork;
        private readonly IStringLocalizer<StatisticsViewComponent> _localizer;

        public StatisticsViewComponent(
            DbContextOptions<UTEPDbContext> contextOptions,
            IStringLocalizer<StatisticsViewComponent> localizer)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _localizer = localizer;
        }


        public async Task<IViewComponentResult> InvokeAsync()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";

            var to = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active);
            var companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Carrier);
            var orders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active);

            var to_ = to.Count();
            var companies_ = companies.Count();
            var orders_ = orders.Count();

            var toModel = new StatisticsDetail
            {
                Count = to_,
                Name = _localizer["directions"]
            };
            var companiesModel = new StatisticsDetail
            {
                Count = companies_,
                Name = _localizer["companies"]
            };
            var ordersModel = new StatisticsDetail
            {
                Count = orders_,
                Name = _localizer["announces"]
            };
            var model = new StatisticsViewModel();
            model.To = toModel;
            model.Companies = companiesModel;
            model.Orders = ordersModel;

            return View("Statistics", model);
        }
    }
}
