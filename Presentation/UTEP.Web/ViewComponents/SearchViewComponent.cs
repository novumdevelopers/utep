﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.ViewModels;

namespace UTEP.Web.ViewComponents
{
    public class SearchViewComponent : ViewComponent
    {
        private readonly UnitOfWork _unitOfWork;

        public SearchViewComponent(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IViewComponentResult Invoke()
        {
            var model = new SearchViewModel
            {
                //Froms = _unitOfWork.Cities.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                //{
                //    Value = x.Id.ToString(),
                //    Text = x.Name
                //}).ToList()
            };
            return View("Search", model);
        }
    }
}
