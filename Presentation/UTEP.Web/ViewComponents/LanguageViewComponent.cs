﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.ViewModels.Language;

namespace UTEP.Web.ViewComponents
{
    public class LanguageViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var lang = RouteData.Values["lang"].ToString();

            var model = new LanguageViewModel
            {
                ExistLanguage = lang
            };

            return View("Language", model);
        }
    }
}
