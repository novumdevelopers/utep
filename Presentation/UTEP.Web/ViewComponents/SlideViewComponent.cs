﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.ViewModels;

namespace UTEP.Web.ViewComponents
{
    public class SlideViewComponent : ViewComponent
    {
        public UnitOfWork _unitOfWork;

        public SlideViewComponent(
            DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IViewComponentResult Invoke()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var model = new SlideViewModel();
            var slides = _unitOfWork.ProductTypeTranslations.LastTypes(x => x.Status == Status.Active && x.SiteLanguage.LanguageCode == lang && x.ProductType.Status == Status.Active).Select(x => new SlideDetail
            {
                Id = x.Id,
                Name = x.Name,
                Count = x.ProductType.CompanyProducts.Where(y => y.CompanyOrder.Status == Status.Active).Count()
            }).ToList();
            model.Slides = slides;
            return View("Slide", model);
        }
    }
}
