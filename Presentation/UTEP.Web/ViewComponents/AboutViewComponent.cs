﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Models.ViewComponents;

namespace UTEP.Web.ViewComponents
{
    public class AboutViewComponent : ViewComponent
    {
        private readonly UnitOfWork _unitOfWork;
        public AboutViewComponent(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var model = new AboutViewModel();
            var about = _unitOfWork.SiteTexts.Get(x => x.Status == Status.Active && x.Type == SiteTextType.About)
                    .SiteTextTranslations.FirstOrDefault(x=>x.SiteLanguage.LanguageCode == lang && x.Status == Status.Active);

            if (about != null)
            {
                //var text = about.Body1.Split(" ");
                //model.Text = text.Length > 25 ? string.Format("{0}...", string.Join(" ", text.Take(25))) : string.Join(" ", text);
                model.Text = about.Body1;
            }
            return View("About", model);
        }


    }
}
