﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Settings;
using UTEP.Web.ViewModels;

namespace UTEP.Web.ViewComponents
{
    public class DesktopMainViewComponent : ViewComponent
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;

        public DesktopMainViewComponent(
            DbContextOptions<UTEPDbContext> contextOptions,
            UserManager<User> userManager)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await _userManager.GetUserAsync(HttpContext.User);
            bool isCompany = user != null && User.IsInRole("Manager");

            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var texts = _unitOfWork.SiteTextTranslations.Find(x => x.SiteText.Type == SiteTextType.Main && x.SiteLanguage.LanguageCode == lang, x => x.Include(y => y.SiteText).Include(y => y.SiteLanguage));

            var title = texts.FirstOrDefault().Title1;
            var body = texts.FirstOrDefault().Title2 ;
            var image = texts.FirstOrDefault().ImagePath1;

            var model = new DesktopMainViewModel
            {
                Title = title,
                Body = body,
                ImagePath = image
            };

            if (isCompany)
            {
                return View("DesktopMainCompany", model);
            }
            return View("DesktopMain", model);

        }

    }
}
