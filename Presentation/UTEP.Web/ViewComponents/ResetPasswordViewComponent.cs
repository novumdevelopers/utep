﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Models.AccountViewModels;

namespace UTEP.Web.ViewComponents
{
    public class ResetPasswordViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(ResetPasswordViewModel model)
        {
            return View("ResetPassword", model);
        }
    }
}
