﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PagedList.Core;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.ViewModels.CompanyViewModels;
using UTEP.Settings;
using UTEP.Web.Models.OrderViewModels;

namespace UTEP.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public OrderController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        [HttpGet]
        [Route("{lang:lang}/[controller]/[action]/{page?}/{id?}")]
        public IActionResult List(int page = 0, long id = 0)
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var pageNumber = page <= 0 ? 1 : page;
            var pageSize = 9;
            var model = new OrderViewModel
            {
                CompanyOrders = (IPagedList<CompanyOrderIndexListViewModel>)_unitOfWork.CompanyOrders.ToPagedList(x =>
                                x.Status == Status.Active &&
                                x.Products.Any(y => y.Status == Status.Active) &&
                                (id == 0 || x.Products.FirstOrDefault(y => y.Status == Status.Active).ProductTypeId == id),
                                lang,
                                pageNumber,
                                pageSize),
                ProductTypeTranslations = _unitOfWork.ProductTypeTranslations.LastTypes(x => x.Status == Status.Active && x.SiteLanguage.LanguageCode == lang).ToList(),
                CurrentProductTypeId = id
            };
            return View(model);
        }

        [HttpGet]
        [Route("{lang:lang}/[controller]/[action]/{id?}")]
        public IActionResult Detail(long id)
        {
            var productId = _unitOfWork.Products.Get(x => x.CompanyOrderId == id).Id;
            //var productTypeId = _unitOfWork.ProductTypes.Get(x => x.CompanyProducts.FirstOrDefault().CompanyOrderId == id).Id;
            var productTypeId = _unitOfWork.CompanyOrders.Get(x => x.Id == id).Products.FirstOrDefault().ProductTypeId;
            var order = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Id == id && x.Status == Status.Active);
            if (order != null)
            {
                var lang = RouteData.Values["lang"].ToString() ?? "az";
                try
                {
                    //var IpAddress = Response.HttpContext.Connection.RemoteIpAddress;
                    var cookieKey = order.Products.First().Id.ToString() + lang;
                    var cookieValue = Request.Cookies[cookieKey];

                    if (string.IsNullOrEmpty(cookieValue))
                    {
                        var cookieOptions = new CookieOptions()
                        {
                            Path = "/",
                            Expires = DateTime.Now.AddDays(1),
                            HttpOnly = true,
                            Secure = false
                        };
                        order.Products.First().ViewCount += 1;

                        _unitOfWork.CompanyOrders.Update(order);
                        _unitOfWork.Complete();

                        Response.Cookies.Append(cookieKey, Guid.NewGuid().ToString(), cookieOptions);
                    }
                }
                catch (Exception)
                {
                    throw;
                }

                var model = new DetailViewModel
                {
                    Id = order.Id,
                    CompanyName = order.Products.FirstOrDefault().Company.Name,
                    CompanyEmail = order.Products.FirstOrDefault().Company.Email,
                    CompanyPhone = order.Products.FirstOrDefault().Company.Phone,
                    Manager = string.Format("{0} {1}", order.Products.FirstOrDefault().Company.User.FirstName, order.Products.FirstOrDefault().Company.User.LastName),
                    CompanyAddress = order.Products.FirstOrDefault().Company.Address,
                    From = $"{order.CountryFrom} ({order.From})",
                    To = $"{order.CountryTo} ({order.To})",
                    FromDescription = order.FromDescription,
                    ToDescription = order.ToDescription,
                    ProductName = order.Products.FirstOrDefault().Name,
                    PaletteCount = order.Products.FirstOrDefault().PaletteCount,
                    Tonnage = order.Products.FirstOrDefault().Tonnage,
                    Weight = Weight.GetWeight(order.Products.FirstOrDefault().Weight),
                    ViewCount = order.Products.FirstOrDefault().ViewCount,
                    DealDate = order.DealDate,
                    DeadLine = order.CompanyShippings.First().Deadline,
                    AddedDate = order.Products.FirstOrDefault().AddedDate,
                    Price = order.ProporsalPrice.ToString("0.##"),
                    Currency = GetCurrency(order.Currency),
                    Description = order.Products.FirstOrDefault().Description,
                    ProductImages = order.Products.FirstOrDefault().ProductImages.Where(x => x.Status == Status.Active).Select(x => new ProductImageViewModel
                    {
                        MainPath = x.MainPath,
                        MediumPath = x.MediumPath
                    }).Take(6).ToList(),
                    SimiliarProducts = _unitOfWork.Products.Find(x => x.ProductTypeId == productTypeId && x.Id != productId && x.CompanyOrder.Status == Status.Active).Select(x => new SimiliarProductsViewModel
                    {
                        OrderId = x.CompanyOrderId,
                        Image = x.ProductImages.Any(y => y.Status == Status.Active) ? x.ProductImages.First(y => y.Status == Status.Active).MediumPath : "question-mark.png",
                        Name = x.Name,
                        Price = x.CompanyOrder.ProporsalPrice.ToString("0.##"),
                        Currency = x.CompanyOrder.Currency,
                        From = $"{x.CompanyOrder.CountryFrom} ({x.CompanyOrder.From})",
                        To = $"{x.CompanyOrder.CountryTo} ({x.CompanyOrder.To})",
                    }).ToList()
                };

                return View(model);
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }

        [HttpGet]
        [Route("{lang:lang}/[controller]/[action]/{id?}")]
        public IActionResult Search(DateTime startDate, DateTime endDate, long fromId, int page = 0)
        {
            var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Status == Status.Active);

            var to = string.Empty;
            if (fromId != 0)
            {
                to = _unitOfWork.Countries.Get(x => x.Id == fromId).Name;
            }

            var pageNumber = page <= 0 ? 1 : page;
            var pageSize = 12;
            var lang = RouteData.Values["lang"].ToString() ?? "az";

            var model = new OrderViewModel
            {
                CompanyOrders = (IPagedList<CompanyOrderIndexListViewModel>)_unitOfWork.CompanyOrders.ToPagedList(x =>
                                    x.Status == Status.Active &&
                                    (startDate == DateTime.Parse("01.01.0001") || startDate <= x.CompanyShippings.FirstOrDefault().Deadline) &&
                                    (endDate == DateTime.Parse("01.01.0001") || endDate >= x.DealDate) &&
                                    (fromId == 0 || to == x.CountryTo),
                                    lang,
                                    pageNumber,
                                    pageSize
                                    ),
                ProductTypeTranslations = _unitOfWork.ProductTypeTranslations.LastTypes(x => x.Status == Status.Active && x.SiteLanguage.LanguageCode == lang).ToList(),
            };

            return View("List", model);
        }


        private string GetCurrency(int num)
        {
            if (num == 1)
            {
                return "AZN";
            }
            else if (num == 2)
            {
                return "$"; //"USD";
            }
            else
            {
                return "€"; //"EUR";
            }
        }

    }
}