﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Web.Services;

namespace UTEP.Web.Controllers
{
    public class TestController : Controller
    {

        private readonly IEmailSender _emailSender;
        private readonly UnitOfWork _unitOfWork;

        public TestController(IEmailSender emailSender, DbContextOptions<UTEPDbContext> contextOptions)
        {
            _emailSender = emailSender;
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }


        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var orders = _unitOfWork.CompanyOrders.GetAll();

            foreach (var item in orders)
            {
                item.CountryFrom = _unitOfWork.Cities.Get(x => x.Name == item.From, x => x.Include(y => y.Country)).Country.Name;
                item.CountryTo = _unitOfWork.Cities.Get(x => x.Name == item.To, x => x.Include(y => y.Country)).Country.Name;
            }
            _unitOfWork.CompanyOrders.UpdateRange(orders);
            _unitOfWork.Complete();
            _unitOfWork.Dispose();

            return Ok();
        }
    }
}