﻿using CashbackApp.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Settings;
using UTEP.Web.Models.AccountViewModels;
using UTEP.Web.Services;

namespace UTEP.Web.Controllers
{
    [Authorize]
    [Route("{lang:lang}/[controller]/[action]")]
    public class AccountController : BaseController
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IStringLocalizer<AccountController> _localizer;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly string _key = "E546C9EF278CD5930069B52BE695D4F2";

        public AccountController(
            IHostingEnvironment hostingEnvironment,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            SignInManager<User> signInManager,
            IStringLocalizer<AccountController> localizer,
            IEmailSender emailSender,
            DbContextOptions<UTEPDbContext> contextOptions,
            ILogger<AccountController> logger)
        {
            _hostingEnvironment = hostingEnvironment;
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _localizer = localizer;
            _emailSender = emailSender;
            _logger = logger;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [HttpGet]
        [AllowAnonymous]
        //[Route("/{lang:lang}/[controller]/[action]")]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            TempData["returnUrl"] = returnUrl;
            //ViewData["ReturnUrl"] = returnUrl;
            //return View("Login");
            return ViewComponent("Login", returnUrl);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                string userName = model.EmailOrPhone;
                var email = new EmailAddressAttribute().IsValid(model.EmailOrPhone);
                var phone = Regex.Match(model.EmailOrPhone, @"^(\+[0-9]{12})$").Success;

                if (!email && !phone)
                {
                    ModelState.AddModelError("", _localizer["unsuccessfully_login"]);
                    var mes = _localizer["unsuccessfully_login"];
                    return Json(new { message = mes, status = false });
                }

                if (email)
                {
                    var user = _userManager.Users.FirstOrDefault(x => x.UserName == model.EmailOrPhone);
                    var company = user != null ? _unitOfWork.Companies.Get(x => x.UserId == user.Id) : null;
                    if (user == null)
                    {
                        ModelState.AddModelError("", _localizer["no_user"]);
                        var mes = _localizer["no_user"];
                        return Json(new { message = mes, status = false });
                    }

                    else if (!user.EmailConfirmed)
                    {
                        ModelState.AddModelError("", _localizer["please_confirm_email"]);
                        var mes = _localizer["please_confirm_email"];
                        return Json(new { message = mes, status = false });
                    }

                    else if (company != null)
                    {
                        if (user.TypeOfUser == TypeOfUsers.CompanyNotConfirmed || company.TypeOfCompany == TypeOfCompany.Carrier)
                        {
                            ModelState.AddModelError("", _localizer["not_permission"]);
                            var mes = _localizer["not_permission"];
                            return Json(new { message = mes, status = false });
                        }
                    }
                }
                var currentUser = _userManager.Users.FirstOrDefault(x => x.UserName == model.EmailOrPhone);
                var result = await _signInManager.PasswordSignInAsync(currentUser, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    //return RedirectToLocal(returnUrl);
                    return Json(new { status = true });
                }
                //if (result.RequiresTwoFactor)
                //{
                //    return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
                //}
                //if (result.IsLockedOut)
                //{
                //    _logger.LogWarning("User account locked out.");
                //    return RedirectToAction(nameof(Lockout));
                //}
                else
                {
                    ModelState.AddModelError(string.Empty, _localizer["unsuccessfull_attempt"]);

                    var mes = _localizer["unsuccessfull_attempt"];
                    return Json(new { message = mes, status = false });
                }
            }

            // If we got this far, something failed, redisplay form
            var message = "Tam doldurulmayıb";
            return Json(new { message, status = false });
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginFromJS(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                string userName = model.EmailOrPhone;
                var email = new EmailAddressAttribute().IsValid(model.EmailOrPhone);
                var phone = Regex.Match(model.EmailOrPhone, @"^(\+[0-9]{12})$").Success;

                if (!email && !phone)
                {
                    ModelState.AddModelError("", _localizer["unsuccessfully_login"]);
                    var mes = _localizer["unsuccessfully_login"];
                    return Json(new { model.returnUrl, status = false });
                }

                if (email)
                {
                    var user = _userManager.Users.FirstOrDefault(x => x.UserName == model.EmailOrPhone);
                    var company = user != null ? _unitOfWork.Companies.Get(x => x.UserId == user.Id) : null;
                    if (user == null)
                    {
                        ModelState.AddModelError("", _localizer["no_user"]);
                        var mes = _localizer["no_user"];
                        return Json(new { model.returnUrl, message = mes, status = false });
                    }

                    else if (!user.EmailConfirmed)
                    {
                        ModelState.AddModelError("", _localizer["please_confirm_email"]);
                        var mes = _localizer["please_confirm_email"];
                        return Json(new { model.returnUrl, message = mes, status = false });
                    }

                    else if (company != null)
                    {
                        if (user.TypeOfUser == TypeOfUsers.CompanyNotConfirmed || company.TypeOfCompany == TypeOfCompany.Carrier)
                        {
                            ModelState.AddModelError("", _localizer["not_permission"]);
                            var mes = _localizer["not_permission"];
                            return Json(new { model.returnUrl, message = mes, status = false });
                        }
                    }
                }
                var currentUser = _userManager.Users.FirstOrDefault(x => x.UserName == model.EmailOrPhone);
                var result = await _signInManager.PasswordSignInAsync(currentUser, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    //return RedirectToLocal(returnUrl);
                    return Json(new { model.returnUrl, status = true });
                }
                //if (result.RequiresTwoFactor)
                //{
                //    return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
                //}
                //if (result.IsLockedOut)
                //{
                //    _logger.LogWarning("User account locked out.");
                //    return RedirectToAction(nameof(Lockout));
                //}
                else
                {
                    ModelState.AddModelError(string.Empty, _localizer["unsuccessfull_attempt"]);

                    var mes = _localizer["unsuccessfull_attempt"];
                    return Json(new { model.returnUrl, message = mes, status = false });
                }
            }

            // If we got this far, something failed, redisplay form
            var message = "Tam doldurulmayıb";
            return Json(new { model.returnUrl, message, status = false });
        }


        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
        //{
        //    // Ensure the user has gone through the username & password screen first
        //    var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

        //    if (user == null)
        //    {
        //        throw new ApplicationException($"Unable to load two-factor authentication user.");
        //    }

        //    var model = new LoginWith2faViewModel { RememberMe = rememberMe };
        //    ViewData["ReturnUrl"] = returnUrl;

        //    return View(model);
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe, string returnUrl = null)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
        //    if (user == null)
        //    {
        //        throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
        //    }

        //    var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

        //    var result = await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe, model.RememberMachine);

        //    if (result.Succeeded)
        //    {
        //        _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    else if (result.IsLockedOut)
        //    {
        //        _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
        //        return RedirectToAction(nameof(Lockout));
        //    }
        //    else
        //    {
        //        _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
        //        ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
        //        return View();
        //    }
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
        //{
        //    // Ensure the user has gone through the username & password screen first
        //    var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
        //    if (user == null)
        //    {
        //        throw new ApplicationException($"Unable to load two-factor authentication user.");
        //    }

        //    ViewData["ReturnUrl"] = returnUrl;

        //    return View();
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model, string returnUrl = null)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
        //    if (user == null)
        //    {
        //        throw new ApplicationException($"Unable to load two-factor authentication user.");
        //    }

        //    var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

        //    var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

        //    if (result.Succeeded)
        //    {
        //        _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    if (result.IsLockedOut)
        //    {
        //        _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
        //        return RedirectToAction(nameof(Lockout));
        //    }
        //    else
        //    {
        //        _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
        //        ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
        //        return View();
        //    }
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //public IActionResult Lockout()
        //{
        //    return View();
        //}

        [HttpGet]
        [AllowAnonymous]
        public IActionResult RegisterDriver(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterDriver(RegisterDriverViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var driver = new Driver
                {
                    CompanyId = 1
                };
                var user = new User { UserName = model.Phone, FirstName = model.FirstName, LastName = model.LastName, PhoneNumber = model.Phone, TypeOfUser = TypeOfUsers.DriverConfirmed, Driver = driver };
                var role = await _roleManager.FindByNameAsync("Driver");
                if (role != null)
                {
                    user.Roles.Add(new IdentityUserRole<long>
                    {
                        UserId = user.Id,
                        RoleId = role.Id
                    });
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        _logger.LogInformation("User created a new account with password.");

                        //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        //var callbackUrl = Url.EmailConfirmationLink(user.Id.ToString(), code, Request.Scheme);
                        //await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created a new account with password.");
                        return RedirectToLocal(returnUrl);
                    }
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult RegisterCompany(string returnUrl = null)
        {
            //ViewData["ReturnUrl"] = returnUrl;
            //return View();
            return ViewComponent("Register");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegisterCompany(RegisterCompanyViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                if (_userManager.Users.Any(x => x.Email == model.Email))
                {
                    ModelState.AddModelError("", string.Format(_localizer["email_is_taken"], model.Email));
                    var message = string.Format(_localizer["email_is_taken"], model.Email);
                    return Json(new { message, status = false });
                }
                if (_unitOfWork.Companies.Find(x => x.TypeOfCompany == TypeOfCompany.Client).Any(x => x.Email == model.Email))
                {
                    ModelState.AddModelError("", string.Format(_localizer["email_is_taken"], model.Email));
                    var message = string.Format(_localizer["email_is_taken"], model.Email);
                    return Json(new { message, status = false });
                }
                if (_unitOfWork.Companies.Find(x => x.TypeOfCompany == TypeOfCompany.Client).Any(x => x.Name == model.CompanyName))
                {
                    ModelState.AddModelError("", string.Format(_localizer["name_is_taken"], model.CompanyName));
                    var message = string.Format(_localizer["name_is_taken"], model.CompanyName);
                    return Json(new { message, status = false });
                }

                var companyTypeId = _unitOfWork.CompanyTypes.Get(x => x.Name == "Özəl")?.Id ?? _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).First().Id;
                if (companyTypeId == 0)
                {
                    return RedirectToAction("Error", "Home");
                }

                var user = new User
                {
                    UserName = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.Phone,
                    Email = model.Email,
                    TypeOfUser = TypeOfUsers.CompanyConfirmed,
                    Company = new Company
                    {
                        Name = model.CompanyName,
                        Email = model.Email,
                        Phone = model.Phone,
                        LocationLatitude = 0m,
                        LocationLongitude = 0m,
                        CompanyTypeId = companyTypeId,
                        TaxIdentificationNumber = Guid.NewGuid().ToString(),
                        Address = "Bakı",
                        VOEN = model.VOEN
                    }
                };

                var role = await _roleManager.FindByNameAsync("Manager");
                user.Roles.Add(new IdentityUserRole<long>
                {
                    UserId = user.Id,
                    RoleId = role.Id
                });

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Action(nameof(ConfirmEmail), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                    var message = string.Format(_localizer["confirm_profile_message"], callbackUrl);
                    await _emailSender.SendEmailAsync(model.Email, _localizer["confirm_profile_title"], message);

                    //await _signInManager.SignInAsync(user, isPersistent: false);
                    //_logger.LogInformation("User created a new account with password.");
                    //return RedirectToLocal(returnUrl);

                    //var content = DateTime.Now.AddMinutes(1).ToString();
                    //var encrypted = Cryptography.EncryptString(content, _key);

                    //return RedirectToAction("CheckEmail", new { code = encrypted });
                    return Json(new { status = true });
                }
                //AddErrors(result);
                var mes = "";
                foreach (var error in result.Errors)
                {
                    mes += error;
                }
                return Json(new { message = mes, status = false });
            }

            // If we got this far, something failed, redisplay form
            //return View(model);
            return Json(new { message = "Tam doldurulmayıb", status = false });
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult CheckEmail(string code)
        {
            //var decrypted = Cryptography.DecryptString(code, _key);
            //if (string.IsNullOrEmpty(decrypted))
            //{
            //    return RedirectToAction("Error", "Home");
            //}
            //var data = Convert.ToDateTime(decrypted);
            //if (DateTime.Now > data)
            //{
            //    return RedirectToAction("Error", "Home");
            //}

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            //var facebookLogout = CookieAuthenticationDefaults.AuthenticationScheme;
            await _signInManager.SignOutAsync();
            //Response.Cookies.Delete(".AspNetCore." + facebookLogout + "");
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public IActionResult ExternalLogin(string provider, string returnUrl = null)
        //{
        //    // Request a redirect to the external login provider.
        //    var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { returnUrl });
        //    var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
        //    return Challenge(properties, provider);
        //}

        //[HttpGet]
        //[AllowAnonymous]
        //public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        //{
        //    if (remoteError != null)
        //    {
        //        ErrorMessage = $"Error from external provider: {remoteError}";
        //        return RedirectToAction(nameof(Login));
        //    }
        //    var info = await _signInManager.GetExternalLoginInfoAsync();
        //    if (info == null)
        //    {
        //        return RedirectToAction(nameof(Login));
        //    }

        //    // Sign in the user with this external login provider if the user already has a login.
        //    var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
        //    if (result.Succeeded)
        //    {
        //        _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);
        //        return RedirectToLocal(returnUrl);
        //    }
        //    if (result.IsLockedOut)
        //    {
        //        return RedirectToAction(nameof(Lockout));
        //    }
        //    else
        //    {
        //        // If the user does not have an account, then ask the user to create an account.
        //        ViewData["ReturnUrl"] = returnUrl;
        //        ViewData["LoginProvider"] = info.LoginProvider;
        //        var email = info.Principal.FindFirstValue(ClaimTypes.Email);
        //        return View("ExternalLogin", new ExternalLoginViewModel { Email = email });
        //    }
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model, string returnUrl = null)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // Get the information about the user from the external login provider
        //        var info = await _signInManager.GetExternalLoginInfoAsync();
        //        if (info == null)
        //        {
        //            throw new ApplicationException("Error loading external login information during confirmation.");
        //        }
        //        var user = new User { UserName = model.Email, Email = model.Email };
        //        var result = await _userManager.CreateAsync(user);
        //        if (result.Succeeded)
        //        {
        //            result = await _userManager.AddLoginAsync(user, info);
        //            if (result.Succeeded)
        //            {
        //                await _signInManager.SignInAsync(user, isPersistent: false);
        //                _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
        //                return RedirectToLocal(returnUrl);
        //            }
        //        }
        //        AddErrors(result);
        //    }

        //    ViewData["ReturnUrl"] = returnUrl;
        //    return View(nameof(ExternalLogin), model);
        //}

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Error), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                //throw new ApplicationException($"Unable to load user with ID '{userId}'.");
                return RedirectToAction(nameof(HomeController.Error), "Home");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return ViewComponent("ForgotPassword");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    //ModelState.AddModelError("", "Daxil etdiyiniz email bazada yoxdur");
                    //return View(model);
                    return Json(new { status = false, message = _localizer["no_user"] });
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action(nameof(HomeController.Index), "Home", new { token = code, userId = user.Id }, protocol: Request.Scheme); //Url.ResetPasswordCallbackLink(user.Id.ToString(), code, Request.Scheme);
                var message = string.Format(_localizer["forgot_password_message"], callbackUrl);
                await _emailSender.SendEmailAsync(model.Email, _localizer["forgot_password"], message);
                //return RedirectToAction(nameof(ForgotPasswordConfirmation));
                return Json(new { status = true, message = _localizer["email_has_sent"] });
            }

            // If we got this far, something failed, redisplay form
            return Json(new { status = false });
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code, long userId)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            //var email = _userManager.FindByIdAsync(userId).ToString();
            var model = new ResetPasswordViewModel { Code = code, UserId = Convert.ToInt64(userId) };
            //return View(model);
            return ViewComponent("ResetPassword", model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { status = false });
            }
            var user = await _userManager.FindByIdAsync(model.UserId.ToString());
            if (user == null)
            {
                // Don't reveal that the user does not exist
                //ModelState.AddModelError("", "Email tapılmadı.");
                //return View(model);
                return Json(new { status = false });
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                //return RedirectToAction(nameof(Login), "Account");
                await _signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, false);
                return Json(new { status = true, url = Url.Action("Index", "Home", "") });
            }
            else
            {
                var msg = string.Empty;
                foreach (var item in result.Errors)
                {
                    msg += item.Description;
                }
                return Json(new { status = false, message = msg });
            }

        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }

}
