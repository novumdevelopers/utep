﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Data.Domain.Models.Tables.CooperationBasedGroup;
using UTEP.Data.Domain.Models.Tables.CountryBasedGroup;
using UTEP.Data.Domain.Models.Tables.SubscribeBasedGroup;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models.SiteTextViewModels;
using UTEP.Web.Attribute;
using UTEP.Web.Models;
using UTEP.Web.Models.HomeViewModels;
using UTEP.Web.Models.IndexViewModels;
using UTEP.Web.Services;

namespace UTEP.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly SignInManager<User> _signInManager;
        private readonly IStringLocalizer<HomeController> _localizer;
        private readonly IEmailSender _emailSender;
        private IConfiguration Configuration;


        public HomeController(
            IEmailSender emailSender,
            SignInManager<User> signInManager,
            IStringLocalizer<HomeController> localizer,
            IConfiguration configuration,
            DbContextOptions<UTEPDbContext> contextOptions)
        {
            _emailSender = emailSender;
            _localizer = localizer;
            _signInManager = signInManager;
            Configuration = configuration;
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        [HttpGet]
        [Route("{lang:lang}")]
        public IActionResult Index(string ReturnUrl, string token, long userId)
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var siteTexts = _unitOfWork.SiteTexts.GetForTranslation(x => x.Status == Status.Active && x.Type == SiteTextType.Main)
                    .SiteTextTranslations.Where(x => x.SiteLanguage.LanguageCode == lang)
                    .Select(x => new EditSiteTextViewModel
                    {
                        Title1 = x.Title1,
                        Title2 = x.Title2,
                        ImageForView1 = x.ImagePath1
                    });

            var model = new IndexViewModel
            {
                External = ReturnUrl,
                Token = token,
                UserId = userId,

                CompanyOrders = _unitOfWork.CompanyOrders.LastOrders(x => x.Status == Status.Active).OrderByDescending(x => x.AddedDate).Take(8).ToList(),
                ProductTypeTranslations = _unitOfWork.ProductTypeTranslations.LastTypes(x => x.Status == Status.Active && x.SiteLanguage.LanguageCode == lang).ToList(),
                //From = _unitOfWork.Cities.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                //{
                //    Text = x.Name,
                //    Value = x.Id.ToString()
                //}).ToList(),
                SiteTextVM = siteTexts,

                AllTrucks = _unitOfWork.Trucks.Count(x => x.Status == Status.Active),
                AllCompanies = _unitOfWork.Companies.Count(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client),
                AllOrders = _unitOfWork.CompanyOrders.Count(x => x.Status == Status.Active),
                AllDirections = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => x.To).Distinct().Count()
            };

            return View(model);
        }

        //[HttpPost]
        //[Route("{lang:lang}/{cat?}")]
        //public IActionResult Index(string cat = "1")
        //{
        //    var lang = RouteData.Values["lang"].ToString() ?? "az";
        //    var siteTexts = _unitOfWork.SiteTexts.Get(x => x.Status == Status.Active && x.Type == SiteTextType.Main)
        //            .SiteTextTranslations.Where(x => x.SiteLanguage.LanguageCode == lang)
        //            .Select(x => new EditSiteTextViewModel
        //            {
        //                Title1 = x.Title1,
        //                Title2 = x.Title2,
        //                ImageForView1 = x.ImagePath1
        //            });

        //    var model = new IndexViewModel
        //    {
        //        Option = cat,
        //        CompanyOrders = _unitOfWork.CompanyOrders.LastOrders(x => x.Status == Status.Active/* && x.Products.FirstOrDefault().ProductImages.FirstOrDefault().Status == Status.Active*/).ToList(),
        //        ProductTypeTranslations = _unitOfWork.ProductTypeTranslations.LastTypes(x => x.Status == Status.Active && x.SiteLanguage.LanguageCode == lang).ToList(),
        //        SiteTextVM = siteTexts,

        //        AllTrucks = _unitOfWork.Trucks.Find(x => x.Status == Status.Active).Count(),
        //        AllCompanies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Count(),
        //        AllOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Count(),
        //        AllDirections = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => x.To).Distinct().Count()
        //    };

        //    switch (cat)
        //    {
        //        case "1":
        //            model.CompanyOrders = model.CompanyOrders.OrderByDescending(x => x.AddedDate).Take(6).ToList();
        //            break;
        //        case "2":
        //            model.CompanyOrders = model.CompanyOrders.OrderByDescending(x => x.Products.FirstOrDefault().ViewCount).Take(6).ToList();
        //            break;
        //        default:
        //            model.CompanyOrders = model.CompanyOrders.OrderByDescending(x => x.AddedDate).Take(6).ToList();
        //            break;
        //    }

        //    return View(model);
        //}

        [HttpGet]
        [Route("{lang:lang}/[action]")]
        public IActionResult About()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var cooperations = _unitOfWork.Cooperations.Find(x => x.Status == Status.Active);
            var about = _unitOfWork.SiteTexts.Get(x => x.Status == Status.Active && x.Type == SiteTextType.About)
                    .SiteTextTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == lang);

            if (about != null)
            {
                var model = new EditSiteTextViewModel
                {
                    Title1 = about.Title1,
                    Body1 = about.Body1,
                    Title2 = about.Title2,
                    Title3 = about.Title3,
                    Body3 = about.Body3,
                    ImagePath1 = about.ImagePath1,
                    ImagePath2 = about.ImagePath2,
                    ImagePath3 = about.ImagePath3,
                    Cooperations = cooperations
                };
                return View(model);
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }

        [HttpGet]
        [Route("{lang:lang}/[action]")]
        public IActionResult Contact()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var contact = _unitOfWork.SiteTextTranslations.Get(x => x.SiteLanguage.LanguageCode == lang && x.Status == Status.Active && x.SiteText.Type == SiteTextType.About);
            var model = new ContactViewModel();
            if (contact != null)
            {
                model.Title = contact.Title3;
                model.Description = contact.Body3;
            }
            ViewData["M"] = TempData["M"];
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("{lang:lang}/[action]")]
        public async Task<IActionResult> Contact(ContactViewModel model)
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", _localizer["all_required"]);
                return View(model);
            }
            var message = string.Format("Göndərənin adı: {0}<br />Göndərənin emaili: <a href='mailto:{1}'>{1}</a><br />Mətn: {2}", model.Name, model.Email, model.Message);
            var subject = "UTEP Contact";
            var email = Configuration["AGS:Email"];

            await _emailSender.SendEmailAsync(email, subject, message);
            ViewData["M"] = _localizer["sent_successfully"];
            return View(new ContactViewModel());

        }

        [HttpGet]
        public IActionResult Error()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Subscribe(string email)
        {
            var isValid = new EmailAddressAttribute().IsValid(email);
            if (isValid)
            {
                var anyEmail = _unitOfWork.Subscribes.Any(x => x.Email.ToLower() == email.ToLower());
                if (anyEmail)
                {
                    return View("Subscribe", false);
                    //return ViewComponent("Subscribe", "error");
                }
                var subscribe = new Subscribe
                {
                    Email = email
                };
                try
                {
                    _unitOfWork.Subscribes.Add(subscribe);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return View("Subscribe", true);
                    //return ViewComponent("Subscribe", "success");
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }

        [HttpGet]
        [AjaxOnly]
        [Route("{lang:lang}/[controller]/[action]")]
        public IActionResult GetCities()
        {
            var list = new List<SelectListItem>();

            list = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            }).ToList();

            return Json(list);

        }

    }
}
