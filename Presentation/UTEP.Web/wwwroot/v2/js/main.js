"use strict"
/* DATEPICKER */
$('.start-date input').datepicker({
});
$('.end-date input').datepicker({
});


/* MOBILE NAV ICON OPEN/CLOSE */
$('.nav-menu-icon').click(function (e) {
  e.preventDefault();
  $('.nav-mobile-navbar').toggleClass('navbar-active')
  $('.nav-menu-icon').toggleClass('icon-active')
})


$('.icon-place').click(function (e) {

})

$('.hidden-layer').click(function () {
  $(".nav-mobile-navbar").removeClass("navbar-active")
})


/* WHEN REFRESH PAGE NAVBAR STAY SAME */
$(document).ready(function () {
  changeNav();
})

$(window).bind('scroll', changeNav);

//  /* WHEN SCROLL CHANGES , ALSO CHANGES THE NAVBAR  */
function changeNav() {
  if ($(window).scrollTop() > 80) {
    $('#desktop-index-nav .nav-left').removeClass("nav-home")
    $('#desktop-index-nav .nav-right').removeClass("nav-home")
    $('#desktop-index-nav .nav-space').css({
      "border-left-color": "white",
      "border-bottom-color": "#ea320c",
    })
    $('#desktop-index-nav .nav-contact .select-items').removeClass('nav-home')
  } else {
    $('#desktop-index-nav .nav-left').addClass("nav-home")
    $('#desktop-index-nav .nav-right').addClass("nav-home")
    $('#desktop-index-nav .nav-space').css({
      "border-left-color": "transparent",
      "border-bottom-color": "transparent",
    })
    $('#desktop-index-nav .nav-contact .select-items').addClass('nav-home')

  }
}

/* OWL CAROUSEL FOR SLIDE */

$('.owl-slide').owlCarousel({
  loop: true,
  dots: false,
  rewind: true,
  nav: true,
  navText: [$('.left-icon'), $('.right-icon')],
  responsiveClass: true,
  responsive: {
    0: {
      items: 1,
      nav: true,
      loop: true
    },
    600: {
      items: 3,
      nav: true,
      loop: true
    },
    768: {
      items: 3,
      nav: true,
      loop: true
    },
    1000: {
      items: 4,
      nav: true,
      loop: true
    },
    1326: {
      items: 5,
      nav: true,
      loop: true
    }
  }

})

/* OWL CAROUSEL FOR STATICTICS */

$('.owl-statistics').owlCarousel({
  loop: true,
  margin: 10,
  nav: false,
  dots: false,
  autoplay: true,
  autoplayTimeout: 2000,
  autoplayHoverPause: true,
  responsiveClass: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 2
    },
    1000: {
      items: 3
    }
  }
})



/* WHEN CHANGE DETAIL NAME HEIGTH CHANGES, CHANGE RESPONSIVELESS  */

var detailNameHeight = $(".details-name")

$(document).ready(function () {
  if ($(window).width() < 768) {
    if (detailNameHeight.height() < 50) {
      $(".owl-details").css({ "top": "60px" })
    }
    else if (detailNameHeight.height() < 75) {
      $(".owl-details").css({ "top": "90px" })
    }
    else if (detailNameHeight.height() < 100) {
      $(".owl-details").css({ "top": "110px" })
    }
    else if (detailNameHeight.height() < 125) {
      $(".owl-details").css({ "top": "130px" })
    }
    else if (detailNameHeight.height() < 150) {
      $(".owl-details").css({ "top": "160px" })
    }
    else if (detailNameHeight.height() < 175) {
      $(".owl-details").css({ "top": "180px" })
    }
    else if (detailNameHeight.height() < 200) {
      $(".owl-details").css({ "top": "200px" })
    }
    else if (detailNameHeight.height() < 225) {
      $(".owl-details").css({ "top": "225px" })
    }
    else if (detailNameHeight.height() < 250) {
      $(".owl-details").css({ "top": "250px" })
    }
    else if (detailNameHeight.height() < 275) {
      $(".owl-details").css({ "top": "275px" })
    }
    else if (detailNameHeight.height() < 300) {
      $(".owl-details").css({ "top": "300px" })
    }
  }
})


/* OWL CAROUSEL FOR DETAILS */
$(function () {
  var owl = $('.owl-details');
  owl.owlCarousel({
    margin: 10,
    nav: false,
    dots: false,
    autoplay: false,
    items: 1,
    onInitialized: counter, //When the plugin has initialized.
    onTranslated: counter //When the translation of the stage has finished.
  });

  function counter(event) {
    var element = event.target;         // DOM element, in this example .owl-carousel
    var items = event.item.count ;     // Number of items
    var item = event.item.index + 1 ;     // Position of the current item

    // it loop is true then reset counter from 1
    if (item > items) {
      item = item - items
    }
    $('.counter').html( item + "/" + items)
  }
}
);


/* MODAL SIGN UP */

$('.nav-sign-up').click(function (e) {
  e.preventDefault();
  $('.modal-sign-up').fadeIn("fast");
})
$('.close').click(function () {
  $('.modal-sign-up').fadeOut("fast");
})


/* MODAL LOG IN */

$('.nav-log-in').click(function (e) {
  e.preventDefault();
  $('.modal-log-in').fadeIn("fast");
})
$('.close').click(function () {
  $('.modal-log-in').fadeOut("fast");
})


/* CATEGORY SELECTED */

var categories = $("#ctgry  a");
var newActiveCategory;
categories.each(function (i, el) {
  $(this).click(function (e) {
    //e.preventDefault();
    var oldActiveCategory = $("#ctgry a.ctgry-active");
    oldActiveCategory.removeClass("ctgry-active");

    newActiveCategory = $(this).addClass("ctgry-active");
  })
});


/* CATEGORY FOR MOBILE VERSION */
var mobileh5 = $("#ctgry h5");
if ($(document).width() <= 992) {
  mobileh5.click(function () {
    mobileh5.toggleClass("h5-active")
    $(".ctgry-hr").fadeToggle()
    $("#ctgry ul").fadeToggle()
  });
}


/* CHANGE THE PAGINATION'S ARROW ICONS FOR MOBILE VERSION */
var paginationLeft = $(".p-left img")
var paginationRight = $(".p-right img")

if($(document).width() <= 577){
  paginationLeft.attr("src", "img/pagination-left-16.png");
  paginationRight.attr("src", "img/pagination-right-16.png")
}

/* DROPDOWN FOR LANGUAGE AND DATEPICKER LOCATION */

var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {

  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
      and the selected item:*/
      var y, i, k, s, h;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      h = this.parentNode.previousSibling;
      for (i = 0; i < s.length; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          for (k = 0; k < y.length; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
    and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {

  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {

    if (elmnt == y[i]) {
      arrNo.push(i);

      $(elmnt).next('.select-items').slideToggle(400);

    } else {
      y[i].classList.remove("select-arrow-active"); 4
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {

      x[i].classList.add("select-hide");
    }
  }
}





