﻿$(document).ready(function () {
    var lang = window.location.pathname.split("/")[1];
    $lastModal = $(".last-modal");

    $.ajax({
        url: "/" + lang + "/account/login",
    })
        .done(function (data) {
            $lastModal.after(data);
        });

});