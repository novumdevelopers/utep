﻿$(document).ready(function () {
    allCities();
});

function allCities() {
    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[1];

    $.ajax({
        type: 'Get',
        url: '/' + lang + '/home/getCities',
        success: function (result) {
            //$("#Cities").empty();
            $.each(result, function (i, item) {
                $('#Cities').append('<option value="' + item.Value + '"> ' + item.Text + ' </option>');
                //$('#Cities').parent().find(".select-items").append("<a class='currentCities' style='height:100% !important;width:100% !important;' onclick='getCity.call(this)' data-value=" + item.Value + " ><div>" + item.Text + "</div></a>");
            });
        },
        error: function (xhr, status, err) {
            console.log(err);
        }
    });
}