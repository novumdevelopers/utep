﻿function formatDate(udate) {
    var date = new Date(udate);

    var month = new Array();
    month[0] = ["1", "January"];
    month[1] = ["2", "February"];
    month[2] = ["3", "March"];
    month[3] = ["4", "April"];
    month[4] = ["5", "May"];
    month[5] = ["6", "June"];
    month[6] = ["7", "July"];
    month[7] = ["8", "August"];
    month[8] = ["9", "September"];
    month[9] = ["10", "October"];
    month[10] = ["11", "November"];
    month[11] = ["12", "December"];

    function a(b) {
        return b < 10 ? "0" + b : b;
    }

    var formattedDate = a(date.getDate()) + ' - ' + a(month[date.getMonth()][0]) + ' - ' + date.getFullYear();
    return formattedDate;
}
