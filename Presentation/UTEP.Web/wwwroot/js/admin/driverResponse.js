﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    $("#driverresponseIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/driverprofile/" + lang + "/driverResponse/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [9],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            {
                data: function (data) {
                    return (data.Driver.Status == 2) ? data.Driver.Name + " <i class='fa fa-times text-danger'></i>" : data.Driver.Name;
                },
                name: "Driver" },
            {
                data: function (data) {
                    return (data.Truck.Status == 2) ? data.Truck.Name + " <i class='fa fa-times text-danger'></i>" : data.Truck.Name;
                },
                name: "Truck" },
            {
                data: function (data) {
                    return (data.Trailer.Status == 2) ? data.Trailer.Name + " <i class='fa fa-times text-danger'></i>" : data.Trailer.Name;
                },
                name: "Trailer" },
            {
                data: function (data) {
                    return (data.CompanyOrder.Status == 2) ? data.CompanyOrder.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyOrder.Name;
                },
                name: "CompanyOrder" },
            { data: "EstimatePrice", name: "EstimatePrice" },
            {
                data: function (data) {
                    return formatDate(data.StartDate);
                },
                name: "StartDate"
            },
            {
                data: function (data) {
                    return formatDate(data.EndDate);
                },
                name: "EndDate"
            },
            { data: "DayCount", name: "DayCount" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/driverprofile/" + lang + "/driverResponse/edit/" +
                        data.Id +
                        "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
                }
            }
        ],
        "order": [[8, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });
});
