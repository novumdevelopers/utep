﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[1];

    var langUrl;
    var edit;
    var deleteItem;
    if (lang === "az") {
        langUrl = "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json";
        edit = "Redakə";
        deleteItem = "Sil";
    }
    else {
        langUrl = "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Russian.json";
        edit = "Отредактировать";
        deleteItem = "Удалить";
    }

    var tableIndex = $("#companyorderIndexTable").DataTable({
        "language": {
            "url": langUrl
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/" + lang + "/companyprofile/companyorder/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [11],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "Currency", name: "Currency" },
            { data: "From", name: "From" },
            { data: "To", name: "To" },
            { data: "Name", name: "Name" },
            //{ data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "Weight", name: "Weight" },
            { data: "PaletteCount", name: "PaletteCount" },
            //{
            //    data: function (data) {
            //        return (data.Company.Status === 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
            //    },
            //    name: "Company"
            //},
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.DeadLine);
                },
                name: "DeadLine"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/" + lang + "/companyprofile/companyorder/edit/" +
                        data.Id +
                        "' class='btn btn-outline blue btn-xs'>  <i class='fa fa-edit'></i> " + edit + " </a>" +
                        "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs delete-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> " + deleteItem + "</a>";
                }
            }
        ],
        "order": [[10, "desc"]],
        "scrollX": true,
        "scrollCollapse": true

    });

    var tableTrash = $("#companyorderTrashTable").DataTable({
        "language": {
            "url": langUrl
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/" + lang + "/companyprofile/companyorder/TrashData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [11],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "Currency", name: "Currency" },
            { data: "From", name: "From" },
            { data: "To", name: "To" },
            { data: "Name", name: "Name" },
            //{ data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "Weight", name: "Weight" },
            { data: "PaletteCount", name: "PaletteCount" },
            //{
            //    data: function (data) {
            //        return (data.Company.Status === 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
            //    },
            //    name: "Company"
            //},
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.DeadLine);
                },
                name: "DeadLine"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> Geri Qaytar</a>";
                }
            }
        ],
        "order": [[10, "desc"]],
        "scrollX": true,
        "scrollCollapse": true

    });

    var tableWaited = $("#companyorderWaitedTable").DataTable({
        "language": {
            "url": langUrl
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/" + lang + "/companyprofile/companyorder/WaitedData",
            "type": "POST",
            "datatype": "json"
        },
        //"columnDefs":
        //    [{
        //        "targets": [9],
        //        "visible": true,
        //        "searchable": false,
        //        "orderable": false
        //    }],
        "columns": [
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "Currency", name: "Currency" },
            { data: "From", name: "From" },
            { data: "To", name: "To" },
            { data: "Name", name: "Name" },
            //{ data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "Weight", name: "Weight" },
            { data: "PaletteCount", name: "PaletteCount" },
            //{
            //    data: function (data) {
            //        return (data.Company.Status === 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
            //    },
            //    name: "Company"
            //},
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.DeadLine);
                },
                name: "DeadLine"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            }
            //{
            //    data: function (data) {
            //        return "<a id='" +
            //            data.Id +
            //            "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> Geri Qaytar</a>";
            //    }
            //}
        ],
        "order": [[10, "desc"]],
        "scrollX": true,
        "scrollCollapse": true

    });


    tableIndex.on('click',
        '.delete-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/" + lang + "/companyprofile/CompanyOrder/Trash/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableIndex.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });


    tableTrash.on('click',
        '.back-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/" + lang + "/companyprofile/CompanyOrder/Back/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableTrash.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });
});
