﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    $("#roleIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json" /*"~/Plugins/DataTables/i18n/Azerbaijan.lang"*/
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/Role/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [1],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "Name", name: "Name" },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/role/edit/" +
                        data.Id +
                        "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
                }
            }
        ],
        "order": [[0, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,

    });
});