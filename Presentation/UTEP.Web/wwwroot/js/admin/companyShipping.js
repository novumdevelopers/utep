﻿$(document).ready(function () {


    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    var tableIndex = $("#companyshippingIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/admin/" + lang + "/companyshipping/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [7],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            {
                data: function (data) {
                    return (data.CompanyOrder.Status == 2) ? data.CompanyOrder.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyOrder.Name;
                },
                name: "CompanyOrder"
            },
            { data: "OriginTownLongitude", name: "OriginTownLongitude" },
            { data: "OriginTownLatitude", name: "OriginTownLatitude" },
            { data: "DestinationTownLongitude", name: "DestinationTownLongitude" },
            { data: "DestinationTownLatitude", name: "DestinationTownLatitude" },
            {
                data: function (data) {
                    return data.Deadline != null ? formatDate(data.Deadline) : "Qeyd olunmayıb";
                },
                name: "Deadline"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/companyshipping/edit/" +
                        data.Id +
                        "' class='btn btn-outline btn-xs'>  <i class='fa fa-edit'></i> Edit </a>" +
                        "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs delete-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> Sil</a>";
                }
            }
        ],
        "order": [[6, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });

    var tableTrash = $("#companyshippingTrashTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/companyshipping/TrashData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [7],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            {
                data: function (data) {
                    return (data.CompanyOrder.Status == 2) ? data.CompanyOrder.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyOrder.Name;
                }, name: "CompanyOrder"
            },
            { data: "OriginTownLongitude", name: "OriginTownLongitude" },
            { data: "OriginTownLatitude", name: "OriginTownLatitude" },
            { data: "DestinationTownLongitude", name: "DestinationTownLongitude" },
            { data: "DestinationTownLatitude", name: "DestinationTownLatitude" },
            {
                data: function (data) {
                    return data.Deadline != null ? formatDate(data.Deadline) : "Qeyd olunmayıb";
                },
                name: "Deadline"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Geri Qaytar</a>";
                }
            }
        ],
        "order": [[6, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });

    tableIndex.on('click',
        '.delete-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/CompanyShipping/Trash/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableIndex.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableTrash.on('click',
        '.back-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/CompanyShipping/Back/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableTrash.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });
});

