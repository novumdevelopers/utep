﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    $("#companyclientIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/companyProfile/" + lang + "/companyClient/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [12],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "CompanyName", name: "CompanyName" },
            { data: "CompanyEmail", name: "CompanyEmail" },
            { data: "CompanyPhone", name: "CompanyPhone" },
            { data: "LocationLatitude", name: "LocationLatitude" },
            { data: "LocationLongitude", name: "LocationLongitude" },
            { data: "Fax", name: "Fax" },

            {
                data: function (data) {
                    return (data.CompanyType.Status == 2) ? data.CompanyType.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyType.Name;
                },
                 name: "CompanyType" },

            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "Email", name: "Email" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/companyProfile/" + lang + "/companyClient/edit/" +
                        data.Id +
                        "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
                }
            }
        ],
        "order": [[11, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });
});
