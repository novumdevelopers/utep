﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Models;

namespace UTEP.Web.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender, ISmsSender
    {
        private readonly IOptions<SmtpConfig> _smtpConfig;
        public IConfiguration Configuration { get; set; }

        public EmailSender(IOptions<SmtpConfig> smtpConfig)
        {
            _smtpConfig = smtpConfig;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("UTEP", _smtpConfig.Value.SmtpUserEmail));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = message;
            bodyBuilder.TextBody = message;

            emailMessage.Body = bodyBuilder.ToMessageBody();
            //new TextPart("plain") { Text = message };

            await Task.Run(() =>
            {
                using (var client = new SmtpClient())
                {
                    // For another mail services
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(_smtpConfig.Value.SmtpHost, _smtpConfig.Value.SmtpPort, SecureSocketOptions.Auto /*false*/);

                    //client.AuthenticationMechanisms.Remove("XOAUTH2");

                    client.Authenticate(_smtpConfig.Value.SmtpUserEmail, _smtpConfig.Value.SmtpPassword);

                    client.Send(emailMessage);

                    client.Disconnect(true);
                }
            });
            //return Task.CompletedTask;
        }

        public Task SendSmsAsync(string number, string message)
        {
            return Task.FromResult(0);
            //throw new NotImplementedException();
        }
    }
}
