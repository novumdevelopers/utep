﻿using System.Drawing;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace UTEP.Web.Services
{
    public interface IFileProcessor
    {
        bool ApplyWatermark(string filename, string watermarkText, string folder);
        string CreateThumbnailCentered(int targetWidth, int targetHeight, string path, string folder, string waterMark);
        string CreateThumbnail(int maxWidth, int maxHeight, string path);
        string CreateThumbnailHeight(int maxHeight, string path);
        string CreateThumbnailWidth(int maxWidth, string path);
        string GetFileName(IFormFile file);
        string GetFileSize(long fileLength, int decimalPlaces = 1);
        string GetPathAndFilename(string filename);
        string GetPathAndFileName(string filename, string folderName);
        Image ResizeImage(int newWidth, int newHeight, string stPhotoPath);
        string UploadFile(string path, string folder);
        Task<string> UploadFileAsync(IFormFile file, string folder, string[] allowedExtensions = null);
        Task<string> UploadFileAsync(IFormFile file, string[] allowedExtensions);
    }
}