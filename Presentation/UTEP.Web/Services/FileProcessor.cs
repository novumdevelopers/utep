﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace UTEP.Web.Services
{
    public class FileProcessor : IFileProcessor
    {
        private IHostingEnvironment hostingEnvironment;

        public FileProcessor(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        static readonly string[] SizeSuffixes = { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        public readonly string[] AllowedExtensions = { ".jpg", ".png", ".jpeg", ".bmp" };

        /*upload*/
        public string UploadFile(string path, string folder)
        {
            var image = System.Drawing.Image.FromFile(path);

            string filename = Path.GetFileName(path);

            var extension = filename.Substring(filename.LastIndexOf(".", StringComparison.Ordinal)).ToLower();
            filename = Guid.NewGuid().ToString().Replace("-", "") + extension;

            image.Save(GetPathAndFileName(filename, $"\\{folder}\\"), image.RawFormat);
            image.Dispose();

            return filename;
        }

        public async Task<string> UploadFileAsync(IFormFile file, string[] allowedExtensions)
        {
            var filename = GetFileName(file);
            var extension = filename.Substring(filename.LastIndexOf(".", StringComparison.Ordinal)).ToLower();
            filename = Guid.NewGuid().ToString().Replace("-", "") + extension;

            if (allowedExtensions == null || allowedExtensions.Any(x => x == extension))
            {
                var contentType = file.ContentType;
                var lenght = file.Length;
                using (FileStream output = System.IO.File.Create(this.GetPathAndFilename(filename)))
                    await file.CopyToAsync(output);

                return filename;
            }
            return "";
        }

        public async Task<string> UploadFileAsync(IFormFile file, string folder, string[] allowedExtensions = null)
        {
            var filename = GetFileName(file);
            var extension = filename.Substring(filename.LastIndexOf(".", StringComparison.Ordinal)).ToLower();
            filename = Guid.NewGuid().ToString().Replace("-", "") + extension;

            if (allowedExtensions == null || allowedExtensions.Any(x => x == extension))
            {
                var contentType = file.ContentType;
                var lenght = file.Length;
                using (FileStream output = System.IO.File.Create(this.GetPathAndFileName(filename, folder)))
                    await file.CopyToAsync(output);

                return filename;
            }
            return "";
        }

        /*watermark*/
        public bool ApplyWatermark(string filename, string watermarkText, string folder)
        {
            try
            {
                var bitmapa = Image.FromFile(filename);
            }
            catch (Exception ex)
            {
                return false;
            }
            using (var bitmap = Image.FromFile(filename))
            {
                using (var tempBitmap = new Bitmap(bitmap.Width, bitmap.Height, PixelFormat.Format32bppPArgb))
                {
                    using (Graphics grp = Graphics.FromImage(tempBitmap))
                    {
                        grp.CompositingQuality = CompositingQuality.HighQuality;
                        grp.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        var size = new Size(bitmap.Width, bitmap.Height);
                        grp.DrawImage(bitmap, new Rectangle(Point.Empty, size));
                        bitmap.Dispose();

                        //Bottom
                        //Brush brush = new SolidBrush(Color.FromArgb(130, 255, 0, 0));
                        //Font font = new Font(FontFamily.GenericSansSerif, 35, FontStyle.Bold, GraphicsUnit.Pixel);
                        //SizeF textSize = grp.MeasureString(watermarkText, font);
                        //var x = (tempBitmap.Width - ((int)textSize.Width + 10));
                        //var y = (tempBitmap.Height - ((int)textSize.Height + 10));
                        //Point position = new Point(x, y);
                        //grp.DrawString(watermarkText, font, brush, position);

                        //Center
                        float emSize = tempBitmap.Height;
                        Font fontC = new Font(FontFamily.GenericSansSerif, emSize, FontStyle.Bold);
                        fontC = FindBestFitFont(grp, watermarkText, fontC, size);
                        SizeF sizeC = grp.MeasureString(watermarkText, fontC);

                        var xC = (int)(tempBitmap.Width - sizeC.Width) / 2;
                        var yC = (int)(tempBitmap.Height - sizeC.Height) / 2;
                        Point positionC = new Point(xC, yC);

                        grp.DrawString(watermarkText, fontC, new SolidBrush(Color.FromArgb(80, 100, 100, 100)), positionC);

                        //var folder = $"company\\product\\medium\\";
                        var newFilename = watermarkText + Path.GetFileName(filename);
                        var filenameForSave = GetPathAndFileName(newFilename, folder);

                        tempBitmap.Save(filenameForSave);
                    }
                }
                return true;
            }
        }

        private Font FindBestFitFont(Graphics g, String text, Font font, Size proposedSize)
        {
            // Compute actual size, shrink if needed
            while (true)
            {
                SizeF size = g.MeasureString(text, font);

                // It fits, back out
                if (size.Height <= proposedSize.Height &&
                     size.Width <= proposedSize.Width) { return font; }

                // Try a smaller font (90% of old size)
                Font oldFont = font;
                font = new Font(font.Name, (float)(font.Size * .9), font.Style);
                oldFont.Dispose();
            }
        }

        public string CreateThumbnailCentered(int targetWidth, int targetHeight, string path, string folder, string waterMark)
        {
            Image image = Image.FromFile(path);
            ImageCodecInfo jpgInfo = ImageCodecInfo.GetImageEncoders().Where(codecInfo => codecInfo.MimeType == "image/jpeg").First();
            Image finalImage = image;
            System.Drawing.Bitmap bitmap = null;
            try
            {
                int left = 0;
                int top = 0;
                int srcWidth = targetWidth;
                int srcHeight = targetHeight;
                bitmap = new System.Drawing.Bitmap(targetWidth, targetHeight);
                double croppedHeightToWidth = (double)targetHeight / targetWidth;
                double croppedWidthToHeight = (double)targetWidth / targetHeight;

                if (image.Width > image.Height)
                {
                    srcWidth = (int)(Math.Round(image.Height * croppedWidthToHeight));
                    if (srcWidth < image.Width)
                    {
                        srcHeight = image.Height;
                        left = (image.Width - srcWidth) / 2;
                    }
                    else
                    {
                        srcHeight = (int)Math.Round(image.Height * ((double)image.Width / srcWidth));
                        srcWidth = image.Width;
                        top = (image.Height - srcHeight) / 2;
                    }
                }
                else
                {
                    srcHeight = (int)(Math.Round(image.Width * croppedHeightToWidth));
                    if (srcHeight < image.Height)
                    {
                        srcWidth = image.Width;
                        top = (image.Height - srcHeight) / 2;
                    }
                    else
                    {
                        srcWidth = (int)Math.Round(image.Width * ((double)image.Height / srcHeight));
                        srcHeight = image.Height;
                        left = (image.Width - srcWidth) / 2;
                    }
                }
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.DrawImage(image, new Rectangle(0, 0, bitmap.Width, bitmap.Height), new Rectangle(left, top, srcWidth, srcHeight), GraphicsUnit.Pixel);
                }
                finalImage = bitmap;
            }
            catch { }
            try
            {
                using (EncoderParameters encParams = new EncoderParameters(1))
                {
                    encParams.Param[0] = new EncoderParameter(Encoder.Quality, (long)100);
                    string fileRelativePath = /*"thumbnail_" + targetWidth + "x" + targetHeight + "_" +*/ waterMark + Path.GetFileName(path);
                    //string a = fileRelativePath.Replace(waterMark, string.Empty);
                    finalImage.Save(GetPathAndFileName(fileRelativePath, string.Format("\\{0}", folder)), jpgInfo, encParams);
                    return fileRelativePath;
                }
            }
            catch { }
            if (bitmap != null)
            {
                bitmap.Dispose();
            }
            return null;
        }

        /*thumbnail*/
        public string CreateThumbnail(int maxWidth, int maxHeight, string path)
        {

            var image = System.Drawing.Image.FromFile(path);
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth, newHeight);
            Graphics thumbGraph = Graphics.FromImage(newImage);

            thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
            thumbGraph.PixelOffsetMode = PixelOffsetMode.HighQuality;
            thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
            image.Dispose();

            string fileRelativePath = /*"_" + maxWidth + "x" + maxHeight + "_" +*/ Path.GetFileName(path);
            newImage.Save(GetPathAndFileName(fileRelativePath, "\\thumbnail\\"), newImage.RawFormat);
            return fileRelativePath;
        }

        internal Task UploadFileAsync()
        {
            throw new NotImplementedException();
        }

        public string CreateThumbnailWidth(int maxWidth, string path)
        {

            var image = System.Drawing.Image.FromFile(path);
            var ratio = (double)maxWidth / image.Width;
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth, newHeight);
            Graphics thumbGraph = Graphics.FromImage(newImage);

            thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
            thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
            //thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
            image.Dispose();

            string fileRelativePath = String.Format("{0}_{1}", Path.GetFileName(path), "_" + maxWidth);
            newImage.Save(GetPathAndFileName(fileRelativePath, "\\thumbnail\\"), newImage.RawFormat);
            return fileRelativePath;
        }

        public string CreateThumbnailHeight(int maxHeight, string path)
        {

            var image = System.Drawing.Image.FromFile(path);
            var ratio = (double)maxHeight / image.Height;
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth, newHeight);
            Graphics thumbGraph = Graphics.FromImage(newImage);

            thumbGraph.CompositingQuality = CompositingQuality.HighSpeed;
            thumbGraph.SmoothingMode = SmoothingMode.HighSpeed;
            //thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;

            thumbGraph.DrawImage(image, 0, 0, newWidth, newHeight);
            image.Dispose();

            string fileRelativePath = String.Format("{0}_{1}", Path.GetFileName(path), "_" + maxHeight);
            newImage.Save(GetPathAndFileName(fileRelativePath, "\\thumbnail\\"), newImage.RawFormat);
            return fileRelativePath;
        }
        /*resize*/
        public Image ResizeImage(int newWidth, int newHeight, string stPhotoPath)
        {
            Image imgPhoto = Image.FromFile(stPhotoPath);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;

            //Consider vertical pics
            if (sourceWidth < sourceHeight)
            {
                int buff = newWidth;

                newWidth = newHeight;
                newHeight = buff;
            }

            int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
            float nPercent = 0, nPercentW = 0, nPercentH = 0;

            nPercentW = ((float)newWidth / (float)sourceWidth);
            nPercentH = ((float)newHeight / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((newWidth -
                                                (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((newHeight -
                                                (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);


            Bitmap bmPhoto = new Bitmap(newWidth, newHeight,
                PixelFormat.Format24bppRgb);

            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White);
            grPhoto.InterpolationMode =
                System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            imgPhoto.Dispose();

            return bmPhoto;
        }

        /*crop*/
        private Image CropImage(string path, int sourceX, int sourceY, int sourceWidth, int sourceHeight, int destinationWidth, int destinationHeight)
        {
            var sourceImage = System.Drawing.Image.FromFile(path);
            Image destinationImage = new Bitmap(destinationWidth, destinationHeight);
            Graphics g = Graphics.FromImage(destinationImage);

            g.DrawImage(
              sourceImage,
              new Rectangle(0, 0, destinationWidth, destinationHeight),
              new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
              GraphicsUnit.Pixel
            );

            return destinationImage;
        }

        /*getfilesize*/
        public string GetFileSize(Int64 fileLength, int decimalPlaces = 1)
        {
            if (fileLength < 0) { return "-" + GetFileSize(-fileLength); }

            int i = 0;
            decimal dValue = (decimal)fileLength;
            while (Math.Round(dValue, decimalPlaces) >= 1000)
            {
                dValue /= 1024;
                i++;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}", dValue, SizeSuffixes[i]);
        }

        /*EnsureCorrectFilename*/
        private string EnsureCorrectFilename(string filename)
        {
            if (filename.Contains("\\"))
                filename = filename.Substring(filename.LastIndexOf("\\") + 1);

            return filename;
        }

        /*GetFileName*/
        public string GetFileName(IFormFile file)
        {
            string filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            filename = this.EnsureCorrectFilename(filename);
            return filename;
        }

        /*GetPathAndFilename*/
        public string GetPathAndFilename(string filename)
        {
            string path = this.hostingEnvironment.WebRootPath + "\\uploads\\";

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path + filename;
        }

        public string GetPathAndFileName(string filename, string folderName)
        {
            string path = this.hostingEnvironment.WebRootPath + "\\uploads\\" + folderName;

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return path + filename;
        }
    }
}
