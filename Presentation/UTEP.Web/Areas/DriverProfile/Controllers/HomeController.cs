﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace UTEP.Web.Areas.DriverProfile.Controllers
{
    [Area("DriverProfile")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}