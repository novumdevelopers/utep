﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;

namespace UTEP.Web.Areas.DriverProfile.Controllers
{
    [Area("DriverProfile")]
    [Route("/[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class OfferToDriverController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public OfferToDriverController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

    }
}