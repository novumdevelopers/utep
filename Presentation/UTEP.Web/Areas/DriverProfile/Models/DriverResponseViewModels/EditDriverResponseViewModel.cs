﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.DriverProfile.Models.DriverResponseViewModels
{
    public class EditDriverResponseViewModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long DriverId { get; set; }
        public IEnumerable<SelectListItem> Drivers { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long TruckId { get; set; }
        public IEnumerable<SelectListItem> Trucks { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long TrailerId { get; set; }
        public IEnumerable<SelectListItem> Trailers { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long CompanyOrderId { get; set; }
        public IEnumerable<SelectListItem> CompanyOrders { get; set; }

        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public decimal EstimatePrice { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public DateTime EndDate { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public int DayCount { get; set; }
    }
}
