﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.TrailerViewModels;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Web.Services;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class TrailerController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IFileProcessor _fileProcessor;
        public TrailerController(
            DbContextOptions<UTEPDbContext> contextOptions,
            IFileProcessor fileProcessor
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileProcessor = fileProcessor;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Trailers.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateTrailerViewModel
            {
                TrailerTypes = _unitOfWork.TrailerTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.Name
                }),
                VehicleModels = _unitOfWork.VehicleModels.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.Name
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateTrailerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var trailer = new Trailer
                {
                    Size=model.Size,
                    Tonnage = model.Tonnage,
                    PaletteCount = model.PaletteCount,
                    RegistrationNumber = model.RegistrationNumber,
                    VehicleModelId = model.VehicleModelId,
                    TrailerTypeId = model.TrailerTypeId
                };
                if (model.RegistrationCertificate != null && !String.IsNullOrEmpty(model.RegistrationCertificate?.Name) && model.RegistrationCertificate.Length > 0)
                {
                    string folder = $"trailer\\";
                    var ae = new string[] { ".jpg", ".png", ".jpeg" };
                    trailer.RegistrationCertificate = await _fileProcessor.UploadFileAsync(model.RegistrationCertificate, folder, ae);
                };
                try
                {
                    _unitOfWork.Trailers.Add(trailer);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }

            model.TrailerTypes = _unitOfWork.TrailerTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            model.VehicleModels = _unitOfWork.VehicleModels.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {

            var trailer = _unitOfWork.Trailers.Get(x => x.Id == id && x.Status == Status.Active);
            if (trailer != null)
            {
                var model = new EditTrailerViewModel
                {
                    Id = trailer.Id,
                    Size=trailer.Size,
                    Tonnage = trailer.Tonnage,
                    PaletteCount = trailer.PaletteCount,
                    RegistrationNumber = trailer.RegistrationNumber,
                    ImagePath = trailer.RegistrationCertificate,
                    VehicleModelId = trailer.VehicleModelId,
                    VehicleModels = _unitOfWork.VehicleModels.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    }),
                    TrailerTypeId = trailer.TrailerTypeId,
                    TrailerTypes = _unitOfWork.TrailerTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    })
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditTrailerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var trailer = _unitOfWork.Trailers.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (trailer != null)
                {
                    trailer.Size = model.Size;
                    trailer.Tonnage = model.Tonnage;
                    trailer.PaletteCount = model.PaletteCount;
                    trailer.RegistrationNumber = model.RegistrationNumber;
                    trailer.VehicleModelId = model.VehicleModelId;
                    trailer.TrailerTypeId = model.TrailerTypeId;
                    if (model.RegistrationCertificate != null && !String.IsNullOrEmpty(model.RegistrationCertificate?.Name) && model.RegistrationCertificate.Length > 0)
                    {
                        string folder = $"trailer\\";
                        var ae = new string[] { ".jpg", ".png", ".jpeg" };
                        trailer.RegistrationCertificate = await _fileProcessor.UploadFileAsync(model.RegistrationCertificate, folder, ae);
                    };
                    try
                    {
                        _unitOfWork.Trailers.Update(trailer);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            model.TrailerTypes = _unitOfWork.TrailerTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            model.VehicleModels = _unitOfWork.VehicleModels.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Trailers.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var trailer = _unitOfWork.Trailers.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (trailer != null)
                {
                    trailer.Status = Status.Active;
                    try
                    {
                        _unitOfWork.Trailers.Update(trailer);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var trailer = _unitOfWork.Trailers.Get(x => x.Id == id && x.Status == Status.Active);
                if (trailer != null)
                {
                    trailer.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.Trailers.Update(trailer);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}

