﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models;
using UTEP.Web.Areas.Admin.Models.UserGroupViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class UserGroupController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly UnitOfWork _unitOfWork;

        public UserGroupController(UserManager<User> userManager,
            RoleManager<Role> roleManager,
            DbContextOptions<UTEPDbContext> contextOptions)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.UserGroups.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        //[AjaxOnly]
        public IActionResult List()
        {
            var list = _unitOfWork.UserGroups.Find(x => x.Status == Status.Active).Select(x => new
            {
                x.Id,
                x.Name,
                UserClaims = x.UserGroupClaims.Select(y => new
                {
                    Name = y.ClaimData
                }),
                AddedDate = x.AddedDate.ToString("MM/dd/yyyy HH:mm")
            }).ToList();
            return Json(list);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateUserGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userGroup = new UserGroup
                {
                    Name = model.Name
                };
                try
                {
                    _unitOfWork.UserGroups.Add(userGroup);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();

                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Something went wrong. Please try again.");
                    return View(model);
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {

            var userGroupInDb = _unitOfWork.UserGroups.Get(x => x.Id == id && x.Status == Status.Active);
            if (userGroupInDb == null)
            {
                return RedirectToAction("Error", "Home");
            }
            var model = new EditUserGroupViewModel
            {
                Id = userGroupInDb.Id,
                Name = userGroupInDb.Name
            };
            var userGroupClaimsInDb = _unitOfWork.UserGroupClaims.Find(x => x.UserGroupId == id).ToList();
            model.UserGroupClaims = ClaimData.UserClaims.Select(claim => new SelectListItem
            {
                Text = claim,
                Value = claim,
            }).ToList();

            foreach (var UserGroupClaim in model.UserGroupClaims)
            {
                foreach (var UserGroupClaimInDb in userGroupClaimsInDb)
                {
                    if (UserGroupClaim.Text == UserGroupClaimInDb.ClaimData)
                    {
                        UserGroupClaim.Selected = true;
                    }
                }
            }
            return View(model);

            //return RedirectToAction("New");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditUserGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userGroupInDb = _unitOfWork.UserGroups.Get(x => x.Id == model.Id && x.Status == Status.Active);

                if (userGroupInDb == null)
                {
                    return RedirectToAction("Error", "Home");
                }

                userGroupInDb.Name = model.Name;

                var users = _userManager.Users.Where(x => x.UserGroupId == model.Id).ToList();
                var userGroupClaimsInDb = _unitOfWork.UserGroupClaims.Find(x => x.UserGroupId == userGroupInDb.Id).ToList();

                foreach (var userGroupsClaim in userGroupClaimsInDb)
                {
                    if (!model.UserGroupClaims.Find(u => u.Value == userGroupsClaim.ClaimData).Selected)
                    {
                        _unitOfWork.UserGroupClaims.Remove(userGroupsClaim);
                    }
                }

                foreach (var user in users)
                {
                    var claims = await _userManager.GetClaimsAsync(user);
                    var userRemoveClaims = claims.Where(c => model.UserGroupClaims.Any(u => u.Value == c.Value && !u.Selected)).ToList();

                    foreach (var claim in userRemoveClaims)
                    {
                        await _userManager.RemoveClaimAsync(user, claim);
                    }
                }

                var newUserClaimsForUsers = model.UserGroupClaims.Where(c => c.Selected).ToList();
                newUserClaimsForUsers.ForEach(x =>
                {
                    if (!userGroupClaimsInDb.Any(y => y.ClaimData == x.Text))
                    {
                        UserGroupClaim cla = new UserGroupClaim
                        {
                            UserGroupId = userGroupInDb.Id,
                            ClaimData = x.Text
                        };
                        userGroupInDb.UserGroupClaims.Add(cla);
                    }
                });

                foreach (var item in users)
                {
                    var updatedClaimsUser = await _userManager.GetClaimsAsync(item);

                    foreach (var claim in newUserClaimsForUsers)
                    {
                        if (!updatedClaimsUser.Any(x => x.Value == claim.Text))
                        {
                            item.Claims.Add(new IdentityUserClaim<long>
                            {
                                ClaimType = claim.Value,
                                ClaimValue = claim.Value
                            });
                        }
                    }
                    var result = await _userManager.UpdateAsync(item);
                }

                try
                {
                    _unitOfWork.UserGroups.Update(userGroupInDb);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();

                    return RedirectToAction("Index", "UserGroup");
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Something went wrong. Please try again.");
                    return View(model);
                }


            }
            return View(model);
        }
    }
}