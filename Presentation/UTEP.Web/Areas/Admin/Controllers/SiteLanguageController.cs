﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.SiteLanguageBasedGroup;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class SiteLanguageController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public SiteLanguageController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }
        

        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.SiteLanguages.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

         }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CreateSiteLanguageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var siteLanguage = new SiteLanguage
                {
                    Name = model.Name,
                    LanguageCode = model.Code.ToLower(),
                    OrderIndex = model.OrderIndex
                };
                try
                {
                    _unitOfWork.SiteLanguages.Add(siteLanguage);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();

                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var siteLanguageDb = _unitOfWork.SiteLanguages.Get(x => x.Id == id && x.Status == Status.Active);
            if (siteLanguageDb != null)
            {
                var model = new EditSiteLanguageViewModel
                {
                    Id = siteLanguageDb.Id,
                    Name = siteLanguageDb.Name,
                    Code = siteLanguageDb.LanguageCode,
                    OrderIndex = siteLanguageDb.OrderIndex
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditSiteLanguageViewModel model)
        {
            var siteLanguageDb = _unitOfWork.SiteLanguages.Get(x => x.Id == model.Id && x.Status == Status.Active);
            if (siteLanguageDb != null)
            {
                siteLanguageDb.Name = model.Name;
                siteLanguageDb.LanguageCode = model.Code.ToLower();
                siteLanguageDb.OrderIndex = model.OrderIndex;
                try
                {
                    _unitOfWork.SiteLanguages.Update(siteLanguageDb);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception e)
                {
                    throw;
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var siteLanguageDb = _unitOfWork.SiteLanguages.Get(x => x.Id == id && x.Status == Status.Active);
                if (siteLanguageDb != null)
                {
                    siteLanguageDb.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.SiteLanguages.Update(siteLanguageDb);
                        _unitOfWork.Complete();
                        return View("Index");
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return View();

        }


        public IActionResult TrashList()
        {
            var trashList = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Deleted)
                .Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name,
                    AddedDate = x.AddedDate
                });
            return Json(trashList);
        }







    }
}