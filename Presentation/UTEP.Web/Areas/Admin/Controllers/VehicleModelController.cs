﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Web.Areas.Admin.Models.VehicleModelViewModels;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Settings;
using UTEP.Web.Attribute;
using UTEP.Web.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class VehicleModelController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IFileProcessor _fileProcessor;
        public VehicleModelController(DbContextOptions<UTEPDbContext> contextOptions,
            IFileProcessor fileProcessor)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileProcessor = fileProcessor;
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.VehicleModels.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);

            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateVehicleModelViewModel
            {
                VehicleMarks = _unitOfWork.VehicleMarks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.Name
                }),
                VehicleTypes = _unitOfWork.VehicleTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.VehicleTypeTranslations.FirstOrDefault(x => x.Status == Status.Active).Name
                })
            };
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateVehicleModelViewModel model)
        {
            if (ModelState.IsValid)
            {
                var vehicleModel = new VehicleModel
                {
                    Name = model.Name,
                    VehicleMarkId = model.VehicleMarkId,
                    VehicleTypeId=model.VehicleTypeId
                };

                try
                {
                    _unitOfWork.VehicleModels.Add(vehicleModel);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();

                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }

            }

            model.VehicleMarks = _unitOfWork.VehicleMarks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            model.VehicleTypes = _unitOfWork.VehicleTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.VehicleTypeTranslations.FirstOrDefault(x => x.Status == Status.Active).Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var vehicleModel = _unitOfWork.VehicleModels.Get(x => x.Id == id && x.Status == Status.Active);
            if (vehicleModel != null)
            {
                var model = new EditVehicleModelViewModel
                {
                    Id = vehicleModel.Id,
                    Name = vehicleModel.Name,
                    VehicleTypeId=vehicleModel.VehicleTypeId,
                    VehicleTypes = _unitOfWork.VehicleTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.VehicleTypeTranslations.FirstOrDefault(x => x.Status == Status.Active).Name
                    }),
                    VehicleMarkId = vehicleModel.VehicleMarkId,
                    VehicleMarks = _unitOfWork.VehicleMarks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    })
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditVehicleModelViewModel model)
        {

            if (ModelState.IsValid)
            {
                var vehicleModel = _unitOfWork.VehicleModels.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (vehicleModel != null)
                {
                    vehicleModel.Name = model.Name;
                    vehicleModel.VehicleMarkId = model.VehicleMarkId;
                    vehicleModel.VehicleTypeId = model.VehicleTypeId;

                    try
                    {
                        _unitOfWork.VehicleModels.Update(vehicleModel);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();

                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            model.VehicleMarks = _unitOfWork.VehicleMarks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            model.VehicleTypes = _unitOfWork.VehicleTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.VehicleTypeTranslations.FirstOrDefault(x => x.Status == Status.Active).Name
            });
            return View(model);
        }

        [HttpGet]
        [AjaxOnly]
        public IActionResult GetVehicleModels(long id)
        {
            var vehicleMark = _unitOfWork.VehicleMarks.GetWithVehicleModels(x =>
                x.Status == Status.Active &&
                x.Id == id
            );
            if (vehicleMark != null)
            {
                var model = vehicleMark.VehicleModels.Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.Name
                });
                return Ok(model);
            }
            return NoContent();
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.VehicleModels.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var vehicleModel = _unitOfWork.VehicleModels.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (vehicleModel != null)
                {
                    vehicleModel.Status = Status.Active;
                    try
                    {
                        _unitOfWork.VehicleModels.Update(vehicleModel);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var vehicleModel = _unitOfWork.VehicleModels.Get(x => x.Id == id && x.Status == Status.Active);
                if (vehicleModel != null)
                {
                    vehicleModel.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.VehicleModels.Update(vehicleModel);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }
    }
}
