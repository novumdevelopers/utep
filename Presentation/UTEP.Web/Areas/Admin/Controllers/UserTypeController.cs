﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.UserTypeViewModels;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class UserTypeController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public UserTypeController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.UserTypes.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateUserTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userType = new UserType
                {
                    Name = model.Name
                };
                try
                {
                    _unitOfWork.UserTypes.Add(userType);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {

            var userType = _unitOfWork.UserTypes.Get(x => x.Id == id && x.Status == Status.Active);
            if (userType != null)
            {
                var model = new EditUserTypeViewModel
                {
                    Id = userType.Id,
                    Name = userType.Name
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditUserTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userType = _unitOfWork.UserTypes.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (userType != null)
                {
                    userType.Name = model.Name;
                    try
                    {
                        _unitOfWork.UserTypes.Update(userType);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return View(model);
        }

    }
}

