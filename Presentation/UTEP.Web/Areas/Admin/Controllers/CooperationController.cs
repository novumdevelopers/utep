﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CooperationBasedGroup;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models.CooperationViewModels;
using UTEP.Web.Attribute;
using UTEP.Web.Services;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class CooperationController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IFileProcessor _fileProcessor;
        public CooperationController(
            DbContextOptions<UTEPDbContext> contextOptions,
            IFileProcessor fileProcessor)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileProcessor = fileProcessor;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Cooperations.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }


        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateCooperationViewModel model, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                if (_unitOfWork.Cooperations.Any(x => x.Name == model.Name))
                {
                    ModelState.AddModelError("", $"{model.Name} adlı tərəfdaş artıq mövcuddur.");
                    return View(model);
                }

                var cooperation = new Cooperation
                {
                    Name = model.Name
                };

                if (file != null && !string.IsNullOrEmpty(file.Name) && file.Length > 0)
                {
                    var extensions = new string[] { ".jpg", ".jpeg", "png" };
                    var folder = $"\\cooperation\\";
                    var fileName = await _fileProcessor.UploadFileAsync(file, folder, extensions);
                    cooperation.Logo = fileName;
                }
                try
                {
                    _unitOfWork.Cooperations.Add(cooperation);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var cooperation = _unitOfWork.Cooperations.Get(x => x.Id == id && x.Status == Status.Active);
            if (cooperation != null)
            {
                var model = new EditCooperationViewModel
                {
                    Id = cooperation.Id,
                    Name = cooperation.Name,
                    LogoForView = cooperation.Logo
                };
                return View(model);
            }
            return RedirectToAction("Error", "Home", new { ara = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditCooperationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var cooperation = _unitOfWork.Cooperations.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (cooperation != null)
                {
                    cooperation.Name = model.Name;

                    if (model.Logo != null && !string.IsNullOrEmpty(model.Logo?.Name) && model.Logo.Length > 0)
                    {
                        var folder = $"\\cooperation\\";
                        var extensions = new string[] { ".jpg", ".jpeg", ".png" };
                        cooperation.Logo = await _fileProcessor.UploadFileAsync(model.Logo, folder, extensions);
                    }
                    try
                    {
                        _unitOfWork.Cooperations.Update(cooperation);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }


        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Cooperations.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var cooperation = _unitOfWork.Cooperations.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (cooperation != null)
                {
                    cooperation.Status = Status.Active;
                    try
                    {
                        _unitOfWork.Cooperations.Update(cooperation);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var cooperation = _unitOfWork.Cooperations.Get(x => x.Id == id && x.Status == Status.Active);
                if (cooperation != null)
                {
                    cooperation.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.Cooperations.Update(cooperation);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}