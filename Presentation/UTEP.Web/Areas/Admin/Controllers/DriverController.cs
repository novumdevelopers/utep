﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.DriverViewModels;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using UTEP.Web.Areas.Admin.Models.UserViewModels;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class DriverController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly UnitOfWork _unitOfWork;
        private readonly ILogger _logger;

        public DriverController(
            DbContextOptions<UTEPDbContext> contextOptions,
            UserManager<User> userManager,
            ILoggerFactory loggerFactory
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _userManager = userManager;
            _logger = loggerFactory.CreateLogger<UserController>();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Drivers.FindDataTable(x => x.Status == Status.Active && x.User.TypeOfUser == TypeOfUsers.DriverConfirmed, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateDriverViewModel
            {
                Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == UTEP.Settings.TypeOfCompany.Carrier).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                }),
                Trailers = _unitOfWork.Trailers.Find(y => y.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.RegistrationNumber.ToString()
                }),
                Trucks = _unitOfWork.Trucks.Find(z => z.Status == Status.Active).Select(z => new SelectListItem
                {
                    Value = z.Id.ToString(),
                    Text = z.RegistrationNumber
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateDriverViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.CreateUsers.Email,
                    Email = model.CreateUsers.Email,
                    FirstName = model.CreateUsers.FirstName,
                    LastName = model.CreateUsers.LastName,
                    PhoneNumber = model.CreateUsers.PhoneNumber,
                    Driver = new Driver
                    {
                        CompanyId = model.CompanyId,
                    }
                };
                user.Driver.DriverTrailers.Add(new DriverTrailer
                {
                    TrailerId = model.TrailerId
                });
                user.Driver.DriverTrucks.Add(new DriverTruck
                {
                    TruckId = model.TruckId
                });
                try
                {
                    var result = await _userManager.CreateAsync(user, model.CreateUsers.Password);

                    if (result.Succeeded)
                    {
                        _logger.LogInformation(3, "User created a new account with password.");
                    }
                    AddErrors(result);

                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            model.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            model.Trailers = _unitOfWork.Trailers.Find(y => y.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Tonnage.ToString()
            });
            model.Trucks = _unitOfWork.Trucks.Find(z => z.Status == Status.Active).Select(z => new SelectListItem
            {
                Value = z.Id.ToString(),
                Text = z.RegistrationNumber
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var driver = _unitOfWork.Drivers.GetWithUser(x => x.Id == id && x.Status == Status.Active);
            if (driver != null)
            {
                var model = new EditDriverViewModel
                {
                    Id = driver.Id,
                    EditUser = new EditUserViewModel
                    {
                        Id = driver.Id,
                        FirstName = driver.User.FirstName,
                        LastName = driver.User.LastName,
                        Email = driver.User.Email,
                        PhoneNumber = driver.User.PhoneNumber,
                        UserGroupId = driver.User.UserGroupId
                    },
                    CompanyId = driver.CompanyId,
                    Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == UTEP.Settings.TypeOfCompany.Carrier).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    }),
                    TruckId = driver.DriverTrucks.FirstOrDefault(x => x.Status == Status.Active).TruckId,
                    Trucks = _unitOfWork.Trucks.Find(z => z.Status == Status.Active).Select(z => new SelectListItem
                    {
                        Value = z.Id.ToString(),
                        Text = z.RegistrationNumber
                    }),
                    TrailerId = driver.DriverTrailers.FirstOrDefault(x => x.Status == Status.Active).TrailerId,
                    Trailers = _unitOfWork.Trailers.Find(y => y.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.RegistrationNumber
                    })
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditDriverViewModel model)
        {
            if (ModelState.IsValid)
            {
                var driver = _unitOfWork.Drivers.GetWithUser(x => x.Id == model.Id && x.Status == Status.Active);
                if (driver != null)
                {
                    driver.CompanyId = model.CompanyId;
                    driver.User = new User
                    {
                        FirstName = model.EditUser.FirstName,
                        LastName = model.EditUser.LastName,
                        Email = model.EditUser.Email,
                        PhoneNumber = model.EditUser.PhoneNumber,
                    };
                    driver.DriverTrucks.FirstOrDefault(x => x.Status == Status.Active).TruckId = model.TruckId;
                    driver.DriverTrailers.FirstOrDefault(x => x.Status == Status.Active).TrailerId = model.TrailerId;
                    try
                    {
                        _userManager.UpdateAsync(driver.User);
                        _unitOfWork.Drivers.Update(driver);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return View(model);

        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Drivers.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var driver = _unitOfWork.Drivers.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (driver != null)
                {
                    driver.Status = Status.Active;
                    try
                    {
                        _unitOfWork.Drivers.Update(driver);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var driver = _unitOfWork.Drivers.Get(x => x.Id == id && x.Status == Status.Active);
                if (driver != null)
                {
                    driver.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.Drivers.Update(driver);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }


        [HttpGet]
        public IActionResult Waited()
        {
            return View();
        }


        [HttpPost]
        [AjaxOnly]
        public IActionResult WaitedData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Users.FindDataTable(x => x.TypeOfUser == TypeOfUsers.DriverNotConfirmed, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }


        [HttpGet]
        public async Task<IActionResult> Add(long id)
        {
            var driver = await _userManager.FindByIdAsync(id.ToString());
            var model = new AddDriverViewmodel
            {
                Id = driver.Id,
                FirstName = driver.FirstName,
                LastName = driver.LastName,
                PhoneNumber = driver.PhoneNumber,
                Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Carrier).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                }),
                Trucks = _unitOfWork.Trucks.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.RegistrationNumber
                }),
                Trailers = _unitOfWork.Trailers.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.RegistrationNumber
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(AddDriverViewmodel model)
        {
            var user = await _userManager.FindByIdAsync(model.Id.ToString());
            if (ModelState.IsValid && user != null)
            {
                var driver = new Driver
                {
                    CompanyId = model.CompanyId,
                    UserId = model.Id
                };
                driver.DriverTrucks.Add(new DriverTruck
                {
                    TruckId = model.TruckId
                });
                driver.DriverTrailers.Add(new DriverTrailer
                {
                    TrailerId = model.TrailerId
                });
                user.TypeOfUser = TypeOfUsers.DriverConfirmed;
                try
                {
                    await _userManager.UpdateAsync(user);
                    _unitOfWork.Drivers.Add(driver);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(DriverController.Index), "Driver", new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }

            }

            model.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Carrier).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });
            model.Trucks = _unitOfWork.Trucks.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.RegistrationNumber
            });
            model.Trailers = _unitOfWork.Trailers.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.RegistrationNumber
            });

            return View(model);
        }

    }
}

