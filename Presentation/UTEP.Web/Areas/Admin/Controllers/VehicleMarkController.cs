﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Web.Areas.Admin.Models.VehicleMarkViewModels;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Settings;
using UTEP.Web.Attribute;
using UTEP.Web.Services;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class VehicleMarkController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IFileProcessor _fileProcessor;
        public VehicleMarkController(DbContextOptions<UTEPDbContext> contextOptions,
            IFileProcessor fileProcessor)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileProcessor = fileProcessor;
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.VehicleMarks.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);

            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateVehicleMarkViewModel model)
        {
            if (ModelState.IsValid)
            {
                var vehicleMark = new VehicleMark
                {
                    Name = model.Name
                };
                if (!String.IsNullOrEmpty(model.Logo?.Name) && model.Logo.Length > 0)
                {
                    string folder = $"vehicle-mark\\";
                    var ae = new string[] { ".jpg", ".png", ".jpeg" };
                    vehicleMark.BrandLogo = await _fileProcessor.UploadFileAsync(model.Logo, folder, ae);
                }
                try
                {
                    _unitOfWork.VehicleMarks.Add(vehicleMark);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }

            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var vehicleMark = _unitOfWork.VehicleMarks.Get(x => x.Id == id && x.Status == Status.Active);
            if (vehicleMark != null)
            {
                var model = new EditVehicleMarkViewModel
                {
                    Id = vehicleMark.Id,
                    Name = vehicleMark.Name,
                    LogoPath = vehicleMark.BrandLogo
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditVehicleMarkViewModel model)
        {
            if (ModelState.IsValid)
            {
                var vehicleMark = _unitOfWork.VehicleMarks.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (vehicleMark != null)
                {
                    vehicleMark.Name = model.Name;

                    if (model.Logo != null && !String.IsNullOrEmpty(model.Logo?.Name) && model.Logo.Length > 0)
                    {
                        string folder = $"vehicle-mark\\";
                        var ae = new string[] { ".jpg", ".png", ".jpeg" };
                        vehicleMark.BrandLogo = await _fileProcessor.UploadFileAsync(model.Logo, folder, ae);
                    }

                    try
                    {
                        _unitOfWork.VehicleMarks.Update(vehicleMark);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();

                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.VehicleMarks.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var vehicleMark = _unitOfWork.VehicleMarks.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (vehicleMark != null)
                {
                    vehicleMark.Status = Status.Active;
                    try
                    {
                        _unitOfWork.VehicleMarks.Update(vehicleMark);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var vehicleMark = _unitOfWork.VehicleMarks.Get(x => x.Id == id && x.Status == Status.Active);
                if (vehicleMark != null)
                {
                    vehicleMark.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.VehicleMarks.Update(vehicleMark);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}
