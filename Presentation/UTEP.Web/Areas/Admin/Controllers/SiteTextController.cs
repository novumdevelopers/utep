﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.SiteBasedGroup;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;
using UTEP.Web.Areas.Admin.Models.SiteTextViewModels;
using UTEP.Web.Services;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    //[Route("[area]/{lang:lang}/[controller]/[action]")]
    public class SiteTextController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IFileProcessor _fileProcessor;

        public SiteTextController(
            DbContextOptions<UTEPDbContext> contextOptions,
            IFileProcessor fileProcessor)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileProcessor = fileProcessor;
        }

        [HttpGet]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public IActionResult Main(string language)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active);
            if (!siteLanguages.Any(x => x.LanguageCode.ToLower() == language))
            {
                language = siteLanguages.First(x => x.Status == Status.Active).LanguageCode.ToLower();
            }

            var main = _unitOfWork.SiteTexts.Get(x => x.Type == SiteTextType.Main && x.Status == Status.Active);
            var mainTranslation =  main.SiteTextTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == language && x.Status == Status.Active);
            if (main != null)
            {
                var model = new EditSiteTextViewModel
                {
                    Id = main.Id,
                    Title1 = mainTranslation != null ? mainTranslation.Title1 : "",
                    Title2 = mainTranslation != null ? mainTranslation.Title2 : "",
                    ImageForView1 = mainTranslation != null ? mainTranslation.ImagePath1 : null,
                    LanguageCode = language,
                    SiteLanguages = siteLanguages.Select(y => new SiteLanguageViewModel
                    {
                        Name = y.Name,
                        LanguageCode = y.LanguageCode.ToLower(),
                        OrderIdex = y.OrderIndex
                    })
                };
                return View(model);
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public async Task<IActionResult> Main(EditSiteTextViewModel model, IFormFile file)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode && x.Status == Status.Active);
            if (siteLanguages == null)
            {
                return RedirectToAction("Error", "Home", new { area = "" });
            }
            if (ModelState.IsValid)
            {
                var main = _unitOfWork.SiteTexts.Get(x => x.Type == SiteTextType.Main && x.Status == Status.Active);
                if (main != null)
                {
                    var mainTranslation = main.SiteTextTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == model.LanguageCode && x.Status == Status.Active);
                    if (mainTranslation != null)
                    {
                        mainTranslation.Body1 = model.Body1;
                        mainTranslation.Title1 = model.Title1;
                        mainTranslation.Title2 = model.Title2;
                        if (file != null)
                        {
                            if (!string.IsNullOrEmpty(file?.Name) && file.Length > 0 && file != null)
                            {
                                var folder = $"\\site\\main\\";
                                var waterMark = "Main";
                                var extensions = new string[] { ".jpg", "jpeg", "png" };
                                var fileName = await _fileProcessor.UploadFileAsync(file, folder);
                                var path = _fileProcessor.GetPathAndFileName(fileName, folder);
                                mainTranslation.ImagePath1 = _fileProcessor.CreateThumbnailCentered(1600, 626, path, folder, waterMark);
                            }
                        }
                    }
                    else
                    {
                        var language = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode && x.Status == Status.Active);
                        if (language != null)
                        {
                            var newMain = new SiteTextTranslation
                            {
                                Body1 = model.Body1,
                                Title1 = model.Title1,
                                Title2 = model.Title2,
                                SiteLanguageId = language.Id
                            };
                            if (file != null)
                            {
                                if (!string.IsNullOrEmpty(file?.Name) && file.Length > 0 && file != null)
                                {
                                    var folder = $"\\site\\main\\";
                                    var waterMark = "Main";
                                    var extensions = new string[] { ".jpg", "jpeg", "png" };
                                    var fileName = await _fileProcessor.UploadFileAsync(file, folder);
                                    var path = _fileProcessor.GetPathAndFileName(fileName, folder);
                                    newMain.ImagePath1 = _fileProcessor.CreateThumbnailCentered(1600, 626, path, folder, waterMark);
                                }
                            }
                            main.SiteTextTranslations.Add(newMain);
                        }
                        else
                        {
                            ModelState.AddModelError("", "Belə bir dil yoxdur.");
                            model.SiteLanguages = _unitOfWork.SiteLanguages.Find(z => z.Status == Status.Active).Select(c => new SiteLanguageViewModel
                            {
                                LanguageCode = c.LanguageCode,
                                Name = c.Name,
                                OrderIdex = c.OrderIndex
                            });
                            return View(model);
                        }
                    }


                    try
                    {
                        _unitOfWork.SiteTexts.Update(main);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(HomeController.Index), "Home", new { area = "", id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                };
                model.SiteLanguages = _unitOfWork.SiteLanguages.Find(z => z.Status == Status.Active).Select(c => new SiteLanguageViewModel
                {
                    LanguageCode = c.LanguageCode,
                    Name = c.Name,
                    OrderIdex = c.OrderIndex
                });
                return View(model);
            }
            return RedirectToAction("Error", "Home", new { area = "", id = "" });
        }

        [HttpGet]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public IActionResult About(string language)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active);
            if (!siteLanguages.Any(x => x.LanguageCode.ToLower() == language && x.Status == Status.Active))
            {
                language = siteLanguages.First(x => x.Status == Status.Active).LanguageCode.ToLower();
            }

            var about = _unitOfWork.SiteTexts.Get(x => x.Status == Status.Active && x.Type == SiteTextType.About);
            var aboutTranslation = about.SiteTextTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == language && x.Status == Status.Active);
            if (about != null)
            {
                var model = new EditSiteTextViewModel
                {
                    Id = about.Id,
                    Title1 = aboutTranslation != null ? aboutTranslation.Title1 : "",
                    Body1 = aboutTranslation != null ? aboutTranslation.Body1 : "",
                    Body2 = aboutTranslation != null ? aboutTranslation.Body2 : "",
                    Title2 = aboutTranslation != null ? aboutTranslation.Title2 : "",
                    Title3 = aboutTranslation != null ? aboutTranslation.Title3 : "",
                    Body3 = aboutTranslation != null ? aboutTranslation.Body3 : "",
                    ImageForView1 = aboutTranslation != null ? aboutTranslation.ImagePath1 : "",
                    ImageForView2 = aboutTranslation != null ? aboutTranslation.ImagePath2 : "",
                    ImageForView3 = aboutTranslation != null ? aboutTranslation.ImagePath3 : "",
                    LanguageCode = language,
                    SiteLanguages = siteLanguages.Select(y => new SiteLanguageViewModel
                    {
                        Name = y.Name,
                        LanguageCode = y.LanguageCode.ToLower(),
                        OrderIdex = y.OrderIndex
                    })
                };
                return View(model);
            }
            return RedirectToAction("Error", "Home", new { area = "", id = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public async Task<IActionResult> About(EditSiteTextViewModel model, IFormFile file1, IFormFile file2, IFormFile file3)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode && x.Status == Status.Active);
            if (siteLanguages == null)
            {
                return RedirectToAction("Error", "Home", new { area = "" });
            }

            if (ModelState.IsValid)
            {
                var about = _unitOfWork.SiteTexts.Get(x => x.Id == model.Id && x.Status == Status.Active && x.Type == SiteTextType.About);
                if (about != null)
                {
                    var aboutTranslation = about.SiteTextTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == model.LanguageCode && x.Status == Status.Active);
                    if (aboutTranslation != null)
                    {
                        aboutTranslation.Title1 = model.Title1;
                        aboutTranslation.Body1 = model.Body1;
                        aboutTranslation.Body2 = model.Body2;
                        aboutTranslation.Title2 = model.Title2;
                        aboutTranslation.Title3 = model.Title3;
                        aboutTranslation.Body3 = model.Body3;

                        var folder = $"site\\about\\";
                        if (file1 != null && !string.IsNullOrEmpty(file1?.Name) && file1.Length > 0)
                        {
                            var waterMark1 = "About1";
                            var fileName1 = await _fileProcessor.UploadFileAsync(file1, folder);
                            var fileDirectory1 = _fileProcessor.GetPathAndFileName(fileName1, folder);
                            aboutTranslation.ImagePath1 = _fileProcessor.CreateThumbnailCentered(1904, 355, fileDirectory1, folder, waterMark1);
                        }
                        if (file2 != null && !string.IsNullOrEmpty(file2?.Name) && file2.Length > 0)
                        {
                            var waterMark2 = "About2";
                            var fileName2 = await _fileProcessor.UploadFileAsync(file2, folder);
                            var fileDirectory2 = _fileProcessor.GetPathAndFileName(fileName2, folder);
                            aboutTranslation.ImagePath2 = _fileProcessor.CreateThumbnailCentered(1630, 540, fileDirectory2, folder, waterMark2);
                        }
                        if (file3 != null && !string.IsNullOrEmpty(file3?.Name) && file3.Length > 0)
                        {
                            var waterMark3 = "About3";
                            var fileName3 = await _fileProcessor.UploadFileAsync(file3, folder);
                            var fileDirectory3 = _fileProcessor.GetPathAndFileName(fileName3, folder);
                            aboutTranslation.ImagePath3 = _fileProcessor.CreateThumbnailCentered(1904, 550, fileDirectory3, folder, waterMark3);
                        }
                    }
                    else
                    {
                        var language = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode && x.Status == Status.Active);
                        if (language != null)
                        {
                            var newAbout = new SiteTextTranslation
                            {
                                Title1 = model.Title1,
                                Body1 = model.Body1,
                                Body2 = model.Body2,
                                Title2 = model.Title2,
                                Title3 = model.Title3,
                                Body3 = model.Body3,
                                SiteLanguageId = language.Id
                            };
                            var folder = $"site\\about\\";
                            if (file1 != null && !string.IsNullOrEmpty(file1?.Name) && file1.Length > 0)
                            {
                                var waterMark1 = "About1";
                                var fileName1 = await _fileProcessor.UploadFileAsync(file1, folder);
                                var fileDirectory1 = _fileProcessor.GetPathAndFileName(fileName1, folder);
                                newAbout.ImagePath1 = _fileProcessor.CreateThumbnailCentered(1904, 355, fileDirectory1, folder, waterMark1);
                            }

                            if (file2 != null && !string.IsNullOrEmpty(file2?.Name) && file2.Length > 0)
                            {
                                var waterMark2 = "About2";
                                var fileName2 = await _fileProcessor.UploadFileAsync(file2, folder);
                                var fileDirectory2 = _fileProcessor.GetPathAndFileName(fileName2, folder);
                                newAbout.ImagePath2 = _fileProcessor.CreateThumbnailCentered(1630, 540, fileDirectory2, folder, waterMark2);
                            }

                            if (file3 != null && !string.IsNullOrEmpty(file3?.Name) && file3.Length > 0)
                            {
                                var waterMark3 = "About3";
                                var fileName3 = await _fileProcessor.UploadFileAsync(file3, folder);
                                var fileDirectory3 = _fileProcessor.GetPathAndFileName(fileName3, folder);
                                newAbout.ImagePath3 = _fileProcessor.CreateThumbnailCentered(1904, 550, fileDirectory3, folder, waterMark3);
                            }
                            about.SiteTextTranslations.Add(newAbout);
                        }
                        else
                        {
                            ModelState.AddModelError("", "Belə bir dil yoxdur.");
                            model.SiteLanguages = _unitOfWork.SiteLanguages.Find(z => z.Status == Status.Active).Select(c => new SiteLanguageViewModel
                            {
                                LanguageCode = c.LanguageCode,
                                Name = c.Name,
                                OrderIdex = c.OrderIndex
                            });
                            return View(model);
                        }
                    }



                    try
                    {
                        _unitOfWork.SiteTexts.Update(about);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction("Index", "Home", new { area = "Admin" });
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                model.SiteLanguages = _unitOfWork.SiteLanguages.Find(z => z.Status == Status.Active).Select(c => new SiteLanguageViewModel
                {
                    LanguageCode = c.LanguageCode,
                    Name = c.Name,
                    OrderIdex = c.OrderIndex
                });
                return View(model);
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }
    }
}