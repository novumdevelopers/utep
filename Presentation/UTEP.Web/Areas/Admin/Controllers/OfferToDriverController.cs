﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.OfferToDriverViewModels;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using UTEP.Data.Domain.Models.Tables.OfferBasedGroup;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class OfferToDriverController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public OfferToDriverController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.OfferToDrivers.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateOfferToDriverViewModel
            {
                CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Products.First(z => z.Status == Status.Active).Name
                }),
                Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.User.FirstName
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateOfferToDriverViewModel model)
        {
            if (ModelState.IsValid)
            {
                var product = new OfferToDriver
                {
                    CompanyOrderId = model.CompanyOrderId,
                    DriverId=model.DriverId
                };
                _unitOfWork.OfferToDrivers.Add(product);
                _unitOfWork.Complete();
                return RedirectToAction("Index");

            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {

            var offerToDriver = _unitOfWork.OfferToDrivers.Get(x => x.Id == id && x.Status == Status.Active);
            if (offerToDriver != null)
            {
                var model = new EditOfferToDriverViewModel
                {
                    Id = offerToDriver.Id,
                    CompanyOrderId = offerToDriver.CompanyOrderId,
                    CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Products.First(z => z.Status == Status.Active).Name
                    }),
                    DriverId=offerToDriver.DriverId,
                    Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.User.FirstName
                    })
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditOfferToDriverViewModel model)
        {
            if (ModelState.IsValid)
            {
                var offerToDriver = _unitOfWork.OfferToDrivers.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (offerToDriver != null)
                {
                    offerToDriver.DriverId = model.DriverId;
                    offerToDriver.CompanyOrderId = model.CompanyOrderId;
                    try
                    {
                        _unitOfWork.OfferToDrivers.Update(offerToDriver);
                        _unitOfWork.Complete();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("Error", "Home", new { area = " " });
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.OfferToDrivers.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var offerToDriver = _unitOfWork.OfferToDrivers.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (offerToDriver != null)
                {
                    offerToDriver.Status = Status.Active;
                    try
                    {
                        _unitOfWork.OfferToDrivers.Update(offerToDriver);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var offerToDriver = _unitOfWork.OfferToDrivers.Get(x => x.Id == id && x.Status == Status.Active);
                if (offerToDriver != null)
                {
                    offerToDriver.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.OfferToDrivers.Update(offerToDriver);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}

