﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.ProductAdminViewModels;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class ProductAdminController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public ProductAdminController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Products.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateProductAdminViewModel
            {
                Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.Name
                }),
                ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.ProductTypeTranslations.FirstOrDefault(z => z.SiteLanguage.LanguageCode == "az").Name
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateProductAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var product = new Product
                {
                    Name = model.Name,
                    Tonnage = model.Tonnage,
                    Size = model.Size,
                    PaletteCount = model.PaletteCount,
                    CompanyId = model.CompanyId,
                    ProductTypeId = model.ProducTypeId
                };
                _unitOfWork.Products.Add(product);
                _unitOfWork.Complete();
                _unitOfWork.Dispose();
                return RedirectToAction("Index");

            }
            model.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.User.ToString()
            });
            model.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.ProductTypeTranslations.FirstOrDefault(z => z.SiteLanguage.LanguageCode == "az").Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {

            var product = _unitOfWork.Products.Get(x => x.Id == id && x.Status == Status.Active);
            if (product != null)
            {
                var model = new EditProductAdminViewModel
                {
                    Id = product.Id,
                    Name = product.Name,
                    Tonnage = product.Tonnage,
                    Size = product.Size,
                    PaletteCount = product.PaletteCount,
                    CompanyId = product.CompanyId,
                    Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.User.ToString()
                    }),
                    ProducTypeId = product.ProductTypeId,
                    ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.ProductTypeTranslations.FirstOrDefault(z => z.SiteLanguage.LanguageCode == "az").Name
                    })
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditProductAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                var product = _unitOfWork.Products.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (product != null)
                {
                    product.Name = model.Name;
                    product.Size = model.Size;
                    product.Tonnage = model.Tonnage;
                    product.PaletteCount = model.PaletteCount;
                    product.CompanyId = model.CompanyId;
                    product.ProductTypeId = model.ProducTypeId;
                    try
                    {
                        _unitOfWork.Products.Update(product);
                        _unitOfWork.Complete();
                        return RedirectToAction("Index");
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("Error", "Home", new { area = " " });
                    }
                }
            }
            model.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.User.ToString()
            });
            model.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.ProductTypeTranslations.FirstOrDefault(z => z.SiteLanguage.LanguageCode == "az").Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Products.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var productadmin = _unitOfWork.Products.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (productadmin != null)
                {
                    productadmin.Status = Status.Active;
                    try
                    {
                        _unitOfWork.Products.Update(productadmin);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var productadmin = _unitOfWork.Products.Get(x => x.Id == id && x.Status == Status.Active);
                if (productadmin != null)
                {
                    productadmin.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.Products.Update(productadmin);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}

