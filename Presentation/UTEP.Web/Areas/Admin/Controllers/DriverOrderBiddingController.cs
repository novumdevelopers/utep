﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.DriverOrderBiddingViewModels;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class DriverOrderBiddingController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public DriverOrderBiddingController(
            DbContextOptions<UTEPDbContext> contextOptions
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.DriverOrderBiddings.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateDriverOrderBiddingViewModel
            {
                Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.User.FirstName + " " + y.User.LastName
                }),
                OfferToDrivers = _unitOfWork.OfferToDrivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.CompanyOrder.Products.First(z => z.Status == Status.Active).Name
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateDriverOrderBiddingViewModel model)
        {
            if (ModelState.IsValid)
            {
                var driverOrderBidding = new DriverOrderBidding
                {
                    DriverId = model.DriverId,
                    OfferToDriverId = model.OfferToDriverId,
                    SuggestedPrice = model.SuggestedPrice
                };
                try
                {
                    _unitOfWork.DriverOrderBiddings.Add(driverOrderBidding);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }

            model.Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.User.FirstName + " " + y.User.LastName
            });
            model.OfferToDrivers = _unitOfWork.OfferToDrivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.CompanyOrder.Products.First(z => z.Status == Status.Active).Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var driverOrderBidding = _unitOfWork.DriverOrderBiddings.Get(x => x.Id == id && x.Status == Status.Active);
            if (driverOrderBidding != null)
            {
                var model = new EditDriverOrderBiddingViewModel
                {
                    DriverId = driverOrderBidding.DriverId,
                    Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.User.FirstName + " " + y.User.LastName
                    }),
                    OfferToDriverId = driverOrderBidding.OfferToDriverId,
                    OfferToDrivers = _unitOfWork.OfferToDrivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.CompanyOrder.Products.First(z => z.Status == Status.Active).Name
                    }),
                    SuggestedPrice = driverOrderBidding.SuggestedPrice
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditDriverOrderBiddingViewModel model)
        {
            if (ModelState.IsValid)
            {
                var driverOrderBidding = _unitOfWork.DriverOrderBiddings.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (driverOrderBidding != null)
                {
                    driverOrderBidding.DriverId = model.DriverId;
                    driverOrderBidding.OfferToDriverId = model.OfferToDriverId;
                    driverOrderBidding.SuggestedPrice = model.SuggestedPrice;
                    try
                    {
                        _unitOfWork.DriverOrderBiddings.Update(driverOrderBidding);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            model.Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.User.FirstName + " " + y.User.LastName
            });
            model.OfferToDrivers = _unitOfWork.OfferToDrivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.CompanyOrder.Products.First(z=>z.Status==Status.Active).Name
            });

            return View(model);

        }
    }
}

