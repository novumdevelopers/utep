﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models.CompanyShippingViewModels;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class CompanyShippingController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public CompanyShippingController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyShippings.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }


        [HttpGet]
        public ActionResult Create()
        {
            var model = new CreateCompanyShippingViewModel
            {
                CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.ProporsalPrice.ToString()
                }),
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(CreateCompanyShippingViewModel model)
        {
            if (ModelState.IsValid)
            {
                var companyShipping = new CompanyShipping
                {
                    CompanyOrderId = model.CompanyOrderId,
                    DestinationTownLatitude = model.DestinationTownLatitude,
                    DestinationTownLongitude = model.DestinationTownLongitude,
                    OriginTownLatitude = model.OriginTownLatitude,
                    OriginTownLongitude = model.OriginTownLongitude,
                    Deadline = model.Deadline,
                };
                try
                {
                    _unitOfWork.CompanyShippings.Add(companyShipping);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();

                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {

                    throw;
                }
            }
            model.CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.ProporsalPrice.ToString()
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var companyShipping = _unitOfWork.CompanyShippings.Get(x => x.Id == id && x.Status == Status.Active);
            if (companyShipping != null)
            {
                var model = new EditCompanyShippingViewModel
                {
                    Id = companyShipping.Id,
                    CompanyOrderId = companyShipping.CompanyOrderId,
                    DestinationTownLatitude = companyShipping.DestinationTownLatitude,
                    DestinationTownLongitude = companyShipping.DestinationTownLongitude,
                    OriginTownLatitude = companyShipping.OriginTownLatitude,
                    OriginTownLongitude = companyShipping.OriginTownLongitude,
                    Deadline = companyShipping.Deadline,
                    CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProporsalPrice.ToString()
                    })
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditCompanyShippingViewModel model)
        {
            var companyShipping = _unitOfWork.CompanyShippings.Get(x => x.Id == model.Id && x.Status == Status.Active);
            if (companyShipping != null)
            {
                companyShipping.CompanyOrderId = model.CompanyOrderId;
                companyShipping.DestinationTownLatitude = model.DestinationTownLatitude;
                companyShipping.DestinationTownLongitude = model.DestinationTownLongitude;
                companyShipping.OriginTownLatitude = model.OriginTownLatitude;
                companyShipping.DestinationTownLongitude = model.DestinationTownLongitude;
                companyShipping.Deadline = model.Deadline;
                try
                {
                    _unitOfWork.CompanyShippings.Update(companyShipping);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception e)
                {
                    throw;
                }
            }
            model.CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Products.First(z => z.Status == Status.Active).Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyShippings.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var companyShipping = _unitOfWork.CompanyShippings.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (companyShipping != null)
                {
                    companyShipping.Status = Status.Active;
                    try
                    {
                        _unitOfWork.CompanyShippings.Update(companyShipping);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var companyShipping = _unitOfWork.CompanyShippings.Get(x => x.Id == id && x.Status == Status.Active);
                if (companyShipping != null)
                {
                    companyShipping.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.CompanyShippings.Update(companyShipping);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }


        







    }
}