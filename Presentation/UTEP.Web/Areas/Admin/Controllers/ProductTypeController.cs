﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using Microsoft.EntityFrameworkCore;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Web.Attribute;
using UTEP.Web.Areas.Admin.Models.ProductTypeViewModels;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using UTEP.Web.Services;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class ProductTypeController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IFileProcessor _fileProcessor;

        public ProductTypeController(
            UserManager<User> userManager,
            IFileProcessor fileProcessor,
            DbContextOptions<UTEPDbContext> contextOptions
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileProcessor = fileProcessor;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.ProductTypes.FindDataTable(x => x.Status == Status.Active && x.ProductTypeTranslations.Any(y => y.SiteLanguage.LanguageCode == lang), lang, skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public IActionResult Create(string language)
        {
            //var lang = string.IsNullOrEmpty(RouteData.Values["lang"].ToString()) ? "az" : RouteData.Values["lang"].ToString();

            var siteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active);
            if (!siteLanguages.Any(x => x.LanguageCode.ToLower() == language))
            {
                language = siteLanguages.First(x => x.Status == Status.Active).LanguageCode.ToLower();
            }
            var model = new CreateProductTypeViewModel
            {
                LanguageCode = language,
                SiteLanguages = siteLanguages.Select(y => new SiteLanguageViewModel
                {
                    Name = y.Name,
                    LanguageCode = y.LanguageCode.ToLower(),
                    OrderIdex = y.OrderIndex
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public async Task<IActionResult> Create(CreateProductTypeViewModel model, IFormFile file)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode);
            if (siteLanguages == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var productType = new ProductType();
                productType.ProductTypeTranslations.Add(new ProductTypeTranslation
                {
                    Name = model.Name,
                    SiteLanguage = siteLanguages
                });
                if (!string.IsNullOrEmpty(file?.Name) && file.Length > 0)
                {
                    var folder = $"icons\\";
                    productType.ClassName = await _fileProcessor.UploadFileAsync(file, folder);
                }

                try
                {
                    _unitOfWork.ProductTypes.Add(productType);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return View(model);
        }

        [HttpGet]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{id}/{language?}")]
        public IActionResult Edit(long id, string language)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active);
            if (!siteLanguages.Any(x => x.LanguageCode.ToLower() == language))
            {
                language = RouteData.Values["lang"].ToString() ?? "az"; //siteLanguages.First(x => x.Status == Status.Active).LanguageCode.ToLower();
            }

            //adlandirmani yazanda axrina Db yazma (productType => productType) 
            var productType = _unitOfWork.ProductTypes.Get(x => x.Id == id && x.Status == Status.Active);
            var productTypeLanguage = productType.ProductTypeTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == language);
            if (productType != null)
            {
                var model = new EditProductTypeViewModel
                {
                    Id = productType.Id,
                    Name = productTypeLanguage != null ? productTypeLanguage.Name : "",
                    LanguageCode = language,
                    ImageForView = productType.ClassName,
                    SiteLanguages = siteLanguages.Select(x => new SiteLanguageViewModel
                    {
                        LanguageCode = x.LanguageCode,
                        Name = x.Name,
                        OrderIdex = x.OrderIndex
                    })
                };

                //Include elemek yaddan cixmiwdi elemiwem men obrilere de bax
                //var productTypeTranslation = productType.ProductTypeTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == language);
                //if (productTypeTranslation != null)
                //{
                //    model.Name = productTypeTranslation.Name;
                //}

                return View(model);
            }
            return RedirectToAction("Error", "Home", new { area = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{id}/{language?}")]
        public async Task<IActionResult> Edit(EditProductTypeViewModel model, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                var productType = _unitOfWork.ProductTypes.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (productType == null)
                {
                    return RedirectToAction("Error", "Home", new { area = "" });
                }

                var productTranslation = productType.ProductTypeTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == model.LanguageCode && x.Status == Status.Active);
                if (productTranslation != null)
                {
                    productTranslation.Name = model.Name;
                    if (!string.IsNullOrEmpty(file?.Name) && file.Length > 0)
                    {
                        var folder = $"icons\\";
                        productType.ClassName = await _fileProcessor.UploadFileAsync(file, folder);
                    }
                }
                else
                {
                    var languageDb = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode && x.Status == Status.Active);
                    if (languageDb != null)
                    {
                        productType.ProductTypeTranslations.Add(new ProductTypeTranslation
                        {
                            Name = model.Name,
                            SiteLanguageId = languageDb.Id
                        });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Belə bir dil yoxdur.");
                        model.SiteLanguages = _unitOfWork.SiteLanguages.Find(z => z.Status == Status.Active).Select(c => new SiteLanguageViewModel
                        {
                            LanguageCode = c.LanguageCode,
                            Name = c.Name,
                            OrderIdex = c.OrderIndex
                        });
                        return View(model);
                    }
                }

                try
                {
                    _unitOfWork.ProductTypes.Update(productType);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            model.SiteLanguages = _unitOfWork.SiteLanguages.Find(z => z.Status == Status.Active).Select(c => new SiteLanguageViewModel
            {
                LanguageCode = c.LanguageCode,
                Name = c.Name,
                OrderIdex = c.OrderIndex
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.ProductTypes.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var productType = _unitOfWork.ProductTypes.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (productType != null)
                {
                    productType.Status = Status.Active;
                    try
                    {
                        _unitOfWork.ProductTypes.Update(productType);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var productType = _unitOfWork.ProductTypes.Get(x => x.Id == id && x.Status == Status.Active);
                if (productType != null)
                {
                    productType.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.ProductTypes.Update(productType);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}