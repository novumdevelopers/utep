﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.CompanyOrderAdminViewModels;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Web.Areas.CompanyProfile.Models.ProductViewModels;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using Microsoft.AspNetCore.Identity;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using Microsoft.AspNetCore.Authorization;
using UTEP.Web.Services;
using Microsoft.AspNetCore.Http;
using System.IO;
using UTEP.Web.Areas.Admin.Models.ProductAdminViewModels;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class CompanyOrderAdminController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IEmailSender _emailSender;
        private readonly IFileProcessor _fileProcessor;
        public CompanyOrderAdminController(
            DbContextOptions<UTEPDbContext> contextOptions,
            UserManager<User> userManager,
            IFileProcessor fileProcessor,
            IEmailSender emailSender
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _userManager = userManager;
            _emailSender = emailSender;
            _fileProcessor = fileProcessor;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [AjaxOnly]
        [HttpPost]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["Order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["Order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyOrders.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var model = new CreateCompanyOrderAdminViewModel
            {
                CreateProduct = new CreateProductViewModel
                {
                    ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    }),
                    Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    }),
                    PaletteCount = 1
                },
                FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
                ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
                Lang = lang
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateCompanyOrderAdminViewModel model, IFormCollection collection)
        {
            if (ModelState.IsValid)
            {
                if (collection.Files.Count > 6)
                {
                    ModelState.AddModelError("", "Şəkillərin sayı 6-dan çox olmamalıdır.");
                    model.CreateProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    });
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if (collection.Files.Count == 0)
                {
                    ModelState.AddModelError("", "Ən az 1 şəkil yükləməlisiniz.");
                    model.CreateProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    });
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if (model.DeadLine > model.DeadLine)
                {
                    ModelState.AddModelError("", "Başlama tarixi bitiş tarixdən böyük ola bilməz.");
                    model.CreateProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    });
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }

                var from = _unitOfWork.Cities.Get(x => x.Status == Status.Active && x.Id == model.FromCityId).Name;
                var to = _unitOfWork.Cities.Get(x => x.Status == Status.Active && x.Id == model.ToCityId).Name;

                var shipping = new List<CompanyShipping>{
                    new CompanyShipping
                    {
                        Deadline = model.DeadLine
                    }};

                var companyOrder = new CompanyOrder
                {
                    DealDate = model.DealDate,
                    ProporsalPrice = model.ProporsalPrice,
                    OriginTownLatitude = 0m,
                    OriginTownLongitude = 0m,
                    DestinationTownLatitude = 0m,
                    DestinationTownLongitude = 0m,
                    From = from,
                    FromDescription = model.FromDescription,
                    To = to,
                    ToDescription = model.ToDescription,
                    Status = Status.Active,
                    Currency = model.Currency,
                    CompanyShippings = shipping
                };

                var product = new Product
                {
                    CompanyId = model.CreateProduct.CompanyId,
                    ProductTypeId = model.CreateProduct.ProducTypeId,
                    Name = model.CreateProduct.Name,
                    Size = 0.00m,
                    Tonnage = model.CreateProduct.Tonnage,
                    PaletteCount = model.CreateProduct.PaletteCount,
                    Weight = model.Weight,
                    WeightType = model.CreateProduct.WeightType,
                    CompanyOrder = companyOrder,
                };

                foreach (var item in collection.Files)
                {
                    if (!string.IsNullOrEmpty(item?.Name) && item.Length > 0)
                    {
                        var folderMain = $"company\\product\\";
                        var folderMed = $"company\\product\\medium\\";
                        var fileName = await _fileProcessor.UploadFileAsync(item, folderMain);
                        var fileDirectory = _fileProcessor.GetPathAndFileName(fileName, folderMain);
                        var watermarkText = "UTEP.az";
                        var writenMain = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMain);
                        var writenMed = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMed);
                        if (writenMed && writenMain)
                        {
                            var fullFilename = watermarkText + Path.GetFileName(fileDirectory);
                            var newfileDirectory = _fileProcessor.GetPathAndFileName(fullFilename, folderMed);
                            var mediumPath = _fileProcessor.CreateThumbnailCentered(270, 142, newfileDirectory, folderMed, watermarkText);

                            product.ProductImages.Add(new ProductImage()
                            {
                                MainPath = fullFilename,
                                MediumPath = mediumPath
                            });
                        }
                    }
                }

                if (!product.ProductImages.Any())
                {
                    ModelState.AddModelError("", "Şəkildə problem var. Zəhmət olmasa, başqa şəkil yükləyin.");
                    model.CreateProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    });
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }

                try
                {
                    _unitOfWork.Products.Add(product);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }

            model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
            });
            model.CreateProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });

            model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });

            model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Id == id && x.Status == Status.Active);
            if (companyOrder != null)
            {
                var fromCity = companyOrder.From;
                var fromCityId = _unitOfWork.Cities.Get(x => x.Name == fromCity && x.Status == Status.Active).Id;
                var fromCountryId = _unitOfWork.Cities.Get(x => x.Name == fromCity && x.Status == Status.Active).CountryId;

                var toCity = companyOrder.To;
                var toCityId = _unitOfWork.Cities.Get(x => x.Name == toCity && x.Status == Status.Active).Id;
                var toCountryId = _unitOfWork.Cities.Get(x => x.Name == toCity && x.Status == Status.Active).CountryId;

                var model = new EditCompanyOrderAdminViewModel
                {
                    Id = companyOrder.Id,
                    ProporsalPrice = companyOrder.ProporsalPrice,
                    DealDate = companyOrder.DealDate,
                    FromDescription = companyOrder.FromDescription,
                    ToDescription = companyOrder.ToDescription,
                    DeadLine = companyOrder.CompanyShippings.FirstOrDefault().Deadline,
                    Currency = companyOrder.Currency,
                    EditProduct = new EditProductAdminViewModel
                    {
                        CompanyId = companyOrder.Products.First(x => x.Status == Status.Active).CompanyId,
                        Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.Name
                        }),
                        ProducTypeId = companyOrder.Products.First(x => x.Status == Status.Active).ProductTypeId,
                        ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.ProductTypeTranslations.FirstOrDefault(y => y.SiteLanguage.LanguageCode == "az").Name
                        }),
                        Name = companyOrder.Products.First(x => x.Status == Status.Active).Name,
                        //Size = companyOrder.Products.First(x => x.Status == Status.Active).Size,
                        Tonnage = companyOrder.Products.First(x => x.Status == Status.Active).Tonnage,
                        Weight = companyOrder.Products.First(x => x.Status == Status.Active).Weight,
                        WeightType = companyOrder.Products.First(x => x.Status == Status.Active).WeightType,
                        PaletteCount = companyOrder.Products.First(x => x.Status == Status.Active).PaletteCount,
                        ProductImages = companyOrder.Products.First(x => x.Status == Status.Active).ProductImages.Where(x => x.Status == Status.Active).ToList()
                    },
                    FromCountryId = fromCountryId,
                    FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),
                    FromCityId = fromCityId,
                    FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == fromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),
                    ToCountryId = toCountryId,
                    ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),
                    ToCityId = toCityId,
                    ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == toCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),
                    Lang = lang,
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditCompanyOrderAdminViewModel model, IFormCollection collection)
        {
            if (ModelState.IsValid)
            {
                if (collection.Files.Count == 0 && model.EditProduct.ProductImages.Count == 0)
                {
                    ModelState.AddModelError("", "Şəkil əlavə edin.");
                    model.EditProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    });
                    model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if ((collection.Files.Count + model.EditProduct.ProductImages.Count) > 6)
                {
                    ModelState.AddModelError("", "Şəkillərin ümumi sayı maksimum 6 olmalıdır.");

                    model.EditProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    });
                    model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if (model.DeadLine > model.DeadLine)
                {
                    ModelState.AddModelError("", "Başlama tarixi bitiş tarixdən böyük ola bilməz.");
                    model.EditProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.Name
                    });
                    model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }

                var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Id == model.Id && x.Status == Status.Active);
                var from = _unitOfWork.Cities.Get(x => x.Id == model.FromCityId && x.Status == Status.Active).Name;
                var to = _unitOfWork.Cities.Get(x => x.Id == model.ToCityId && x.Status == Status.Active).Name;

                if (companyOrder != null)
                {
                    companyOrder.CompanyShippings.First().Deadline = model.DeadLine;
                    companyOrder.Currency = model.Currency;
                    companyOrder.DealDate = model.DealDate;
                    companyOrder.ProporsalPrice = model.ProporsalPrice;
                    companyOrder.From = from;
                    companyOrder.FromDescription = model.FromDescription;
                    companyOrder.To = to;
                    companyOrder.ToDescription = model.ToDescription;
                    companyOrder.Products.First(x => x.Status == Status.Active).CompanyId = model.EditProduct.CompanyId;
                    companyOrder.Products.First(x => x.Status == Status.Active).ProductTypeId = model.EditProduct.ProducTypeId;
                    companyOrder.Products.First(x => x.Status == Status.Active).Name = model.EditProduct.Name;
                    //companyOrder.Products.First(x => x.Status == Status.Active).Size = model.EditProduct.Size;
                    companyOrder.Products.First(x => x.Status == Status.Active).Tonnage = model.EditProduct.Tonnage;
                    companyOrder.Products.First(x => x.Status == Status.Active).Weight = model.EditProduct.Weight;
                    companyOrder.Products.First(x => x.Status == Status.Active).WeightType = model.EditProduct.WeightType;
                    companyOrder.Products.First(x => x.Status == Status.Active).PaletteCount = model.EditProduct.PaletteCount;

                    foreach (var item in collection.Files)
                    {
                        if (item != null && !string.IsNullOrEmpty(item?.Name) && item.Length > 0)
                        {
                            var folderMain = $"company\\product\\";
                            var folderMed = $"company\\product\\medium\\";
                            var fileName = await _fileProcessor.UploadFileAsync(item, folderMain);
                            var fileDirectory = _fileProcessor.GetPathAndFileName(fileName, folderMain);
                            var watermarkText = "UTEP.az";
                            var writenMain = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMain);
                            var writenMed = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMed);
                            if (writenMed && writenMain)
                            {
                                var fullFilename = watermarkText + Path.GetFileName(fileDirectory);
                                var newfileDirectory = _fileProcessor.GetPathAndFileName(fullFilename, folderMed);
                                var mediumPath = _fileProcessor.CreateThumbnailCentered(270, 142, newfileDirectory, folderMed, watermarkText);

                                companyOrder.Products.First().ProductImages.Add(new ProductImage()
                                {
                                    MainPath = fullFilename,
                                    MediumPath = mediumPath
                                });
                            }
                        }
                    }

                    if (!companyOrder.Products.First().ProductImages.Any())
                    {
                        ModelState.AddModelError("", "Şəkildə problem var. Zəhmət olmasa, başqa şəkil yükləyin.");
                        model.EditProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.Name
                        });
                        model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                        });

                        model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });
                        model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });

                        model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });
                        model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });
                        return View(model);
                    }

                    try
                    {
                        _unitOfWork.CompanyOrders.Update(companyOrder);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
            });
            model.EditProduct.Companies = _unitOfWork.Companies.Find(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Name
            });

            model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });

            model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            return View(model);
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult TrashImage(long id)
        {
            var productId = _unitOfWork.ProductImages.Get(x => x.Id == id && x.Status == Status.Active).ProductId;
            //var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Company.Id == 2);
            var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Products.FirstOrDefault(y => y.Id == productId).Id == productId);
            if (companyOrder != null)
            {
                var image = companyOrder.Products.FirstOrDefault().ProductImages.FirstOrDefault(x => x.Id == id);
                if (image != null)
                {
                    image.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.ProductImages.Update(image);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Status = true, Message = "Success!" });
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
            return Json(new { Status = false, Message = "Failed!" });
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyOrders.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var companyOrder = _unitOfWork.CompanyOrders.Get(x => x.Id == id && x.Status == Status.Deleted || x.Status == Status.Waited);
                if (companyOrder != null)
                {
                    companyOrder.Status = Status.Active;
                    try
                    {
                        _unitOfWork.CompanyOrders.Update(companyOrder);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        // Dil meselesine baxilacaq
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> WaitedToActive(long id)
        {
            if (id != 0 || !string.IsNullOrEmpty(id.ToString()))
            {
                var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Id == id && x.Status == Status.Waited);
                var product = companyOrder.Products.First();
                if (companyOrder != null)
                {
                    companyOrder.Status = Status.Active;
                    try
                    {
                        var user = _userManager.Users
                                .Include(x => x.Company)
                                .ThenInclude(x => x.CompanyProducts)
                                .FirstOrDefault(x => x.Company.CompanyProducts.FirstOrDefault(y => y.Id == product.Id).Id == product.Id);

                        var callbackUrl = Url.Action(nameof(Index), "CompanyOrder", new { area = "CompanyProfile", id = "" }, protocol: HttpContext.Request.Scheme);
                        var message = $"<!DOCTYPE html><html lang='az'><head> <meta charset='UTF-8'> <title>UTEP</title></head><body style='text-align:center;'> <table> <tr> <td align='center'> <img src='http://utep.a2hosted.com/logo/ULogoNew2.png' alt='UTEP' style='width:60px; height:60px;'> </td></tr><tr> <td style='font-size: 18px'> <h3 align='center'> Sifarişiniz qəbul edildi. </h3> <p> '{product.Name}' adlı sifarişiniz artıq sayta yerləşdirildi. Sifarişinizə baxmaq üçün <a href='{callbackUrl}'>istinadla</a> keçin. </p><p> Unutmayın, siz istənilən zaman <a href='mailto:support@utep.az'>support@utep.az</a> emaili vasitəsilə bizə müraciət edə bilərsiniz. Biz UTEP.az servisi haqda sizin bütün suallarınıza cavab verməyə hazırıq. </p><br><hr> </td></tr><tr> <td align='center' style='font-size: 14px'> <p> Cəfər Cabbarlı küçəsi, 44, Bakı, Azərbaycan </p><p> <a href='tel:+994777779696'> +994 77 777 96 96</a> <br><a href='mailto:support@utep.az'>support@utep.az</a> </p></td></tr></table></body></html>";
                        await _emailSender.SendEmailAsync(user.Email, "Sifarişiniz qəbul edildi.", message);

                        _unitOfWork.CompanyOrders.Update(companyOrder);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var companyOrder = _unitOfWork.CompanyOrders.Get(x => x.Id == id && x.Status == Status.Active);
                if (companyOrder != null)
                {
                    companyOrder.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.CompanyOrders.Update(companyOrder);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }




        [HttpGet]
        public IActionResult TrashbyCompany()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult TrashDatabyCompany()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyOrders.FindDataTable(x => x.Status == Status.End, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BackbyCompany(long id)
        {
            if (ModelState.IsValid)
            {
                var companyOrder = _unitOfWork.CompanyOrders.Get(x => x.Id == id && x.Status == Status.End);
                if (companyOrder != null)
                {
                    companyOrder.Status = Status.Active;
                    try
                    {
                        _unitOfWork.CompanyOrders.Update(companyOrder);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult TrashbyCompany(long id)
        {
            if (ModelState.IsValid)
            {
                var companyOrder = _unitOfWork.CompanyOrders.Get(x => x.Id == id && x.Status == Status.Active);
                if (companyOrder != null)
                {
                    companyOrder.Status = Status.End;
                    try
                    {
                        _unitOfWork.CompanyOrders.Update(companyOrder);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }



        [HttpGet]
        public IActionResult Waited()
        {
            return View();
        }

        [AjaxOnly]
        [HttpPost]
        public IActionResult WaitedData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyOrders.FindDataTable(x => x.Status == Status.Waited, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }


        public IActionResult GetCities(long id)
        {
            var country = _unitOfWork.Countries.Get(x => x.Id == id && x.Status == Status.Active);
            if (country != null)
            {
                var list = new List<SelectListItem>();

                var cities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == country.Id).OrderBy(x => x.Name).Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                if (cities != null)
                {
                    list.AddRange(cities);
                    return Json(list);
                }
            }
            return View("Error", "Home");
        }

    }
}


