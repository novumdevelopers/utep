﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.CompanyTypeViewModels;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class CompanyTypeController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        public CompanyTypeController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyTypes.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateCompanyTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var companyType = new CompanyType
                {
                    Name = model.Name
                };
                try
                {
                    _unitOfWork.CompanyTypes.Add(companyType);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var companyTypeDb = _unitOfWork.CompanyTypes.Get(x => x.Id == id && x.Status == Status.Active);
            if (companyTypeDb != null)
            {
                var model = new EditCompanyTypeViewModel
                {
                    Id = companyTypeDb.Id,
                    Name = companyTypeDb.Name
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditCompanyTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var companyTypeDb = _unitOfWork.CompanyTypes.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (companyTypeDb != null)
                {
                    companyTypeDb.Name = model.Name;
                    try
                    {
                        _unitOfWork.CompanyTypes.Update(companyTypeDb);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyTypes.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var companyType = _unitOfWork.CompanyTypes.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (companyType != null)
                {
                    companyType.Status = Status.Active;
                    try
                    {
                        _unitOfWork.CompanyTypes.Update(companyType);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var companyType = _unitOfWork.CompanyTypes.Get(x => x.Id == id && x.Status == Status.Active);
                if (companyType != null)
                {
                    companyType.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.CompanyTypes.Update(companyType);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}

