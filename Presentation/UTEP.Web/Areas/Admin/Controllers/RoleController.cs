﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using UTEP.Web.Areas.Admin.Models.RoleViewModels;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class RoleController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly RoleManager<Role> _roleManager;

        public RoleController(RoleManager<Role> roleManager, DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _roleManager = roleManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            // Skiping number of Rows count
            var start = Request.Form["start"].FirstOrDefault();
            // Paging Length 10,20
            var length = Request.Form["length"].FirstOrDefault();
            // Sort Column Name
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            // Sort Column Direction ( asc ,desc)
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            // Search Value from (Search box)
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            //Paging Size (10,20,50,100)
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Roles.FindDataTable(x=> true, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = new Role
                {
                    Name = model.Name,
                    Description = model.Description
                };
                var roleResult = await _roleManager.CreateAsync(role);
                if (roleResult.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Something went wrong. Please try again.");
                    return View(model);
                }

            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            if (ModelState.IsValid)
            {
                var roleInDb = await _roleManager.FindByIdAsync(id);
                if (roleInDb != null)
                {
                    var model = new EditRoleViewModel()
                    {
                        Id = roleInDb.Id,
                        Name = roleInDb.Name,
                        Description = roleInDb.Description
                    };
                    return View(model);
                }
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var roleInDb = await _roleManager.FindByIdAsync(model.Id.ToString());
                if (roleInDb != null)
                {

                    roleInDb.Name = model.Name;
                    roleInDb.Description = model.Description;
                    var roleResult = await _roleManager.UpdateAsync(roleInDb);
                    if (roleResult.Succeeded)
                    {
                        return RedirectToAction(nameof(Index), new { id = " " });
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Something went wrong. Please try again.");
                        return View(model);
                    }
                }

            }
            return View(model);
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        #endregion
    }
}
