﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using UTEP.Web.Areas.Admin.Models.UserViewModels;
using UTEP.Web.Areas.Admin.Models.CompanyClientAdminViewModels;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using Microsoft.AspNetCore.Authorization;
using UTEP.Web.Services;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class CompanyClientAdminController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly UnitOfWork _unitOfWork;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;

        public CompanyClientAdminController(
            DbContextOptions<UTEPDbContext> contextOptions,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            ILoggerFactory loggerFactory,
            IEmailSender emailSender
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _userManager = userManager;
            _roleManager = roleManager;
            _logger = loggerFactory.CreateLogger<UserController>();
            _emailSender = emailSender;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Companies.FindDataTable(x => x.Status == Status.Active && x.TypeOfCompany == TypeOfCompany.Client, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateCompanyClientAdminViewModel
            {
                CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.Name
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateCompanyClientAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_userManager.Users.Any(x => x.Email == model.CreateUsers.Email))
                {
                    ModelState.AddModelError("", $"{model.CreateUsers.Email} emaili artıq mövcuddur.");
                    model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    });
                    return View(model);
                }
                if (_unitOfWork.Companies.GetWithUsers(x => x.TypeOfCompany == TypeOfCompany.Client && x.User.TypeOfUser == TypeOfUsers.CompanyConfirmed).Any(x => x.Email == model.CompanyEmail))
                {
                    ModelState.AddModelError("", $"{model.CompanyEmail} emaili artıq mövcuddur.");
                    model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    });
                    return View(model);
                }
                if (_unitOfWork.Companies.Find(x => x.TypeOfCompany == TypeOfCompany.Client).Any(x => x.Name == model.CompanyName))
                {
                    ModelState.AddModelError("", $"{model.CompanyName} adlı şirkət artıq mövcuddur.");
                    model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    });
                    return View(model);
                }

                var user = new User
                {
                    UserName = model.CreateUsers.Email,
                    Email = model.CreateUsers.Email,
                    FirstName = model.CreateUsers.FirstName,
                    LastName = model.CreateUsers.LastName,
                    PhoneNumber = model.CreateUsers.PhoneNumber,
                    TypeOfUser = TypeOfUsers.CompanyConfirmed,
                    Company = new Company
                    {
                        CompanyTypeId = model.CompanyTypeId,
                        TaxIdentificationNumber = Guid.NewGuid().ToString(),
                        Name = model.CompanyName,
                        Email = model.CreateUsers.Email,
                        Address = model.CompanyAddress,
                        Phone = model.CompanyPhone,
                        LocationLatitude = 0m,
                        LocationLongitude = 0m,
                        TypeOfCompany = TypeOfCompany.Client,
                        VOEN = model.VOEN
                    },
                    EmailConfirmed = true
                };

                try
                {
                    var role = await _roleManager.FindByNameAsync("Manager");
                    if (role != null)
                    {
                        user.Roles.Add(new IdentityUserRole<long>
                        {
                            UserId = user.Id,
                            RoleId = role.Id
                        });
                        var result = await _userManager.CreateAsync(user, model.CreateUsers.Password);

                        if (result.Succeeded)
                        {
                            //var token = _userManager.GenerateEmailConfirmationTokenAsync(user).ToString();
                            //await _userManager.ConfirmEmailAsync(user, token);
                            _logger.LogInformation(3, "User created a new account with password.");

                            //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            //var callbackUrl = Url.Action(nameof(ConfirmEmail), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                            var message = $"<!DOCTYPE html><html lang='az'><head> <meta charset='UTF-8'> <title>UTEP</title></head><body style='text-align:center;'> <table> <tr> <td align='center'> <img src='http://utep.a2hosted.com/logo/ULogoNew2.png' alt='UTEP' style='width:60px; height:60px;'> </td></tr><tr> <td style='font-size: 18px'> <h3 align='center'> UTEP-ə qatıldığınız üçün şadıq! </h3> <p> Siz artıq elanlarınızı saytda yerləşdirə bilərsiniz. </p> <p> Sayta giriş üçün emailiniz: <b> {model.CompanyEmail} </b>, şifrəniz: <b> {model.CreateUsers.Password} </b> </br>. </p><p> Unutmayın, siz istənilən zaman <a href='mailto:support@utep.az'>support@utep.az</a> emaili vasitəsilə bizə müraciət edə bilərsiniz. Biz UTEP.az servisi haqda sizin bütün suallarınıza cavab verməyə hazırıq. </p><br><hr> </td></tr><tr> <td align='center' style='font-size: 14px'> <p> Cəfər Cabbarlı küçəsi, 44, Bakı, Azərbaycan </p><p> <a href='tel:+994777779696'> +994 77 777 96 96</a> <br><a href='mailto:support@utep.az'>support@utep.az</a> </p></td></tr></table></body></html>";
                            await _emailSender.SendEmailAsync(model.CompanyEmail, "UTEP profiliniz haqqında məlumat!", message);

                            return RedirectToAction(nameof(Index), new { id = "" });
                        }
                        AddErrors(result);

                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var company = _unitOfWork.Companies.GetWithUser(x => x.Id == id && x.Status == Status.Active);
            if (company != null)
            {
                var model = new EditCompanyClientAdminViewModel
                {
                    Id = company.Id,
                    CompanyName = company.Name,
                    CompanyEmail = company.Email,
                    CompanyPhone = company.Phone,
                    CompanyAddress = company.Address,
                    VOEN = company.VOEN,
                    CompanyTypeId = company.CompanyTypeId,
                    CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    }),
                    EditUser = new EditUserViewModel
                    {
                        Id = company.Id,
                        FirstName = company.User.FirstName,
                        LastName = company.User.LastName,
                        Email = company.User.Email,
                        PhoneNumber = company.User.PhoneNumber,
                        UserGroupId = company.User.UserGroupId
                    },
                    ownCompanyEmail = company.Email,
                    ownCompanyName = company.Name,
                    ownUserEmail = company.User.Email
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditCompanyClientAdminViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_userManager.Users.Where(x => x.Email != model.ownUserEmail).Any(x => x.Email == model.EditUser.Email))
                {
                    ModelState.AddModelError("", $"{model.CompanyEmail} emaili artıq mövcuddur.");
                    model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    });
                    return View(model);
                }
                if (_unitOfWork.Companies.Find(x => x.TypeOfCompany == TypeOfCompany.Client && x.Email != model.ownCompanyEmail).Any(x => x.Email == model.CompanyEmail))
                {
                    ModelState.AddModelError("", $"{model.CompanyEmail} emaili artıq mövcuddur.");
                    model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    });
                    return View(model);
                }
                if (_unitOfWork.Companies.Find(x => x.TypeOfCompany == TypeOfCompany.Client && x.Name != model.ownCompanyName).Any(x => x.Name == model.CompanyName))
                {
                    ModelState.AddModelError("", $"{model.CompanyName} adlı şirkət artıq mövcuddur.");
                    model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                    {
                        Value = y.Id.ToString(),
                        Text = y.Name
                    });
                    return View(model);
                }
                var company = _unitOfWork.Companies.GetWithUser(x => x.Id == model.Id && x.Status == Status.Active);
                if (company != null)
                {
                    company.Name = model.CompanyName;
                    company.Phone = model.CompanyPhone;
                    company.Email = model.CompanyEmail;
                    company.Address = model.CompanyAddress;
                    company.VOEN = model.VOEN;
                    company.CompanyTypeId = model.CompanyTypeId;
                    company.User.FirstName = model.EditUser.FirstName;
                    company.User.LastName = model.EditUser.LastName;
                    company.User.Email = model.EditUser.Email;
                    company.User.UserName = model.EditUser.Email;
                    company.User.NormalizedEmail = model.EditUser.Email.ToUpper();
                    company.User.NormalizedUserName = model.EditUser.Email.ToUpper();
                    company.User.PhoneNumber = model.EditUser.PhoneNumber;
                    try
                    {
                        _userManager.UpdateAsync(company.User);
                        _unitOfWork.Companies.Update(company);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            model.CompanyTypes = _unitOfWork.CompanyTypes.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }


        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Companies.FindDataTable(x => x.Status == Status.Deleted && x.TypeOfCompany == TypeOfCompany.Client, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var company = _unitOfWork.Companies.GetWithUser(x => x.Id == id && x.User.TypeOfUser == TypeOfUsers.CompanyNotConfirmed);
                if (company != null)
                {
                    company.User.TypeOfUser = TypeOfUsers.CompanyConfirmed;
                    company.Status = Status.Active;
                    try
                    {
                        _unitOfWork.Companies.Update(company);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var company = _unitOfWork.Companies.GetOrderAndUser(x => x.Id == id && x.Status == Status.Active);
                if (company != null)
                {
                    company.Status = Status.Deleted;
                    company.User.TypeOfUser = TypeOfUsers.CompanyNotConfirmed;
                    company.CompanyProducts.Where(x => x.Status == Status.Active && x.CompanyOrder.Status == Status.Active).ToList()
                            .ForEach(x => x.CompanyOrder.Status = Status.Deleted);
                    try
                    {
                        _unitOfWork.Companies.Update(company);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpGet]
        public IActionResult Waited()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult WaitedData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Companies.FindDataTable(x => x.Status == Status.Active &&
                                                                x.TypeOfCompany == TypeOfCompany.Client &&
                                                                x.User.TypeOfUser == TypeOfUsers.CompanyNotConfirmed &&
                                                                x.User.EmailConfirmed,
                                                                "az",
                                                                skip,
                                                                pageSize,
                                                                sortColumn,
                                                                sortColumnDirection,
                                                                searchValue);

            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }


        [HttpGet]
        public IActionResult EmailNotConfirmed()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult EmailNotConfirmedData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Companies.FindDataTable(x => x.Status == Status.Active &&
                                                                x.TypeOfCompany == TypeOfCompany.Client &&
                                                                x.User.TypeOfUser == TypeOfUsers.CompanyConfirmed &&
                                                                !x.User.EmailConfirmed,
                                                                "az",
                                                                skip,
                                                                pageSize,
                                                                sortColumn,
                                                                sortColumnDirection,
                                                                searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

    }
}

