﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Data.Domain.Models.Tables.RoleBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Web.Areas.Admin.Models.UserViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Settings;
using System.Security.Claims;
using UTEP.Web.Attribute;
using UTEP.Web.Models.AccountViewModels;
using Microsoft.AspNetCore.Authorization;
using UTEP.Web.Models.ManageViewModels;
using UTEP.Web.Controllers;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly UnitOfWork _unitOfWork;
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger _logger;

        public UserController(
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            DbContextOptions<UTEPDbContext> contextOptions,
            SignInManager<User> signInManager,
            ILoggerFactory loggerFactory
            )
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<UserController>();
        }

        [HttpGet]
        public IActionResult Admin()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult AdminData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Users.FindDataTable(x => x.Roles.FirstOrDefault().RoleId == 1, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult CreateAdmin()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAdmin(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };
                var role = await _roleManager.FindByNameAsync("Admin");
                user.Roles.Add(new IdentityUserRole<long>
                {
                    UserId = user.Id,
                    RoleId = role.Id
                });
                var result = _userManager.CreateAsync(user, model.Password).Result;
                if (result.Succeeded)
                {
                    var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var confirmResult = _userManager.ConfirmEmailAsync(user, token).Result;

                    if (confirmResult.Succeeded)
                    {
                        _logger.LogInformation("User created a new account with password.");

                        //await _signInManager.SignInAsync(user, isPersistent: false);
                        return RedirectToAction(nameof(UserController.Admin), "User", new { id = "" });
                    }
                }
                AddErrors(result);
            }
            return View();
        }

        [HttpGet]
        public IActionResult Manager()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult ManagerData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Users.FindDataTable(x => x.Roles.FirstOrDefault().RoleId == 2 && x.Company.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Driver()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult DriverData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Users.FindDataTable(x => x.Roles.FirstOrDefault().RoleId == 3, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }


        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignOutAsync();
                        return RedirectToAction(nameof(AccountController.Login), "Account", new { area = "" });
                    }
                }
            }
            return RedirectToAction("Error", "Home");
        }

        //[HttpGet]
        //public IActionResult Create()
        //{
        //    var model = new CreateUserViewModel();
        //    return View(model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create(CreateUserViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var userGroup = _unitOfWork.UserGroups.Get(x => x.Name == "Client");
        //        var user = new User { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, PhoneNumber = model.PhoneNumber, UserGroupId = userGroup.Id};
        //        var result = await _userManager.CreateAsync(user, model.Password);

        //        if (result.Succeeded)
        //        {
        //            // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
        //            // Send an email with this link
        //            //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
        //            //var callbackUrl = Url.Action(nameof(ConfirmEmail), "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
        //            //await _emailSender.SendEmailAsync(model.Email, "Confirm your account",
        //            //    $"Please confirm your account by clicking this link: <a href='{callbackUrl}'>link</a>");
        //            //await _signInManager.SignInAsync(user, isPersistent: false);
        //            _logger.LogInformation(3, "User created a new account with password.");

        //            // Linkin aktiv olma vaxti 
        //            //var content = DateTime.Now.AddMinutes(1).ToString();
        //            //var encrypted = Cryptography.EncryptString(content, _key);

        //            return RedirectToAction("Index");

        //        }
        //        AddErrors(result);
        //    }
        //    return View(model);
        //}

        //[HttpGet]
        //public async Task<IActionResult> Edit(long id)
        //{
        //    var userInDb = await _userManager.FindByIdAsync(id.ToString());
        //    if (userInDb != null)
        //    {
        //        var existingRole = _userManager.GetRolesAsync(userInDb).Result.FirstOrDefault();
        //        var existingRoleInDb = _roleManager.Roles.FirstOrDefault(r => r.Name == existingRole);
        //        var model = new EditUserViewModel
        //        {
        //            Roles = _roleManager.Roles.Select(r => new SelectListItem
        //                {
        //                    Text = r.Name,
        //                    Value = r.Id.ToString()
        //                }).ToList(),
        //            UserGroups = _unitOfWork.UserGroups
        //                .Find(x => x.Status == Status.Active)
        //                .Select(u => new SelectListItem()
        //                {
        //                    Text = u.Name,
        //                    Value = u.Id.ToString()
        //                }).ToList(),
        //            UserGroupId = userInDb.UserGroupId,
        //            FirstName = userInDb.FirstName,
        //            LastName = userInDb.LastName,
        //            Email = userInDb.Email,
        //            RoleId = existingRoleInDb?.Id,
        //            PhoneNumber = userInDb.PhoneNumber
        //        };
        //        return View(model);
        //    }

        //    return RedirectToAction("Create");
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(EditUserViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = await _userManager.FindByIdAsync(model.Id.ToString());
        //        if (user != null)
        //        {
        //            user.FirstName = model.FirstName;
        //            user.LastName = model.LastName;
        //            user.Email = model.Email;
        //            var existingRole = _userManager.GetRolesAsync(user).Result.FirstOrDefault();
        //            var existingRoleInDb = _roleManager.Roles.FirstOrDefault(r => r.Name == existingRole);

        //            if (user.UserGroupId != model.UserGroupId)
        //            {
        //                var newUserGroupClaims = _unitOfWork.UserGroupClaims.Find(x => x.UserGroupId == model.UserGroupId).ToList();
        //                var o = _unitOfWork.UserGroupClaims.Find(x => x.UserGroupId == user.UserGroupId).ToList();
        //                var oldUserGroupClaims = _unitOfWork.UserGroupClaims.Find(x => x.UserGroupId == user.UserGroupId).Select(x => new Claim(x.ClaimData, x.ClaimData)).ToList();
        //                await _userManager.RemoveClaimsAsync(user, oldUserGroupClaims);
        //                var UserGroupClaimListForAdd = new List<Claim>();
        //                var userClaims = await _userManager.GetClaimsAsync(user);

        //                foreach (var userClaim in newUserGroupClaims)
        //                {
        //                    if (userClaims.Count() == 0 || userClaims.Any(x => x.Value != userClaim.ClaimData))
        //                    {
        //                        user.Claims.Add(new IdentityUserClaim<long>
        //                        {
        //                            ClaimType = userClaim.ClaimData,
        //                            ClaimValue = userClaim.ClaimData,
        //                            UserId = user.Id
        //                        });
        //                    }
        //                }
        //                user.UserGroupId = model.UserGroupId;
        //                var resultx = await _userManager.UpdateAsync(user);
        //            }
        //            var result = await _userManager.UpdateAsync(user);
        //            if (result.Succeeded)
        //            {

        //                if (existingRoleInDb != null && existingRoleInDb.Id != model.RoleId)
        //                {
        //                    var roleResult = await _userManager.RemoveFromRoleAsync(user, existingRole);
        //                }
        //                if (model.RoleId != null)
        //                {
        //                    var role = await _roleManager.FindByIdAsync(model.RoleId.ToString());
        //                    if (role != null)
        //                    {
        //                        var newRoleResult = await _userManager.AddToRoleAsync(user, role.Name);
        //                        if (newRoleResult.Succeeded)
        //                        {
        //                            var rrr = await _userManager.UpdateAsync(user);

        //                            return RedirectToAction("Index");
        //                        }
        //                    }
        //                }

        //                var rer = await _userManager.UpdateAsync(user);
        //                if (rer.Succeeded)
        //                {
        //                    return RedirectToAction("Index");
        //                }
        //            }

        //        }
        //    }
        //    model.Roles = _roleManager.Roles.Select(r => new SelectListItem
        //    {
        //        Text = r.Name,
        //        Value = r.Id.ToString()
        //    }).ToList();
        //    model.UserGroups = _unitOfWork.UserGroups.Find(x => x.Status == Status.Active).Select(u => new SelectListItem
        //    {
        //        Text = u.Name,
        //        Value = u.Id.ToString()
        //    }).ToList();
        //    return View(model);
        //}

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        #endregion
    }
}