﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using Microsoft.EntityFrameworkCore;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Web.Attribute;
using UTEP.Web.Areas.Admin.Models.VehicleTypeViewModels;
using UTEP.Data.Domain.Models.Tables.VehicleBasedGroup;
using UTEP.Settings;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id:int?}")]
    public class VehicleTypeController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;

        public VehicleTypeController(
            UserManager<User> userManager,
            DbContextOptions<UTEPDbContext> contextOptions
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.VehicleTypes.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public IActionResult Create(string language)
        {
            //lang = string.IsNullOrEmpty(RouteData.Values["lang"].ToString()) ? "az" : RouteData.Values["lang"].ToString();

            var siteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active);
            if (!siteLanguages.Any(x => x.LanguageCode.ToLower() == language))
            {
                language = siteLanguages.First(x => x.Status == Status.Active).LanguageCode.ToLower();
            }
            var model = new CreateVehicleTypeViewModel
            {
                LanguageCode = language.ToLower(),
                SiteLanguages = siteLanguages.Select(y => new SiteLanguageViewModel
                {
                    Name = y.Name,
                    LanguageCode = y.LanguageCode.ToLower(),
                    OrderIdex = y.OrderIndex
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{language?}")]
        public IActionResult Create(CreateVehicleTypeViewModel model)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode);
            if (siteLanguages == null)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                var vehicleType = new VehicleType();

                vehicleType.VehicleTypeTranslations.Add(new VehicleTypeTranslation
                {
                    Name = model.Name,
                    SiteLanguage = siteLanguages
                });

                try
                {
                    _unitOfWork.VehicleTypes.Add(vehicleType);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            model.SiteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active).Select(x => new SiteLanguageViewModel
            {
                Name = x.Name,
                LanguageCode = x.LanguageCode,
                OrderIdex = x.OrderIndex
            });
            return View(model);
        }

        [HttpGet]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{id?}/{language?}")]
        public IActionResult Edit(long id, string language)
        {
            var siteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active);
            if (!siteLanguages.Any(x => x.LanguageCode.ToLower() == language))
            {
                language = siteLanguages.First(x => x.Status == Status.Active).LanguageCode.ToLower();
            }

            var vehicleType = _unitOfWork.VehicleTypes.Get(x => x.Id == id && x.Status == Status.Active);
            var vehicleTypeLanguage = vehicleType.VehicleTypeTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == language);
            if (vehicleType != null)
            {
                var model = new EditVehicleTypeViewModel
                {
                    Id = vehicleType.Id,
                    Name = vehicleTypeLanguage != null ? vehicleTypeLanguage.Name : "",
                    LanguageCode = language,
                    SiteLanguages = siteLanguages.Select(x => new SiteLanguageViewModel
                    {
                        LanguageCode = x.LanguageCode,
                        Name = x.Name,
                        OrderIdex = x.OrderIndex
                    })
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/[area]/{lang:lang}/[controller]/[action]/{id}/{language?}")]
        public IActionResult Edit(EditVehicleTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var vehicleType = _unitOfWork.VehicleTypes.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (vehicleType == null)
                {
                    return NotFound();
                }
                var vehicleTranslation = vehicleType.VehicleTypeTranslations.FirstOrDefault(x => x.SiteLanguage.LanguageCode == model.LanguageCode);
                if (vehicleTranslation != null)
                {
                    vehicleTranslation.Name = model.Name;
                }
                else
                {
                    var language = _unitOfWork.SiteLanguages.Get(x => x.LanguageCode == model.LanguageCode);
                    if (language != null)
                    {
                        vehicleType.VehicleTypeTranslations.Add(new VehicleTypeTranslation
                        {
                            Name = model.Name,
                            SiteLanguageId = language.Id
                        });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Belə bir dil yoxdur.");
                        model.SiteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active).Select(x => new SiteLanguageViewModel
                        {
                            LanguageCode = x.LanguageCode,
                            Name = x.Name,
                            OrderIdex = x.OrderIndex
                        });
                        return View(model);
                    }
                }

                try
                {
                    _unitOfWork.VehicleTypes.Update(vehicleType);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {

                    throw;
                }
            }
            model.SiteLanguages = _unitOfWork.SiteLanguages.Find(x => x.Status == Status.Active).Select(y => new SiteLanguageViewModel
            {
                Name = y.Name,
                LanguageCode = y.LanguageCode.ToLower(),
                OrderIdex = y.OrderIndex
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.VehicleTypes.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var vehicleType = _unitOfWork.VehicleTypes.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (vehicleType != null)
                {
                    vehicleType.Status = Status.Active;
                    try
                    {
                        _unitOfWork.VehicleTypes.Update(vehicleType);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var vehicleType = _unitOfWork.VehicleTypes.Get(x => x.Id == id && x.Status == Status.Active);
                if (vehicleType != null)
                {
                    vehicleType.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.VehicleTypes.Update(vehicleType);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}