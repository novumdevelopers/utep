﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.Admin.Models.DeliveryDetailViewModels;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Data.Domain.Models.Tables.DeliveryBasedGroup;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class DeliveryDetailController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public DeliveryDetailController(
            DbContextOptions<UTEPDbContext> contextOptions
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.DeliveryDetails.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateDeliveryDetailViewModel
            {
                Deliveries = _unitOfWork.Deliveries.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.CompanyOrder.Products.First(z => z.Status == Status.Active).Name
                })
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateDeliveryDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var deliveryDetail = new DeliveryDetail
                {
                    ShippingId = model.DeliveryId,
                    DestinationTownLatitude = model.DestinationTownLatitude,
                    DestinationTownLongitude=model.DestinationTownLongitude,
                    FinishDateTime=model.FinishDate
                };
                try
                {
                    _unitOfWork.DeliveryDetails.Add(deliveryDetail);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }

            model.Deliveries = _unitOfWork.Deliveries.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.CompanyOrder.Products.First(z => z.Status == Status.Active).Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var deliveryDetail = _unitOfWork.DeliveryDetails.Get(x => x.Id == id && x.Status == Status.Active);
            if (deliveryDetail != null)
            {
                var model = new EditDeliveryDetailViewModel
                {
                    DeliveryId = deliveryDetail.ShippingId,
                    Deliveries = _unitOfWork.Deliveries.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.CompanyOrder.Products.First(z => z.Status == Status.Active).Name
                    }),
                    DestinationTownLatitude = deliveryDetail.DestinationTownLatitude,
                    DestinationTownLongitude = deliveryDetail.DestinationTownLongitude,
                    FinishDate = deliveryDetail.FinishDateTime
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditDeliveryDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var deliveryDetail = _unitOfWork.DeliveryDetails.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (deliveryDetail != null)
                {
                    deliveryDetail.ShippingId = model.DeliveryId;
                    deliveryDetail.DestinationTownLatitude = model.DestinationTownLatitude;
                    deliveryDetail.DestinationTownLongitude = model.DestinationTownLongitude;
                    deliveryDetail.FinishDateTime = model.FinishDate;
                    try
                    {
                        _unitOfWork.DeliveryDetails.Update(deliveryDetail);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            model.Deliveries = _unitOfWork.Deliveries.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.CompanyOrder.Products.First(z => z.Status == Status.Active).Name
            });

            return View(model);

        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.DeliveryDetails.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var deliveryDetail = _unitOfWork.DeliveryDetails.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (deliveryDetail != null)
                {
                    deliveryDetail.Status = Status.Active;
                    try
                    {
                        _unitOfWork.DeliveryDetails.Update(deliveryDetail);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var deliveryDetail = _unitOfWork.DeliveryDetails.Get(x => x.Id == id && x.Status == Status.Active);
                if (deliveryDetail != null)
                {
                    deliveryDetail.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.DeliveryDetails.Update(deliveryDetail);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }
    }
}

