﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.CountryBasedGroup;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]")]
    public class ImportController : Controller
    {
        private readonly UnitOfWork _unitOfWork;

        public ImportController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(string a)
        {
            //string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"D:\Work_Project\Utep Additional\Zips\simplemaps_worldcities_basicv1.4\worldcities.xlsx";
            FileInfo file = new FileInfo(sFileName);
            try
            {
                var country = new Country();
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    int row = 0;
                    List<Country> countries = new List<Country>();
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.First();
                    int rowCount = worksheet.Dimension.Rows;
                    int ColCount = worksheet.Dimension.Columns;

                    for (row = 1; row <= rowCount; row++)
                    {
                        if (row != 1)
                        {
                            if (row == 2 || worksheet.Cells[row, 5].Value != worksheet.Cells[row - 1, 5].Value)
                            {
                                country = new Country
                                {
                                    Name = worksheet.Cells[row, 5].Value != null ? worksheet.Cells[row, 5].Value.ToString() : "NoName"
                                };
                            }

                            country.Cities.Add(new City
                            {
                                Name = worksheet.Cells[row, 1].Value.ToString()
                            });

                            
                            if (worksheet.Cells[row, 5].Value != worksheet.Cells[row + 1, 5].Value)
                            {
                                countries.Add(country);
                            }
                        }
                    }
                    _unitOfWork.Countries.AddRange(countries);
                    _unitOfWork.Complete();
                }
            }
            catch (Exception ex)
            {
                /*"Some error occured while importing." + */
                ex.Message.ToString();
            }
            return View();

        }
    }
}