﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Web.Areas.Admin.Models.TruckViewModels;
using UTEP.Data.Domain.Models.Tables.TruckBasedGroup;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace UTEP.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class TruckController : Controller
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IFileProcessor _fileProcessor;
        public TruckController(
            DbContextOptions<UTEPDbContext> contextOptions,
            IFileProcessor fileProcessor
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileProcessor = fileProcessor;
        }


        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Trucks.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpGet]
        public IActionResult Create()
        {
            var model = new CreateTruckViewModel
            {
                VehicleMarks = _unitOfWork.VehicleMarks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
                {
                    Value = y.Id.ToString(),
                    Text = y.Name
                })
            };

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateTruckViewModel model)
        {
            if (ModelState.IsValid)
            {
                var truck = new Truck
                {
                    YearOf = model.YearOf,
                    RegistrationNumber = model.RegistrationNumber,
                    VehicleModelId = model.VehicleModelId
                };
                if (model.RegistrationCertificate != null && !String.IsNullOrEmpty(model.RegistrationCertificate?.Name) && model.RegistrationCertificate.Length > 0)
                {
                    string folder = $"truck\\";
                    var ae = new string[] { ".jpg", ".png", ".jpeg" };
                    truck.RegistrationCertificate = await _fileProcessor.UploadFileAsync(model.RegistrationCertificate, folder, ae);
                };
                try
                {
                    _unitOfWork.Trucks.Add(truck);
                    _unitOfWork.Complete();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {

                    throw;
                }
            }

            model.VehicleMarks = _unitOfWork.VehicleMarks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            return View();
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var truck = _unitOfWork.Trucks.Get(x => x.Id == id && x.Status == Status.Active);
            if (truck != null)
            {
                var model = new EditTruckViewModel
                {
                    Id = truck.Id,
                    CertificatePath = truck.RegistrationCertificate,
                    RegistrationNumber = truck.RegistrationNumber,
                    YearOf = truck.YearOf
                };
                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditTruckViewModel model)
        {
            if (ModelState.IsValid)
            {
                var truck = _unitOfWork.Trucks.Get(x => x.Id == model.Id && x.Status == Status.Active);
                if (truck != null)
                {
                    truck.RegistrationNumber = model.RegistrationNumber;
                    truck.YearOf = model.YearOf;
                    if (model.RegistrationCertificate != null && !String.IsNullOrEmpty(model.RegistrationCertificate?.Name) && model.RegistrationCertificate.Length > 0)
                    {
                        string folder = $"truck\\";
                        var ae = new string[] { ".jpg", ".png", ".jpeg" };
                        truck.RegistrationCertificate = await _fileProcessor.UploadFileAsync(model.RegistrationCertificate, folder, ae);
                    };
                    try
                    {
                        _unitOfWork.Trucks.Update(truck);
                        _unitOfWork.Complete();
                        return RedirectToAction("Index", new { id = ""});
                    }
                    catch (Exception)
                    {
                        return RedirectToAction("Error", "Home", new { area = " " });
                    }
                }
            }
            model.VehicleMarks = _unitOfWork.VehicleMarks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
            {
                Value = y.Id.ToString(),
                Text = y.Name
            });
            return View(model);
        }

        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        public IActionResult TrashData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.Trucks.FindDataTable(x => x.Status == Status.Deleted, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            if (ModelState.IsValid)
            {
                var truck = _unitOfWork.Trucks.Get(x => x.Id == id && x.Status == Status.Deleted);
                if (truck != null)
                {
                    truck.Status = Status.Active;
                    try
                    {
                        _unitOfWork.Trucks.Update(truck);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            if (ModelState.IsValid)
            {
                var truck = _unitOfWork.Trucks.Get(x => x.Id == id && x.Status == Status.Active);
                if (truck != null)
                {
                    truck.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.Trucks.Update(truck);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Message = "success", Status = true });
                    }
                    catch (Exception e)
                    {
                        return Json(new { Message = "error", Status = false });
                    }
                }
            }
            return Json(new { Message = "error", Status = false });
        }

    }
}
