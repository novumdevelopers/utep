﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.TrailerViewModels
{
    public class EditTrailerViewModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public decimal Size { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public decimal Tonnage { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public int PaletteCount { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public string RegistrationNumber { get; set; }

        public IFormFile RegistrationCertificate { get; set; }
        public string ImagePath { get; set; }

        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long TrailerTypeId { get; set; }
        public IEnumerable<SelectListItem> TrailerTypes { get; set; }

        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long VehicleModelId { get; set; }
        public IEnumerable<SelectListItem> VehicleModels { get; set; }
    }
}
