﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UTEP.Web.Areas.Admin.Models.UserViewModels
{
    public class CreateUserViewModel
    {
        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        public string LastName { get; set; }

        [DataType(dataType: DataType.EmailAddress)]
        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        [StringLength(18, ErrorMessage = " {0} ən az {2} simvol uzunluğundan ibarət olmalıdır.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Şifrələr bir biri ilə uyğunlaşmır.")]
        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public string PhoneNumber { get; set; }
    }
}
