﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UTEP.Web.Areas.Admin.Models.UserViewModels
{
    public class EditUserViewModel
    {

        public long Id { get; set; }

        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        public string LastName { get; set; }

        [DataType(dataType: DataType.EmailAddress)]
        [Required(ErrorMessage = "Boş ola bilməz.", AllowEmptyStrings = false)]
        public string Email { get; set; }


        public long? UserGroupId { get; set; }
        public List<SelectListItem> UserGroups { get; set; }

        public long? RoleId { get; set; }
        public List<SelectListItem> Roles { get; set; }

        public string PhoneNumber { get; set; }
    }
}