﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels
{
    public class CreateSiteLanguageViewModel
    {
        [Required(ErrorMessage ="Doldurulmalıdır.")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Doldurulmalıdır.")]
        public string Code { get; set; }
        [Required]
        public int OrderIndex { get; set; }
    }
}
