﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels
{
    public class SiteLanguageViewModel
    {
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        public int OrderIdex { get; set; }
    }
}
