﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Areas.CompanyProfile.Models.ProductViewModels;

namespace UTEP.Web.Areas.Admin.Models.CompanyOrderAdminViewModels
{
    public class CreateCompanyOrderAdminViewModel
    {
        public decimal ProporsalPrice { get; set; }
        //public string From { get; set; }
        //public string To { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime DealDate { get; set; }
        public int Currency { get; set; }
        public int Weight { get; set; }


        public CreateProductViewModel CreateProduct { get; set; }
        public string FromDescription { get; set; }
        public string ToDescription { get; set; }

        public long FromCountryId { get; set; }
        public IEnumerable<SelectListItem> FromCountries { get; set; }
        public long FromCityId { get; set; }
        public IEnumerable<SelectListItem> FromCities { get; set; }


        public long ToCountryId { get; set; }
        public IEnumerable<SelectListItem> ToCountries { get; set; }
        public long ToCityId { get; set; }
        public IEnumerable<SelectListItem> ToCities { get; set; }

        public string Lang { get; set; }
    }
}
