﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.CooperationBasedGroup;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;

namespace UTEP.Web.Areas.Admin.Models.SiteTextViewModels
{
    public class EditSiteTextViewModel
    {
        public EditSiteTextViewModel()
        {
        }
        public long Id { get; set; }
        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Body1 { get; set; }
        public string Body2 { get; set; }
        public string Body3 { get; set; }
        public string ImagePath1 { get; set; }
        public string ImagePath2 { get; set; }
        public string ImagePath3 { get; set; }
        public string ImageForView1 { get; set; }
        public string ImageForView2 { get; set; }
        public string ImageForView3 { get; set; }
        public string LanguageCode { get; set; }
        public IEnumerable<SiteLanguageViewModel> SiteLanguages { get; set; }
        public IQueryable<Cooperation> Cooperations { get; set; }
    }
}
