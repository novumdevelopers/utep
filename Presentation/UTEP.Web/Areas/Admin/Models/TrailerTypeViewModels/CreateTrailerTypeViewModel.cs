﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.TrailerTypeViewModels
{
    public class CreateTrailerTypeViewModel
    {
        [Required(ErrorMessage ="Doldurulmalıdır.")]
        public string Name { get; set; }
    }
}
