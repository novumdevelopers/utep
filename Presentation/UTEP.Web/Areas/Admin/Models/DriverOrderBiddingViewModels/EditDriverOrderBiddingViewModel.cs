﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.DriverOrderBiddingViewModels
{
    public class EditDriverOrderBiddingViewModel
    {
        public long Id { get; set; }

        public long DriverId { get; set; }
        public IEnumerable<SelectListItem> Drivers { get; set; }
        public long OfferToDriverId { get; set; }
        public IEnumerable<SelectListItem> OfferToDrivers { get; set; }
        public decimal SuggestedPrice { get; set; }
    }
}
