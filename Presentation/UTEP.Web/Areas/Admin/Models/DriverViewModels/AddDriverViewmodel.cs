﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.DriverViewModels
{
    public class AddDriverViewmodel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public long CompanyId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }
        public long TruckId { get; set; }
        public IEnumerable<SelectListItem> Trucks { get; set; }
        public long TrailerId { get; set; }
        public IEnumerable<SelectListItem> Trailers { get; set; }
    }
}
