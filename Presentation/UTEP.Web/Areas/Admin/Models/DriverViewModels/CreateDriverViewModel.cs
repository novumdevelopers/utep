﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Web.Areas.Admin.Models.UserViewModels;

namespace UTEP.Web.Areas.Admin.Models.DriverViewModels
{
    public class CreateDriverViewModel
    {
        public CreateUserViewModel CreateUsers { get; set; }

        public long TruckId { get; set; }
        public IEnumerable<SelectListItem> Trucks { get; set; }

        public long TrailerId { get; set; }
        public IEnumerable<SelectListItem> Trailers { get; set; }

        public long CompanyId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }
    }
}
