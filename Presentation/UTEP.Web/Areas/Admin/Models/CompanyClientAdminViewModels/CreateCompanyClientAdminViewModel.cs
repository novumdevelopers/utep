﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Areas.Admin.Models.UserViewModels;

namespace UTEP.Web.Areas.Admin.Models.CompanyClientAdminViewModels
{
    public class CreateCompanyClientAdminViewModel
    {
        public long CompanyTypeId { get; set; }
        public IEnumerable<SelectListItem> CompanyTypes { get; set; }

        public CreateUserViewModel CreateUsers { get; set; }

        public string CompanyName { get; set; }
        [EmailAddress]
        public string CompanyEmail { get; set; }
        [Phone]
        public string CompanyPhone { get; set; }
        public string CompanyAddress { get; set; }
        public decimal LocationLatitude { get; set; }
        public decimal LocationLongitude { get; set; }
        public string VOEN { get; set; }

    }
}
