﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.VehicleMarkViewModels
{
    public class CreateVehicleMarkViewModel
    {
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public IFormFile Logo { get; set; }
    }
}
