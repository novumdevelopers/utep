﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.VehicleMarkViewModels
{
    public class EditVehicleMarkViewModel
    {
        public long Id { get; set; }
        public string LogoPath { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public string Name { get; set; }
        public IFormFile Logo { get; set; }
    }
}
