﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.CooperationViewModels
{
    public class EditCooperationViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public IFormFile Logo { get; set; }
        public string LogoForView { get; set; }
    }
}
