﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.UserTypeViewModels
{
    public class EditUserTypeViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
