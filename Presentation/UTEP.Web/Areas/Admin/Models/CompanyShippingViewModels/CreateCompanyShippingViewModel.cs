﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;

namespace UTEP.Web.Areas.Admin.Models.CompanyShippingViewModels
{
    public class CreateCompanyShippingViewModel
    {
        public long CompanyOrderId { get; set; }
        public IEnumerable<SelectListItem> CompanyOrders { get; set; }

        public decimal OriginTownLongitude { get; set; }
        public decimal OriginTownLatitude { get; set; }
        public decimal DestinationTownLongitude { get; set; }
        public decimal DestinationTownLatitude { get; set; }
        public virtual DateTime Deadline { get; set; }
    }
}
