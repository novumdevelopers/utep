﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.DeliveryViewModels
{
    public class EditDeliveryViewModel
    {
        public long Id { get; set; }
        public long CompanyOrderId { get; set; }
        public IEnumerable<SelectListItem> CompanyOrders { get; set; }
    }
}
