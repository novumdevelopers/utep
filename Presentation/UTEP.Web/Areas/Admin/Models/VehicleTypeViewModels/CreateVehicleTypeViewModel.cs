﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;

namespace UTEP.Web.Areas.Admin.Models.VehicleTypeViewModels
{
    public class CreateVehicleTypeViewModel
    {
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        public IEnumerable<SiteLanguageViewModel> SiteLanguages { get; set; }
    }
}
