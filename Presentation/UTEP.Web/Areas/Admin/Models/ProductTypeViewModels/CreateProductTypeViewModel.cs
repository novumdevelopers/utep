﻿using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;

namespace UTEP.Web.Areas.Admin.Models.ProductTypeViewModels
{
    public class CreateProductTypeViewModel
    {
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        //public FormFile Image { get; set; }
        public IEnumerable<SiteLanguageViewModel> SiteLanguages { get; set; }
    }
}
