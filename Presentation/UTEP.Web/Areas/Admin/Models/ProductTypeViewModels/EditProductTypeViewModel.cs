﻿using Microsoft.AspNetCore.Http.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Web.Areas.Admin.Models.SiteLanguageViewModels;

namespace UTEP.Web.Areas.Admin.Models.ProductTypeViewModels
{
    public class EditProductTypeViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        //public IFormFile Image { get; set; }
        public string ImageForView { get; set; }
        public IEnumerable<SiteLanguageViewModel> SiteLanguages { get; set; }
    }
}
