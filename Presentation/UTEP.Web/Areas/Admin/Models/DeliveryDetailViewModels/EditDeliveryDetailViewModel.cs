﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.DeliveryDetailViewModels
{
    public class EditDeliveryDetailViewModel
    {
        public long Id { get; set; }

        public long DeliveryId { get; set; }
        public IEnumerable<SelectListItem> Deliveries { get; set; }

        public decimal DestinationTownLongitude { get; set; }
        public decimal DestinationTownLatitude { get; set; }
        public DateTime? FinishDate { get; set; }
    }
}
