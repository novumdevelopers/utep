﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.CompanyTypeViewModels
{
    public class EditCompanyTypeViewModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage ="Doldurulmalıdır.",AllowEmptyStrings = false)]
        public string Name { get; set; }
    }
}
