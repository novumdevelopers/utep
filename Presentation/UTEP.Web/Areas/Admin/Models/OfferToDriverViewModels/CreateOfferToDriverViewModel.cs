﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.OfferToDriverViewModels
{
    public class CreateOfferToDriverViewModel
    {
        public long DriverId { get; set; }
        public IEnumerable<SelectListItem> Drivers { get; set; }

        public long CompanyOrderId { get; set; }
        public IEnumerable<SelectListItem> CompanyOrders { get; set; }
    }
}
