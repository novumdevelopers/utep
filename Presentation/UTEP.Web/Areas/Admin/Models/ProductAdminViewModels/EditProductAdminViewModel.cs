﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;

namespace UTEP.Web.Areas.Admin.Models.ProductAdminViewModels
{
    public class EditProductAdminViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Size { get; set; }
        public decimal Tonnage { get; set; }
        public int PaletteCount { get; set; }
        public int Weight { get; set; }
        public int WeightType { get; set; }

        public long ProducTypeId { get; set; }
        public IEnumerable<SelectListItem> ProductTypes { get; set; }

        public long CompanyId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }
        public List<ProductImage> ProductImages { get; set; }
    }
}
