﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UTEP.Web.Areas.Admin.Models.UserGroupViewModels
{
    public class EditUserGroupViewModel
    {
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<SelectListItem> UserGroupClaims { get; set; }
    }
}