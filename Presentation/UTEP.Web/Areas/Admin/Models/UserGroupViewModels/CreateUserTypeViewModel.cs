﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace UTEP.Web.Areas.Admin.Models.UserGroupViewModels
{
    public class CreateUserGroupViewModel
    {
        [Required(ErrorMessage = "Boş qoymaq olmaz.")]
        public string Name { get; set; }
        public long ParentId { get; set; }
        public List<SelectListItem> UserGroups { get; set; }
    }
}
