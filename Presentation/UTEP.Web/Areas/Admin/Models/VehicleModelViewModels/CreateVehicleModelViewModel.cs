﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.VehicleModelViewModels
{
    public class CreateVehicleModelViewModel
    {
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long VehicleMarkId { get; set; }
        public IEnumerable<SelectListItem> VehicleMarks { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public long VehicleTypeId { get; set; }
        public IEnumerable<SelectListItem> VehicleTypes { get; set; }
    }
}
