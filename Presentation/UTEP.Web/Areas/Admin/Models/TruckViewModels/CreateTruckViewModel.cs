﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTEP.Web.Areas.Admin.Models.TruckViewModels
{
    public class CreateTruckViewModel
    {
        [Required(ErrorMessage ="Doldurulmalıdır.")]
        public DateTime YearOf { get; set; }
        [Required(ErrorMessage ="Doldurulmalıdır.")]
        public string RegistrationNumber { get; set; }
        [Required(ErrorMessage ="Doldurulmalıdır.")]
        public IFormFile RegistrationCertificate { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public int VehicleMarkId { get; set; }
        public IEnumerable<SelectListItem> VehicleMarks { get; set; }
        [Required(ErrorMessage = "Doldurulmalıdır.")]
        public int VehicleModelId { get; set; }
        public IEnumerable<SelectListItem> VehicleModels { get; set; }
    }
}
