﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Areas.CompanyProfile.Models.ProductViewModels;

namespace UTEP.Web.Areas.CompanyProfile.Models.CompanyOrderViewModels
{
    public class EditCompanyOrderViewModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Təklif edilən qiymət bölməsi boş ola bilməz!")]
        public decimal ProporsalPrice { get; set; }
        //public string From { get; set; }
        //public string To { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime DealDate { get; set; }
        public int Currency { get; set; }
        public int Weight { get; set; }
        public string FromDescription { get; set; }
        public string ToDescription { get; set; }

        public EditProductViewModel EditProduct { get; set; }

        [Required(ErrorMessage = "Başlanğıc Yeri bölməsi boş ola bilməz!")]
        public long FromCountryId { get; set; }
        public IEnumerable<SelectListItem> FromCountries { get; set; }
        public long FromCityId { get; set; }
        public IEnumerable<SelectListItem> FromCities { get; set; }


        [Required(ErrorMessage = "Təhvil Yeri bölməsi boş ola bilməz!")]
        public long ToCountryId { get; set; }
        public IEnumerable<SelectListItem> ToCountries { get; set; }
        public long ToCityId { get; set; }
        public IEnumerable<SelectListItem> ToCities { get; set; }

        public string Lang { get; set; }

    }
}
