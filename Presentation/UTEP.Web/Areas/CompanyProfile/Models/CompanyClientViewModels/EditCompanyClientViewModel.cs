﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Web.Areas.Admin.Models.UserViewModels;

namespace UTEP.Web.Areas.CompanyProfile.Models.CompanyClientViewModels
{
    public class EditCompanyClientAdminViewModel
    {
        public long Id { get; set; }

        public long CompanyTypeId { get; set; }
        public IEnumerable<SelectListItem> CompanyTypes { get; set; }

        public EditUserViewModel EditUser { get; set; }

        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyPhone { get; set; }
        public decimal LocationLatitude { get; set; }
        public decimal LocationLongitude { get; set; }
        public string Fax { get; set; }

        public int TypeOfCompany { get; set; }
    }
}
