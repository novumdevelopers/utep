﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using UTEP.Web.Areas.CompanyProfile.Models.CompanyOrderViewModels;

namespace UTEP.Web.Areas.CompanyProfile.Models.ProductViewModels
{
    public class EditProductViewModel
    {
        public EditProductViewModel()
        {
            ProductImages = new List<ProductImage>();
        }
        public long Id { get; set; }
        [Required(ErrorMessage = "Məhsulun adı bölməsi boş ola bilməz!")]
        public string Name { get; set; }
        //public decimal Size { get; set; }
        [Required(ErrorMessage = "Məhsulun ağırlığı bölməsi boş ola bilməz!")]
        public decimal Tonnage { get; set; }
        public int Weight { get; set; }
        public int WeightType { get; set; }
        [Required(ErrorMessage = "Məhsulun sayı bölməsi boş ola bilməz!")]
        public int PaletteCount { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Məhsulun tipi bölməsi boş ola bilməz!")]
        public long ProducTypeId { get; set; }
        public IEnumerable<SelectListItem> ProductTypes { get; set; }

        public long CompanyId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }

        public List<ProductImage> ProductImages { get; set; }

    }
}
