﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UTEP.Core.UnitOfWork.Persistence;
using UTEP.Data;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Settings;
using UTEP.Web.Controllers;
using UTEP.Web.Models.HomeViewModels;
using UTEP.Web.Services;

namespace UTEP.Web.Areas.CompanyProfile.Controllers
{
    [Area("CompanyProfile")]
    [Route("{lang:lang}/[area]/[controller]/[action]/{id?}")]
    public class HomeController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly UnitOfWork _unitOfWork;
        private readonly IFileProcessor _fileService;
        public HomeController(
            UserManager<User> userManager,
            DbContextOptions<UTEPDbContext> contextOptions,
            IFileProcessor fileService
            )
        {
            _userManager = userManager;
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _fileService = fileService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> OwnPage()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return RedirectToAction("Error", "Home", new { area = "" });
            }

            var company = _unitOfWork.Companies.GetWithUser(x => x.Status == Status.Active && x.UserId == user.Id);
            if (/*user.EmailConfirmed &&*/ company != null)
            {
                var model = new OwnPageViewModel
                {
                    CompanyAddress = company.Address,
                    LogoForView = company.Logo,
                    CompanyName = company.Name,
                    CompanyPhone = company.Phone,
                    ManagerFirstName = company.User.FirstName,
                    ManagerLastName = company.User.LastName
                };
                return View(model);
            };
            return RedirectToAction("Error", "Home", new { area = "" });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OwnPage(OwnPageViewModel model, IFormFile file)
        {
            var user = _userManager.Users.Include(x => x.Company).FirstOrDefault(x => x.UserName == User.Identity.Name);
            if (user == null)
            {
                return RedirectToAction("Error", "Home");
            }
            var company = _unitOfWork.Companies.GetWithUser(x => x.Status == Status.Active && x.UserId == user.Id);
            if (company != null)
            {
                user.Company.Phone = model.CompanyPhone;
                user.Company.Address = model.CompanyAddress;
                user.FirstName = model.ManagerFirstName;
                user.LastName = model.ManagerLastName;

                if (!string.IsNullOrEmpty(file?.Name) && file.Length > 0)
                {
                    var folder = $"\\company\\logo\\";
                    user.Company.Logo = await _fileService.UploadFileAsync(file, folder);
                }

                try
                {
                    await _userManager.UpdateAsync(user);
                    //_unitOfWork.Companies.Update(company);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return RedirectToAction("Index", "Home", new { area = "CompanyProfile" });
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Home", new { area = "" });
                }

                return View(model);
            };
            return RedirectToAction("Error", "Home");
        }


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}