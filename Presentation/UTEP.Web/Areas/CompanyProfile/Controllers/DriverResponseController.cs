﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.DriverProfile.Models.DriverResponseViewModels;
using UTEP.Data.Domain.Models.Tables.TrailerBasedGroup;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using UTEP.Data.Domain.Models.Tables.DriverBasedGroup;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using UTEP.Web.Controllers;

namespace UTEP.Web.Areas.CompanyrProfile.Controllers
{
    [Area("CompanyrProfile")]
    [Authorize(Roles = "Manager")]
    [Route("[area]/{lang:lang}/[controller]/[action]/{id?}")]
    public class DriverResponseController : BaseController
    {
        private readonly UnitOfWork _unitOfWork;
        public DriverResponseController(DbContextOptions<UTEPDbContext> contextOptions)
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult LoadData()
        {
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.DriverResponses.FindDataTable(x => x.Status == Status.Active, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        //[HttpGet]
        //public IActionResult Create()
        //{
        //    var model = new CreateDriverResponseViewModel
        //    {
        //        Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //        {
        //            Value = y.Id.ToString(),
        //            Text = y.User.ToString()
        //        }),
        //        Trucks = _unitOfWork.Trucks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //        {
        //            Value = y.Id.ToString(),
        //            Text = y.RegistrationNumber
        //        }),
        //        Trailers = _unitOfWork.Trailers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //        {
        //            Value = y.Id.ToString(),
        //            Text = y.RegistrationNumber
        //        }),
        //        CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //        {
        //            Value = y.Id.ToString(),
        //            Text = y.Product.Name
        //        })
        //    };
        //    return View(model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Create(CreateDriverResponseViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var DriverResponse = new DriverResponse
        //        {
        //            DriverId = model.DriverId,
        //            TruckId = model.TruckId,
        //            TrailerId = model.TrailerId,
        //            CompanyOrderId = model.CompanyOrderId,
        //            EndDate = model.EndDate,
        //            StartDate = model.StartDate,
        //            EstimatePrice = model.EstimatePrice,
        //            DayCount = model.DayCount,
        //        };
        //        _unitOfWork.DriverResponses.Add(DriverResponse);
        //        _unitOfWork.Complete();
        //        _unitOfWork.Dispose();
        //        return RedirectToAction("Index", new { id = "" });

        //    }
        //    model.Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.User.ToString()
        //    });
        //    model.Trucks = _unitOfWork.Trucks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.RegistrationNumber
        //    });
        //    model.Trailers = _unitOfWork.Trailers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.RegistrationNumber
        //    });
        //    model.CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.Product.Name
        //    });
        //    return View(model);
        //}

        //[HttpGet]
        //public IActionResult Edit(long id)
        //{
        //    var driverResponse = _unitOfWork.DriverResponses.Get(x => x.Id == id && x.Status == Status.Active);
        //    if (driverResponse != null)
        //    {
        //        var model = new EditDriverResponseViewModel
        //        {
        //            Id = driverResponse.Id,
        //            EndDate = driverResponse.EndDate,
        //            StartDate = driverResponse.StartDate,
        //            EstimatePrice = driverResponse.EstimatePrice,
        //            DayCount = driverResponse.DayCount,
        //            CompanyOrderId = driverResponse.CompanyOrderId,
        //            CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //            {
        //                Value = y.Id.ToString(),
        //                Text = y.Product.Name
        //            }),
        //            DriverId = driverResponse.DriverId,
        //            Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //            {
        //                Value = y.Id.ToString(),
        //                Text = y.User.FirstName + y.User.LastName
        //            }),
        //            TruckId = driverResponse.TruckId,
        //            Trucks = _unitOfWork.Trucks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //            {
        //                Value = y.Id.ToString(),
        //                Text = y.RegistrationNumber
        //            }),
        //            TrailerId = driverResponse.TrailerId,
        //            Trailers = _unitOfWork.Trailers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //            {
        //                Value = y.Id.ToString(),
        //                Text = y.RegistrationNumber
        //            })
        //        };
        //        return View(model);
        //    }
        //    return NotFound();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public IActionResult Edit(EditDriverResponseViewModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var driverResponse = _unitOfWork.DriverResponses.Get(x => x.Id == model.Id && x.Status == Status.Active);
        //        if (driverResponse != null)
        //        {
        //            driverResponse.EndDate = model.EndDate;
        //            driverResponse.StartDate = model.StartDate;
        //            driverResponse.DayCount = model.DayCount;
        //            driverResponse.EstimatePrice = model.EstimatePrice;

        //            driverResponse.DriverId = model.DriverId;
        //            driverResponse.TruckId = model.TruckId;
        //            driverResponse.TrailerId = model.TrailerId;
        //            driverResponse.CompanyOrderId = model.CompanyOrderId;
        //            driverResponse.CompanyOrderId = model.CompanyOrderId;
        //            try
        //            {
        //                _unitOfWork.DriverResponses.Update(driverResponse);
        //                _unitOfWork.Complete();
        //                _unitOfWork.Dispose();
        //                return RedirectToAction("Index", new { di = "" });
        //            }
        //            catch (Exception)
        //            {
        //                return RedirectToAction("Error", "Home", new { area = " " });
        //            }
        //        }
        //    }
        //    model.Drivers = _unitOfWork.Drivers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.User.ToString()
        //    });
        //    model.Trucks = _unitOfWork.Trucks.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.RegistrationNumber
        //    });
        //    model.Trailers = _unitOfWork.Trailers.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.RegistrationNumber
        //    });
        //    model.CompanyOrders = _unitOfWork.CompanyOrders.Find(x => x.Status == Status.Active).Select(y => new SelectListItem
        //    {
        //        Value = y.Id.ToString(),
        //        Text = y.ProporsalPrice.ToString()
        //    });
        //    return View(model);
        //}

    }
}

