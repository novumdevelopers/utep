﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTEP.Web.Areas.CompanyProfile.Models.CompanyOrderViewModels;
using UTEP.Core.UnitOfWork.Persistence;
using Microsoft.EntityFrameworkCore;
using UTEP.Data;
using UTEP.Settings;
using UTEP.Web.Attribute;
using Microsoft.AspNetCore.Mvc.Rendering;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;
using UTEP.Web.Areas.CompanyProfile.Models.ProductViewModels;
using UTEP.Data.Domain.Models.Tables.ProductBasedGroup;
using Microsoft.AspNetCore.Identity;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using Microsoft.AspNetCore.Http;
using UTEP.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using UTEP.Web.Areas.CompanyProfile.Models;
using System.IO;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Drawing.Imaging;
using System.Drawing;
using Microsoft.Extensions.Localization;
using UTEP.Web.Controllers;

namespace UTEP.Web.Areas.CompanyProfile.Controllers
{
    [Area("CompanyProfile")]
    [Authorize(Roles = "Manager")]
    [Route("{lang:lang}/[area]/[controller]/[action]/{id?}")]
    public class CompanyOrderController : BaseController
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly IStringLocalizer<CompanyOrderController> _localizer;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IEmailSender _emailSender;
        private readonly IFileProcessor _fileProcessor;
        public CompanyOrderController(
            DbContextOptions<UTEPDbContext> contextOptions,
            UserManager<User> userManager,
            IStringLocalizer<CompanyOrderController> localizer,
            IHostingEnvironment hostingEnvironment,
            IFileProcessor fileProcessor,
            IEmailSender emailSender
            )
        {
            _unitOfWork = new UnitOfWork(new UTEPDbContext(contextOptions));
            _localizer = localizer;
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
            _fileProcessor = fileProcessor;
            _emailSender = emailSender;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["success"] = TempData["success"];
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public async Task<IActionResult> LoadData()
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            var user = await _userManager.GetUserAsync(User);

            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["Order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["Order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            var list = _unitOfWork.CompanyOrders.FindDataTable(x => x.Status == Status.Active && x.Products.FirstOrDefault().Company.UserId == user.Id, lang, skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var lang = RouteData.Values["lang"].ToString();
            //var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Company.Id == 2);  
            var user = await _userManager.GetUserAsync(User);
            var companyId = _unitOfWork.Companies.Get(x => x.UserId == user.Id).Id;
            var model = new CreateCompanyOrderViewModel
            {
                CreateProduct = new CreateProductViewModel
                {
                    CompanyId = companyId,
                    ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    }),
                    PaletteCount = 1
                },
                FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
                ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                }),
                Lang = lang
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateCompanyOrderViewModel model, IFormCollection files)
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            if (ModelState.IsValid)
            {
                if (files.Files.Count > 6)
                {
                    ModelState.AddModelError("", _localizer["6images_validate"]);
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if (files.Files.Count == 0)
                {
                    ModelState.AddModelError("", _localizer["image_required"]);
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if (model.DealDate > model.DeadLine)
                {
                    ModelState.AddModelError("", _localizer["start_end_validate"]);
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }

                var from = _unitOfWork.Cities.Get(x => x.Status == Status.Active && x.Id == model.FromCityId).Name;
                var fromCountry = _unitOfWork.Countries.Get(x => x.Status == Status.Active && x.Id == model.FromCountryId).Name;
                var to = _unitOfWork.Cities.Get(x => x.Status == Status.Active && x.Id == model.ToCityId).Name;
                var toCountry = _unitOfWork.Countries.Get(x => x.Status == Status.Active && x.Id == model.ToCountryId).Name;

                var shipping = new List<CompanyShipping>{
                    new CompanyShipping
                    {
                        Deadline = model.DeadLine
                    }};

                var companyOrder = new CompanyOrder
                {
                    DealDate = model.DealDate,
                    ProporsalPrice = model.ProporsalPrice,
                    OriginTownLatitude = 0m,
                    OriginTownLongitude = 0m,
                    DestinationTownLatitude = 0m,
                    DestinationTownLongitude = 0m,
                    From = from,
                    CountryFrom = fromCountry,
                    FromDescription = model.FromDescription,
                    To = to,
                    CountryTo = toCountry,
                    ToDescription = model.ToDescription,
                    Status = Status.Waited,
                    Currency = model.Currency,
                    CompanyShippings = shipping
                };

                var product = new Product
                {
                    CompanyId = model.CreateProduct.CompanyId,
                    ProductTypeId = model.CreateProduct.ProducTypeId,
                    Name = model.CreateProduct.Name,
                    Size = 0.00m,
                    Tonnage = model.CreateProduct.Tonnage,
                    PaletteCount = model.CreateProduct.PaletteCount,
                    Weight = model.Weight,
                    WeightType = model.WeightType,
                    Description = model.CreateProduct.Description,
                    CompanyOrder = companyOrder
                };

                foreach (var item in files.Files)
                {
                    if (!string.IsNullOrEmpty(item?.Name) && item.Length > 0)
                    {
                        var folderMain = $"company\\product\\";
                        var folderMed = $"company\\product\\medium\\";
                        var fileName = await _fileProcessor.UploadFileAsync(item, folderMain);
                        var fileDirectory = _fileProcessor.GetPathAndFileName(fileName, folderMain);
                        var watermarkText = "UTEP.az";
                        var writenMain = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMain);
                        var writenMed = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMed);
                        if (writenMed && writenMain)
                        {
                            var fullFilename = watermarkText + Path.GetFileName(fileDirectory);
                            var newfileDirectory = _fileProcessor.GetPathAndFileName(fullFilename, folderMed);
                            var mediumPath = _fileProcessor.CreateThumbnailCentered(/*270, 142,*/ 338, 145, newfileDirectory, folderMed, watermarkText);

                            product.ProductImages.Add(new ProductImage()
                            {
                                MainPath = fullFilename,
                                MediumPath = mediumPath
                            });
                        }
                    }
                }

                if (!product.ProductImages.Any())
                {
                    ModelState.AddModelError("", _localizer["problem_in_image"]);
                    model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }

                var user = await _userManager.GetUserAsync(User);
                var admins = _userManager.Users.Include(x => x.Roles).Where(x => x.Roles.FirstOrDefault().RoleId == 1); //"nicathidayetzade@gmail.com"; //"huseyn.hajiyev.aydin@gmail.com";
                try
                {
                    var callbackUrl = Url.Action(nameof(Waited), "CompanyOrder", new { area = "CompanyProfile" }, protocol: HttpContext.Request.Scheme);
                    var messageForClient = string.Format(_localizer["message_for_client"], model.CreateProduct.Name, callbackUrl);
                    await _emailSender.SendEmailAsync(user.Email, _localizer["checking_announce"], messageForClient);

                    var callbackUrlForAdmin = Url.Action(nameof(Waited), "CompanyOrderAdmin", new { area = "Admin" }, protocol: HttpContext.Request.Scheme);
                    var messageForAdmin = $"<!DOCTYPE html><html lang='az'><head> <meta charset='UTF-8'> <title>UTEP</title></head><body style='text-align:center;'> <table> <tr> <td align='center'> <img src='" + Request.Scheme + "://" + Request.Host + "/v2/img/utep-logo.png' alt='UTEP' style='width:60px; height:60px;'> </td></tr><tr> <td style='font-size: 18px'> <h3 align='center'> Yeni Sifariş </h3> <p> '{model.CreateProduct.Name}' adlı yeni sifariş əlavə edildi. Sifarişinizə baxmaq üçün <a href='{callbackUrlForAdmin}'>istinadla</a> keçin. </p><p> Unutmayın, siz istənilən zaman <a href='mailto:support@utep.az'>support@utep.az</a> emaili vasitəsilə bizə müraciət edə bilərsiniz. Biz UTEP.az servisi haqda sizin bütün suallarınıza cavab verməyə hazırıq. </p><br><hr> </td></tr><tr> <td align='center' style='font-size: 14px'> <p> Cəfər Cabbarlı küçəsi, 44, Bakı, Azərbaycan </p><p> <a href='tel:+994777779696'> +994 77 777 96 96</a> <br><a href='mailto:support@utep.az'>support@utep.az</a> </p></td></tr></table></body></html>";
                    foreach (var item in admins)
                    {
                        if (item.Email != "nicathidayetzade@gmail.com")
                        {
                            await _emailSender.SendEmailAsync(item.Email, "Yeni elan yükləndi", messageForAdmin);
                        }
                    }

                    _unitOfWork.Products.Add(product);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    TempData["success"] = "Success";
                    return RedirectToAction(nameof(Index), new { id = "" });
                }
                catch (Exception)
                {
                    throw;
                }
            }
            model.CreateProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
            });

            model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });

            model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(long id)
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            //var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Company.Id == 2);
            var user = await _userManager.GetUserAsync(User);
            var companyId = _unitOfWork.Companies.Get(x => x.Status == Status.Active && x.UserId == user.Id).Id;

            var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Id == id && x.Status == Status.Active);
            if (companyOrder != null)
            {
                var fromCity = companyOrder.From;
                var fromCityId = _unitOfWork.Cities.Get(x => x.Name == fromCity && x.Status == Status.Active).Id;
                var fromCountryId = _unitOfWork.Cities.Get(x => x.Name == fromCity && x.Status == Status.Active).CountryId;

                var toCity = companyOrder.To;
                var toCityId = _unitOfWork.Cities.Get(x => x.Name == toCity && x.Status == Status.Active).Id;
                var toCountryId = _unitOfWork.Cities.Get(x => x.Name == toCity && x.Status == Status.Active).CountryId;

                var model = new EditCompanyOrderViewModel
                {
                    Id = companyOrder.Id,
                    ProporsalPrice = companyOrder.ProporsalPrice,
                    DeadLine = companyOrder.CompanyShippings.FirstOrDefault().Deadline,
                    DealDate = companyOrder.DealDate,
                    FromDescription = companyOrder.FromDescription,
                    ToDescription = companyOrder.ToDescription,
                    Currency = companyOrder.Currency,
                    Weight = companyOrder.Products.First().Weight,
                    EditProduct = new EditProductViewModel
                    {
                        CompanyId = companyId,
                        ProducTypeId = companyOrder.Products.First(x => x.Status == Status.Active).ProductTypeId,
                        Name = companyOrder.Products.First(x => x.Status == Status.Active).Name,
                        //Size = companyOrder.Products.First(x => x.Status == Status.Active).Size,
                        Tonnage = companyOrder.Products.First(x => x.Status == Status.Active).Tonnage,
                        Weight = companyOrder.Products.First(x => x.Status == Status.Active).Weight,
                        WeightType = companyOrder.Products.First(x => x.Status == Status.Active).WeightType,
                        PaletteCount = companyOrder.Products.First(x => x.Status == Status.Active).PaletteCount,
                        ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == "az").Name
                        }),
                        ProductImages = companyOrder.Products.First(x => x.Status == Status.Active).ProductImages.Where(x => x.Status == Status.Active).ToList(),
                        Description = companyOrder.Products.First(x => x.Status == Status.Active).Description
                    },
                    FromCountryId = fromCountryId,
                    FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),
                    FromCityId = fromCityId,
                    FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == fromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),

                    ToCountryId = toCountryId,
                    ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),
                    ToCityId = toCityId,
                    ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == toCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }),
                    Lang = lang

                };

                return View(model);
            }
            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EditCompanyOrderViewModel model, IFormCollection collection)
        {
            var lang = RouteData.Values["lang"].ToString() ?? "az";
            if (ModelState.IsValid)
            {
                if (collection.Files.Count == 0 && model.EditProduct.ProductImages.Count == 0)
                {
                    ModelState.AddModelError("", _localizer["image_required"]);
                    model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if ((collection.Files.Count + model.EditProduct.ProductImages.Count) > 6)
                {
                    ModelState.AddModelError("", _localizer["6images_validate"]);
                    model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }
                if (model.DealDate > model.DeadLine)
                {
                    ModelState.AddModelError("", _localizer["start_end_validate"]);
                    model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                    {
                        Value = x.Id.ToString(),
                        Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                    });

                    model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });

                    model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    });
                    return View(model);
                }

                var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Id == model.Id && x.Status == Status.Active);
                var from = _unitOfWork.Cities.Get(x => x.Id == model.FromCityId && x.Status == Status.Active).Name;
                var fromCountry = _unitOfWork.Countries.Get(x => x.Id == model.FromCountryId && x.Status == Status.Active).Name;
                var to = _unitOfWork.Cities.Get(x => x.Id == model.ToCityId && x.Status == Status.Active).Name;
                var toCountry = _unitOfWork.Countries.Get(x => x.Id == model.ToCountryId && x.Status == Status.Active).Name;

                if (companyOrder != null)
                {
                    companyOrder.CompanyShippings.First().Deadline = model.DeadLine;
                    companyOrder.Currency = model.Currency;
                    companyOrder.DealDate = model.DealDate;
                    companyOrder.ProporsalPrice = model.ProporsalPrice;
                    companyOrder.From = from;
                    companyOrder.CountryFrom = fromCountry;
                    companyOrder.FromDescription = model.FromDescription;
                    companyOrder.To = to;
                    companyOrder.CountryTo = toCountry;
                    companyOrder.ToDescription = model.ToDescription;
                    companyOrder.Products.First(x => x.Status == Status.Active).Weight = model.Weight;
                    companyOrder.Products.First(x => x.Status == Status.Active).WeightType = model.EditProduct.WeightType;
                    companyOrder.Products.First(x => x.Status == Status.Active).CompanyId = model.EditProduct.CompanyId;
                    companyOrder.Products.First(x => x.Status == Status.Active).ProductTypeId = model.EditProduct.ProducTypeId;
                    companyOrder.Products.First(x => x.Status == Status.Active).Name = model.EditProduct.Name;
                    //companyOrder.Products.First(x => x.Status == Status.Active).Size = model.EditProduct.Size;
                    companyOrder.Products.First(x => x.Status == Status.Active).Tonnage = model.EditProduct.Tonnage;
                    companyOrder.Products.First(x => x.Status == Status.Active).PaletteCount = model.EditProduct.PaletteCount;
                    companyOrder.Products.First(x => x.Status == Status.Active).Description = model.EditProduct.Description;

                    //var currentImages = model.EditProduct.ProductImages;
                    //List<ProductImage> images = companyOrder.Products.FirstOrDefault().ProductImages.ToList();

                    //for (int i = 0; i < images.Count(); i++)
                    //{
                    //    if (images[i].Id != currentImages[i].Id)
                    //    {
                    //        images[i].Status = Status.Deleted;
                    //    }
                    //}

                    foreach (var item in collection.Files)
                    {
                        if (!string.IsNullOrEmpty(item?.Name) && item.Length > 0)
                        {
                            var folderMain = $"company\\product\\";
                            var folderMed = $"company\\product\\medium\\";
                            var fileName = await _fileProcessor.UploadFileAsync(item, folderMain);
                            var fileDirectory = _fileProcessor.GetPathAndFileName(fileName, folderMain);
                            var watermarkText = "UTEP.az";
                            var writenMain = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMain);
                            var writenMed = _fileProcessor.ApplyWatermark(fileDirectory, watermarkText, folderMed);
                            if (writenMed && writenMain)
                            {
                                var fullFilename = watermarkText + Path.GetFileName(fileDirectory);
                                var newfileDirectory = _fileProcessor.GetPathAndFileName(fullFilename, folderMed);
                                var mediumPath = _fileProcessor.CreateThumbnailCentered(/*270, 142,*/ 338, 145, newfileDirectory, folderMed, watermarkText);

                                companyOrder.Products.First().ProductImages.Add(new ProductImage()
                                {
                                    MainPath = fullFilename,
                                    MediumPath = mediumPath
                                });
                            }
                        }
                    }


                    if (!companyOrder.Products.First().ProductImages.Any())
                    {
                        ModelState.AddModelError("", _localizer["problem_in_image"]);
                        model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
                        {
                            Value = x.Id.ToString(),
                            Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
                        });

                        model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });
                        model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });

                        model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });
                        model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
                        {
                            Text = x.Name,
                            Value = x.Id.ToString()
                        });
                        return View(model);
                    }

                    try
                    {
                        _unitOfWork.CompanyOrders.Update(companyOrder);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return RedirectToAction(nameof(Index), new { id = "" });
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            model.EditProduct.ProductTypes = _unitOfWork.ProductTypes.Find(x => x.Status == Status.Active).Select(x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.ProductTypeTranslations.First(y => y.SiteLanguage.LanguageCode == lang).Name
            });

            model.FromCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.FromCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.FromCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });

            model.ToCountries = _unitOfWork.Countries.Find(x => x.Status == Status.Active).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            model.ToCities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == model.ToCountryId).OrderBy(x => x.Name).Select(x => new SelectListItem
            {
                Text = x.Name,
                Value = x.Id.ToString()
            });
            return View(model);
        }

        [HttpPost]
        [AjaxOnly]
        public IActionResult TrashImage(long id)
        {
            var productId = _unitOfWork.ProductImages.Get(x => x.Id == id && x.Status == Status.Active).ProductId;
            //var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Company.Id == 2);
            var companyOrder = _unitOfWork.CompanyOrders.GetWithProduct(x => x.Products.FirstOrDefault(y => y.Id == productId).Id == productId);
            if (companyOrder != null)
            {
                var image = companyOrder.Products.FirstOrDefault().ProductImages.FirstOrDefault(x => x.Id == id);
                if (image != null)
                {
                    image.Status = Status.Deleted;
                    try
                    {
                        _unitOfWork.ProductImages.Update(image);
                        _unitOfWork.Complete();
                        _unitOfWork.Dispose();
                        return Json(new { Status = true, Message = "Success!" });
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }
            }
            return Json(new { Status = false, Message = "Failed!" });
        }


        [HttpGet]
        public IActionResult Trash()
        {
            return View();
        }

        [HttpPost]
        [AjaxOnly]
        public async Task<IActionResult> TrashData()
        {
            var user = await _userManager.GetUserAsync(User);
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["Order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["Order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyOrders.FindDataTable(x => x.Status == Status.End && x.Products.FirstOrDefault().Company.UserId == user.Id, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Trash(long id)
        {
            var order = _unitOfWork.CompanyOrders.Get(x => x.Status == Status.Active && x.Id == id);
            if (order != null)
            {
                order.Status = Status.End;
                try
                {
                    _unitOfWork.CompanyOrders.Update(order);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new { Message = "Success", Status = true });
                }
                catch (Exception)
                {
                    return Json(new { Message = "Error", Status = false });
                }
            }
            return Json(new { Message = "Error", Status = false });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Back(long id)
        {
            var order = _unitOfWork.CompanyOrders.Get(x => x.Status == Status.End && x.Id == id);
            if (order != null)
            {
                order.Status = Status.Active;
                try
                {
                    _unitOfWork.CompanyOrders.Update(order);
                    _unitOfWork.Complete();
                    _unitOfWork.Dispose();
                    return Json(new { Message = "Success", Status = true });
                }
                catch (Exception)
                {
                    return Json(new { Message = "Error", Status = false });
                }
            }
            return Json(new { Message = "Error", Status = false });
        }


        [HttpGet]
        public IActionResult Waited()
        {
            return View();
        }


        [HttpPost]
        [AjaxOnly]
        public async Task<IActionResult> WaitedData()
        {
            var user = await _userManager.GetUserAsync(User);
            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["Order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["Order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;

            var list = _unitOfWork.CompanyOrders.FindDataTable(x => x.Status == Status.Waited && x.Products.FirstOrDefault().Company.UserId == user.Id, "az", skip, pageSize, sortColumn, sortColumnDirection, searchValue);
            return Json(new { draw = draw, recordsFiltered = list.RecordsFiltered, recordsTotal = list.RecordsTotal, data = list.Data });

        }



        public IActionResult GetCities(long id)
        {
            var country = _unitOfWork.Countries.Get(x => x.Id == id && x.Status == Status.Active);
            if (country != null)
            {
                var list = new List<SelectListItem>();

                var cities = _unitOfWork.Cities.Find(x => x.Status == Status.Active && x.CountryId == country.Id).OrderBy(x => x.Name).Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                });
                if (cities != null)
                {
                    list.AddRange(cities);
                    return Json(list);
                }
            }
            return View("Error", "Home");
        }


    }
}


