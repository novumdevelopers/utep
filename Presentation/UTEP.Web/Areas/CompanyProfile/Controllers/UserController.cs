﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UTEP.Data.Domain.Models.Tables.UserBasedGroup;
using UTEP.Web.Controllers;
using UTEP.Web.Models.ManageViewModels;

namespace UTEP.Web.Areas.CompanyProfile.Controllers
{
    [Authorize(Roles = "Manager")]
    [Area("CompanyProfile")]
    [Route("{lang:lang}/[area]/[controller]/[action]")]
    public class UserController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserController(
            UserManager<User> userManager,
            SignInManager<User> signInManager
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user != null)
                {
                    var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignOutAsync();
                        return RedirectToAction(nameof(AccountController.Login), "Account", new { area = "" });
                    }
                }
            }
            return RedirectToAction("Error", "Home");
        }
    }
}