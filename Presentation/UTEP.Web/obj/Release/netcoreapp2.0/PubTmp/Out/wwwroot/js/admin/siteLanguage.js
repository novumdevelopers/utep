﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    $("#sitelanguageIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/admin/" + lang + "/SiteLanguage/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [3],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "Name", name: "Name" },
            { data: "Code", name: "Code" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/sitelanguage/edit/" +
                        data.Id +
                        "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
                }
            }
        ],
        "order": [[2, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,

    });
});
