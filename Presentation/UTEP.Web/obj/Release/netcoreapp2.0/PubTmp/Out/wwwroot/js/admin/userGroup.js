﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    $("#usergroupIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/usergroup/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [2],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "Name", name: "Name" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/usergroup/edit/" +
                        data.Id +
                        "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
                }
            }
        ],
        "order": [[1, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,

    });
});