﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    $("#productIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/companyprofile/" + lang + "/product/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [13],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "Name", name: "Name" },
            { data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "PaletteCount", name: "PaletteCount" },
            {
                data: function (data) {
                    return (data.Company.Status == 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
                },
                name: "Company"
            },
            {
                data: function (data) {
                    return (data.ProductType.Status == 2) ? data.ProductType.Name + " <i class='fa fa-times text-danger'></i>" : data.ProductType.Name;
                },
                name: "ProductType"
            },
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "OriginTownLatitude", name: "OriginTownLatitude" },
            { data: "OriginTownLongitude", name: "OriginTownLongitude" },
            { data: "DestinationTownLatitude", name: "DestinationTownLatitude" },
            { data: "DestinationTownLongitude", name: "DestinationTownLongitude" },
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/companyprofile/" + lang + "/product/edit/" +
                        data.Id +
                        "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
                }
            }
        ],
        "order": [[12, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,

    });
});
