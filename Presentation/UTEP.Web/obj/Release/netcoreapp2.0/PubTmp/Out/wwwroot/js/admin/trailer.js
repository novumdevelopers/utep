﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    var tableIndex = $("#trailerIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/Trailer/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [6],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "Tonnage", name: "Tonnage" },
            { data: "PaletteCount", name: "PaletteCount" },
            { data: "RegistrationNumber", name: "RegistrationNumber" },
            {
                data: function (data) {
                    return (data.TrailerType.Status == 2) ? data.TrailerType.Name + " <i class='fa fa-times text-danger'></i>" : data.TrailerType.Name;
                },
                name: "TrailerType"
            },
            {
                data: function (data) {
                    return (data.VehicleModel.Status == 2) ? data.VehicleModel.Name + " <i class='fa fa-times text-danger'></i>" : data.VehicleModel.Name;
                },
                name: "VehicleModel"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/Trailer/edit/" +
                        data.Id +
                        "' class='btn btn-outline blue btn-xs'>  <i class='fa fa-edit'></i> Edit </a>" +
                        "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs delete-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> Sil</a>";
                }
            }
        ],
        "order": [[5, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,

    });

    var tableTrash = $("#trailerTrashTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/Trailer/TrashData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [6],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "Tonnage", name: "Tonnage" },
            { data: "PaletteCount", name: "PaletteCount" },
            { data: "RegistrationNumber", name: "RegistrationNumber" },
            {
                data: function (data) {
                    return (data.TrailerType.Status == 2) ? data.TrailerType.Name + " <i class='fa fa-times text-danger'></i>" : data.TrailerType.Name;
                },
                name: "TrailerType"
            },
            {
                data: function (data) {
                    return (data.VehicleModel.Status == 2) ? data.VehicleModel.Name + " <i class='fa fa-times text-danger'></i>" : data.VehicleModel.Name;
                },
                name: "VehicleModel"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Geri Qaytar</a>";
                }
            }
        ],
        "order": [[5, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,

    });

    tableIndex.on('click',
        '.delete-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/Trailer/Trash/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableIndex.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableTrash.on('click',
        '.back-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/Trailer/Back/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableTrash.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });
});