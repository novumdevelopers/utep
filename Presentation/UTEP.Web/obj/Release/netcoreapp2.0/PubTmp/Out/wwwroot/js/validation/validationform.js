﻿$(document).ready(function () {
    $('#btn-validate').click(function () {
        if ($(this.form).valid()) {
            this.disabled = 'disabled';
            $(this.form).submit();
        }
    });

    $('#btn-validate-1').click(function () {
        if ($(this.form).valid()) {
            this.disabled = 'disabled';
            $(this.form).submit();
        }
    });

    $('#btn-validate-loading').click(function () {
        if ($(this.form).valid()) {
            this.disabled = 'disabled';
            $(this).html('Loading...');
            $(this.form).submit();
        }
    });

    $('#btn-validate-spinner').click(function () {
        if ($(this.form).valid()) {
            this.disabled = 'disabled';
            $(this).html('<i class="fa fa-spinner fa-pulse" style="font-size:16px"></i>');
            $(this.form).submit();
        }
    });
});