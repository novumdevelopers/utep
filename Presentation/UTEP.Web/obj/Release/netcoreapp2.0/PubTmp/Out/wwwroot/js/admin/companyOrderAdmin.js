﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    var tableIndex = $("#companyorderadminIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/admin/" + lang + "/CompanyOrderAdmin/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [12],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "Currency", name: "Currency" },
            { data: "From", name: "From" },
            { data: "To", name: "To" },
            { data: "Name", name: "Name" },
            //{ data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "Weight", name: "Weight" },
            { data: "PaletteCount", name: "PaletteCount" },
            {
                data: function (data) {
                    return (data.Company.Status === 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
                },
                name: "Company"
            },
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.DeadLine);
                },
                name: "DeadLine"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/companyorderadmin/edit/" +
                        data.Id +
                        "' class='btn btn-outline blue btn-xs'>  <i class='fa fa-edit'></i> Edit </a>" +
                        "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs delete-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> Sil</a>";
                }
            }
        ],
        "order": [[11, "desc"]],
        "scrollX": true,
        "scrollCollapse": true
    });

    var tableTrash = $("#companyorderadminTrashTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/admin/" + lang + "/companyorderadmin/TrashData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [10],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "From", name: "From" },
            { data: "To", name: "To" },
            { data: "Name", name: "Name" },
            //{ data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "PaletteCount", name: "PaletteCount" },
            {
                data: function (data) {
                    return (data.Company.Status === 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
                },
                name: "Company"
            },
            //{
            //    data: function (data) {
            //        return (data.ProductType.Status === 2) ? data.ProductType.Name + " <i class='fa fa-times text-danger'></i>" : data.ProductType.Name;
            //    },
            //    name: "ProductType"
            //},
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.DeadLine);
                },
                name: "DeadLine"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Geri Qaytar</a>";
                }
            }
        ],
        "order": [[9, "desc"]],
        "scrollX": true,
        "scrollCollapse": true
    });
    
    var tableTrashbyCompany = $("#companyorderadminTrashTablebyCompany").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/admin/" + lang + "/companyorderadmin/TrashDatabyCompany",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [10],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "From", name: "From" },
            { data: "To", name: "To" },
            { data: "Name", name: "Name" },
            //{ data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "PaletteCount", name: "PaletteCount" },
            {
                data: function (data) {
                    return (data.Company.Status === 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
                },
                name: "Company"
            },
            //{
            //    data: function (data) {
            //        return (data.ProductType.Status === 2) ? data.ProductType.Name + " <i class='fa fa-times text-danger'></i>" : data.ProductType.Name;
            //    },
            //    name: "ProductType"
            //},
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.DeadLine);
                },
                name: "DeadLine"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Geri Qaytar</a>";
                }
            }
        ],
        "order": [[9, "desc"]],
        "scrollX": true,
        "scrollCollapse": true

    });

    var tableWaited = $("#companyorderadminWaitedTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "ajax": {
            "url": "/admin/" + lang + "/CompanyOrderAdmin/WaitedData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [10],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "ProporsalPrice", name: "ProporsalPrice" },
            { data: "From", name: "From" },
            { data: "To", name: "To" },
            { data: "Name", name: "Name" },
            //{ data: "Size", name: "Size" },
            { data: "Tonnage", name: "Tonnage" },
            { data: "PaletteCount", name: "PaletteCount" },
            {
                data: function (data) {
                    return (data.Company.Status === 2) ? data.Company.Name + " <i class='fa fa-times text-danger'></i>" : data.Company.Name;
                },
                name: "Company"
            },
            {
                data: function (data) {
                    return formatDate(data.DealDate);
                },
                name: "DealDate"
            },
            {
                data: function (data) {
                    return formatDate(data.DeadLine);
                },
                name: "DeadLine"
            },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs waited-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-check'></i> Aktiv Et</a>";
                }
            }
        ],
        "order": [[9, "desc"]],
        "scrollX": true,
        "scrollCollapse": true

    });



    tableIndex.on('click',
        '.delete-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/companyorderadmin/Trash/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableIndex.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableTrash.on('click',
        '.back-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/companyorderadmin/Back/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableTrash.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableTrashbyCompany.on('click',
        '.back-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/companyorderadmin/BackbyCompany/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableTrash.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableWaited.on('click',
        '.waited-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/companyorderadmin/WaitedToActive/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableWaited.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

});
