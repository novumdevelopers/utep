﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    $("#adminIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/user/AdminData",
            "type": "POST",
            "datatype": "json"
        },
        //"columnDefs":
        //    [{
        //        "targets": [2],
        //        "visible": true,
        //        "searchable": false,
        //        "orderable": false
        //    }],
        "columns": [
            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" }
            //{
            //    data: function (data) {
            //        return "<a href='/admin/" + lang + "/user/edit/" +
            //            data.Id +
            //            "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
            //    }
            //}
        ],
        "order": [[1, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });


    $("#managerIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/user/ManagerData",
            "type": "POST",
            "datatype": "json"
        },
        //"columnDefs":
        //    [{
        //        "targets": [2],
        //        "visible": true,
        //        "searchable": false,
        //        "orderable": false
        //    }],
        "columns": [
            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            //{
            //    data: function (data) {
            //        return "<a href='/admin/" + lang + "/user/edit/" +
            //            data.Id +
            //            "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
            //    }
            //}
        ],
        "order": [[1, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });


    $("#driverIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/user/DriverData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [2],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/user/edit/" +
                        data.Id +
                        "' class='btn btn-outline-primary btn-sm'>  <i class='fa fa-edit'></i> Edit </a>";
                }
            }
        ],
        "order": [[1, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });

});
