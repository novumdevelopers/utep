﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    var tableIndex = $("#driverIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/driver/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [5],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            { data: "Company", name: "Company" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/driver/edit/" +
                        data.Id +
                        "' class='btn btn-outline blue btn-xs'>  <i class='fa fa-edit'></i> Edit </a>" +
                        "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs delete-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> Sil</a>";
                }
            }
        ],
        "order": [[4, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });


    var tableWaited = $("#driverWaitedTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/driver/WaitedData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [3],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            {
                data: function (data) {
                    return "<a href='/admin/" + lang + "/driver/Add/" +
                        data.Id +
                        "' class='btn btn-outline blue btn-xs'>  <i class='fa fa-edit'></i> Əlavə Et </a>";
                }
            }
        ],
        "order": [[1, "asc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });

    var tableTrash = $("#driverTrashTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/admin/" + lang + "/driver/TrashData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [5],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "Email", name: "Email" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Geri Qaytar</a>";
                }
            }
        ],
        "order": [[4, "desc"]],
        "scrollX": true,
        "scrollCollapse": true
    });

    tableIndex.on('click',
        '.delete-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/Driver/Trash/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableIndex.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableTrash.on('click',
        '.back-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/Driver/Back/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableTrash.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

});
