﻿$(document).ready(function () {

    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    var lang = pathname[2];

    var tableIndex = $("#companyclientadminIndexTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/Admin/" + lang + "/companyClientAdmin/loadData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [10],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "CompanyName", name: "CompanyName" },
            { data: "CompanyEmail", name: "CompanyEmail" },
            { data: "CompanyPhone", name: "CompanyPhone" },
            { data: "CompanyAddress", name: "CompanyAddress" },

            {
                data: function (data) {
                    return (data.CompanyType.Status == 2) ? data.CompanyType.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyType.Name;
                },
                name: "CompanyType"
            },

            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "Email", name: "Email" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a href='/Admin/" + lang + "/companyClientAdmin/edit/" +
                        data.Id +
                        "' class='btn btn-outline blue btn-xs'>  <i class='fa fa-edit'></i> Edit </a>" +
                        "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs delete-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fa fa-remove'></i> Sil</a>";
                }
            }
        ],
        "order": [[9, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });

    var tableTrash = $("#companyclientadminTrashTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/Admin/" + lang + "/companyClientAdmin/TrashData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [10],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "CompanyName", name: "CompanyName" },
            { data: "CompanyEmail", name: "CompanyEmail" },
            { data: "CompanyPhone", name: "CompanyPhone" },
            { data: "CompanyAddress", name: "CompanyAddress" },

            {
                data: function (data) {
                    return (data.CompanyType.Status == 2) ? data.CompanyType.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyType.Name;
                },
                name: "CompanyType"
            },

            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "Email", name: "Email" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs back-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Geri Qaytar</a>";
                }
            }
        ],
        "order": [[9, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });

    var tableWaited = $("#companyclientadminWaitedTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/Admin/" + lang + "/companyClientAdmin/WaitedData",
            "type": "POST",
            "datatype": "json"
        },
        "columnDefs":
            [{
                "targets": [10],
                "visible": true,
                "searchable": false,
                "orderable": false
            }],
        "columns": [
            { data: "CompanyName", name: "CompanyName" },
            { data: "CompanyEmail", name: "CompanyEmail" },
            { data: "CompanyPhone", name: "CompanyPhone" },
            { data: "CompanyAddress", name: "CompanyAddress" },

            {
                data: function (data) {
                    return (data.CompanyType.Status === 2) ? data.CompanyType.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyType.Name;
                },
                name: "CompanyType"
            },

            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "Email", name: "Email" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            {
                data: function (data) {
                    return "<a id='" +
                        data.Id +
                        "' class='btn btn-outline yellow-lemon  btn-xs waited-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Aktiv Et</a>";
                }
            }
        ],
        "order": [[9, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });


    var tableEmailNotConfirmed = $("#emailNotConfirmedTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Azerbaijan.json"
        },
        "processing": true,
        "serverSide": true,
        "filter": true,
        "orderMulti": false,
        "ajax": {
            "url": "/Admin/" + lang + "/companyClientAdmin/EmailNotConfirmedData",
            "type": "POST",
            "datatype": "json"
        },
        //"columnDefs":
        //    [{
        //        "targets": [10],
        //        "visible": true,
        //        "searchable": false,
        //        "orderable": false
        //    }],
        "columns": [
            { data: "CompanyName", name: "CompanyName" },
            { data: "CompanyEmail", name: "CompanyEmail" },
            { data: "CompanyPhone", name: "CompanyPhone" },
            { data: "CompanyAddress", name: "CompanyAddress" },

            {
                data: function (data) {
                    return (data.CompanyType.Status === 2) ? data.CompanyType.Name + " <i class='fa fa-times text-danger'></i>" : data.CompanyType.Name;
                },
                name: "CompanyType"
            },

            { data: "FirstName", name: "FirstName" },
            { data: "LastName", name: "LastName" },
            { data: "Email", name: "Email" },
            { data: "PhoneNumber", name: "PhoneNumber" },
            {
                data: function (data) {
                    return formatDate(data.AddedDate);
                },
                name: "AddedDate"
            },
            //{
            //    data: function (data) {
            //        return "<a id='" +
            //            data.Id +
            //            "' class='btn btn-outline yellow-lemon  btn-xs waited-confirmation'  data-btn-ok-label='Bəli!' data-btn-cancel-label='Xeyr!' data-toggle='confirmation' data-placement='bottom' data-original-title='Əminsiniz?' data-container='body'  aria-describedby='confirmation64993'>  <i class='fas fa-undo'></i> Aktiv Et</a>";
            //    }
            //}
        ],
        "order": [[9, "desc"]],
        "scrollX": true,
        "scrollCollapse": true,
    });

    tableIndex.on('click',
        '.delete-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/CompanyClientAdmin/Trash/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableIndex.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableTrash.on('click',
        '.back-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/CompanyClientAdmin/Back/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableTrash.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

    tableWaited.on('click',
        '.waited-confirmation',
        function (e) {
            $(this).confirmation({
                onConfirm: function (event) {
                    var tr = $(this).closest('tr');
                    var btn = $(this);
                    var id = btn.attr("id");
                    $.ajax({
                        url: "/Admin/" + lang + "/CompanyClientAdmin/Back/" + id,
                        method: "Post",
                        data: $('#DataTableForm').serialize(),
                        success: function (response) {
                            if (response.Status) {
                                swal('Uğurlu!', 'Success', 'success');
                                tableWaited.row(tr).remove().draw();
                            } else {
                                swal('Uğursuz!', 'Error', 'error');
                            }
                        },
                        error: function () {
                            swal('Uğursuz!', 'Xəta yarandı. Daha sonra yenidən yoxlayın.', 'error');
                        }
                    });

                }
            });
            $(this).confirmation("show");
        });

});
