﻿using System.Collections.Generic;
using UTEP.Data.Domain.Models.Base;

namespace UTEP.Data.Domain.Models.Tables.LocationBasedGroup
{
    public class LocationType : BaseEntity
    {
        public LocationType()
        {
            Locations = new HashSet<Location>();
        }

        public string Name { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
    }
}
