﻿using System.Collections.Generic;
using UTEP.Data.Domain.Models.Base;
using UTEP.Data.Domain.Models.Tables.CompanyBasedGroup;

namespace UTEP.Data.Domain.Models.Tables.LocationBasedGroup
{
    public class Location : BaseEntity
    {
        public Location()
        {
            Children = new HashSet<Location>();
            //CompanyInfos = new HashSet<CompanyInfo>();
        }
        public string Name { get; set; }
        public long? ParentId { get; set; }
        public virtual Location Parent { get; set; }
        public long LocationTypeId { get; set; }
        public virtual LocationType LocationType { get; set; }
        public virtual ICollection<Location> Children { get; set; }
        //public virtual ICollection<CompanyInfo> CompanyInfos { get; set; }
    }
}
